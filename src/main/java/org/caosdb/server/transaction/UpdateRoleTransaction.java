/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import org.apache.shiro.SecurityUtils;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.database.backend.transaction.InsertRole;
import org.caosdb.server.database.backend.transaction.RetrieveRole;
import org.caosdb.server.utils.ServerMessages;

public class UpdateRoleTransaction extends AccessControlTransaction {

  private final Role role;

  public UpdateRoleTransaction(final Role role) {
    this.role = role;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject()
        .checkPermission(ACMPermissions.PERMISSION_UPDATE_ROLE_DESCRIPTION(this.role.name));

    if (!UserSources.isRoleExisting(this.role.name)) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }

    execute(new InsertRole(this.role), getAccess());
    RetrieveRole.removeCached(this.role.name);
  }
}
