/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import java.util.List;
import org.caosdb.server.database.backend.transaction.GetChildren;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether any children of this entity do exist. There must not be any children left when an
 * entity is to be deleted. If all children are to be deleted, too, the test passes.
 *
 * @author tf
 */
public class CheckChildDependencyExistent extends EntityJob {

  @Override
  public final void run() {
    if (getEntity().getDomain() == null || getEntity().getDomain() == 0) {

      final List<Integer> children = execute(new GetChildren(getEntity().getId())).getList();

      // loop:
      for (final Integer id : children) {
        final EntityInterface foreign = getEntityById(id);
        if (foreign == null) {
          // if the child is not in the container, the test fails.
          getEntity().addError(ServerMessages.REQUIRED_BY_PERSISTENT_ENTITY);
          getEntity().addInfo("Required by entity " + id + ".");
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        }

        // // loop through all entities in the current container
        // for (final EntityInterface e : getContainer()) {
        //
        // // if e is a child
        // if (e.getId().equals(id)) {
        // if (e.getEntityStatus() == EntityStatus.UNQUALIFIED) {
        // getEntity().addError(ServerMessages.REQUIRED_BY_UNQUALIFIED);
        // getEntity().addInfo("Required by entity " + id + ".");
        // getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        // } else {
        // // if the children are among the entities in the
        // // container which is to be deleted, the test passes
        // e.acceptObserver(new Observer() {
        // @Override
        // public boolean notifyObserver(final String evt,
        // final Observable o) {
        // if (evt == Entity.ENTITY_STATUS_CHANGED_EVENT && o == e) {
        // if (e.getEntityStatus() == EntityStatus.UNQUALIFIED) {
        // getEntity().addError(
        // ServerMessages.REQUIRED_BY_UNQUALIFIED);
        // getEntity().addInfo("Required by entity " + id + ".");
        // getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        // return false;
        // }
        // }
        // return true;
        // }
        // });
        // }
        // continue loop;
        // }
        // }

        // // if the child is not in the container, the test fails.
        // getEntity().addError(ServerMessages.REQUIRED_BY_PERSISTENT_ENTITY);
        // getEntity().addInfo("Required by entity " + id + ".");
        // getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
      }
    }
  }
}
