/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.container;

import static org.junit.Assert.assertEquals;

import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.PropertyToElementStrategyTest;
import org.caosdb.server.entity.xml.SetFieldStrategy;
import org.jdom2.Element;
import org.junit.BeforeClass;
import org.junit.Test;

public class PropertyContainerTest {

  public static Entity house = null;
  public static Property houseHeight = null;
  public static Entity window = null;
  public static Property windowHeight = null;
  public static Entity houseOwner = null;
  public static Property windowProperty = null;

  @BeforeClass
  public static void setup() {
    window = new Entity(1234);
    windowHeight = new Property(new Entity("window.height", Role.Property));
    window.addProperty(windowHeight);
    windowHeight.setValue(new GenericValue("windowHeight"));

    houseOwner = new Entity("The Queen", Role.Record);

    house = new Entity("Buckingham Palace", Role.Record);
    houseHeight = new Property(new Entity("height", Role.Property));
    houseHeight.setValue(new GenericValue("houseHeight"));
    house.addProperty(houseHeight);
    windowProperty = new Property(2345);
    windowProperty.setName("window");
    windowProperty.setValue(new ReferenceValue(window.getId()));
    house.addProperty(windowProperty);

    house.addProperty(new Property());
    house.addProperty(new Property(houseHeight));
  }

  @Test
  public void test() {
    PropertyContainer container = new PropertyContainer(new Entity());
    Element element = new Element("Record");
    SetFieldStrategy setFieldStrategy =
        new SetFieldStrategy().addSelection(PropertyToElementStrategyTest.parse("window.height"));

    container.addToElement(windowProperty, element, setFieldStrategy);

    assertEquals(1, element.getChildren().size());
  }
}
