/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import java.util.ArrayList;
import java.util.BitSet;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.jdom2.Element;

public class CQLParsingErrorListener extends BaseErrorListener {

  public static class ParsingError {

    private final String type;
    private final String msg;
    private final Integer line;
    private final Integer c;

    public ParsingError(final String type, final String msg) {
      this(type, msg, null, null);
    }

    public ParsingError(final String type, final String msg, final Integer line, final Integer c) {
      this.type = type;
      this.msg = msg;
      this.line = line;
      this.c = c;
    }

    public Element toElement() {
      final Element e = new Element("ParsingError");
      e.setAttribute("type", this.type);
      e.setText(this.msg);
      if (this.line != null) {
        e.setAttribute("line", this.line.toString());
      }
      if (this.c != null) {
        e.setAttribute("character", this.c.toString());
      }
      return e;
    }

    @Override
    public String toString() {
      return "ParsingError: " + this.type + "," + this.msg + "," + this.line + "," + this.c;
    }
  }

  private final int unknownChar;

  public CQLParsingErrorListener(final int t) {
    this.unknownChar = t;
  }

  private final ArrayList<ParsingError> errors = new ArrayList<ParsingError>();

  @Override
  public void reportAmbiguity(
      final Parser recognizer,
      final DFA dfa,
      final int startIndex,
      final int stopIndex,
      final boolean exact,
      final BitSet ambigAlts,
      final ATNConfigSet configs) {
    this.errors.add(
        new ParsingError(
            "Ambiguity", "Ambiguity conflict in query. Please report this with a bug ticket."));
  }

  @Override
  public void reportAttemptingFullContext(
      final Parser recognizer,
      final DFA dfa,
      final int startIndex,
      final int stopIndex,
      final BitSet conflictingAlts,
      final ATNConfigSet configs) {
    this.errors.add(
        new ParsingError(
            "AttemptingFullContext",
            "AttemptingFullContext conflict in query. Please report this with a bug ticket.\n"));
  }

  @Override
  public void reportContextSensitivity(
      final Parser recognizer,
      final DFA dfa,
      final int startIndex,
      final int stopIndex,
      final int prediction,
      final ATNConfigSet configs) {
    this.errors.add(
        new ParsingError(
            "ContextSensitivity",
            "ContextSensitivity conflict in query. Please report this with a bug ticket.\n"));
  }

  @Override
  public void syntaxError(
      final Recognizer<?, ?> recognizer,
      final Object offendingSymbol,
      final int line,
      final int charPositionInLine,
      final String msg,
      final RecognitionException e) {
    this.hasErrors = true;
    if (offendingSymbol instanceof Token) {
      if (this.unknownChar == ((Token) offendingSymbol).getType()) {
        this.errors.add(
            new ParsingError(
                "SyntaxError",
                "Unknown character '" + ((Token) offendingSymbol).getText() + "'",
                line,
                charPositionInLine));
        return;
      }
    }
    this.errors.add(new ParsingError("SyntaxError", msg, line, charPositionInLine));
  }

  public ArrayList<ParsingError> getErrors() {
    if (this.hasErrors) {
      return this.errors;
    }
    return null;
  }

  private boolean hasErrors = false;

  public boolean hasErrors() {
    return this.hasErrors;
  }
}
