/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.database.access.InitAccess;
import org.caosdb.server.transaction.WriteTransaction;
import org.junit.BeforeClass;
import org.junit.Test;

public class QueryTest {

  @BeforeClass
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  String getCacheKey(String query) {
    Query q = new Query(query);
    q.parse();
    return q.getCacheKey();
  }

  @Test
  public void testGetKey() {
    assertEquals("enamePOV(pname,=,val1)", getCacheKey("FIND ename WITH pname = val1"));
    assertEquals("enamePOV(pname,=,val1)", getCacheKey("COUNT ename WITH pname = val1"));
    assertEquals("enamePOV(pname,=,val1)", getCacheKey("SELECT bla FROM ename WITH pname = val1"));
    assertEquals("enamePOV(pname,null,null)", getCacheKey("SELECT bla FROM ename WITH pname"));
    assertEquals(
        "enamemaxPOV(pname,null,null)",
        getCacheKey("SELECT bla FROM ename WITH THE GREATEST pname"));

    assertEquals(
        "RECORDenamePOV(pname,=,val1)", getCacheKey("FIND RECORD ename WITH pname = val1"));
    assertEquals("ENTITYPOV(pname,=,val1)", getCacheKey("COUNT ENTITY WITH pname = val1"));
    assertEquals(
        "enameConj(POV(pname,=,val1)POV(ename2,=,val2))",
        getCacheKey("SELECT bla FROM ename WITH pname = val1 AND ename2 = val2"));

    assertEquals("versionedENTITYID(,>,2)", getCacheKey("FIND ANY VERSION OF ENTITY WITH ID > 2"));
    assertEquals("ENTITYID(min,,)", getCacheKey("FIND ENTITY WITH THE SMALLEST ID"));
    assertEquals("ENTITYSAT(asdf/%%)", getCacheKey("FIND ENTITY WHICH IS STORED AT /asdf/*"));
    assertEquals("ENTITYSAT(asdf/asdf)", getCacheKey("FIND ENTITY WHICH IS STORED AT asdf/asdf"));
    assertEquals(
        "enamePOV(ref1,null,null)SUB(POV(pname,>,val1)",
        getCacheKey("FIND ename WITH ref1 WITH pname > val1 "));
    assertEquals(
        "ename@(ref1,null)SUB(POV(pname,>,val1)",
        getCacheKey("FIND ename WHICH IS REFERENCED BY ref1 WITH pname > val1 "));
  }

  @Test
  public void testOptimizationOfFindStar() {
    Query q = new Query("FIND *");
    q.parse(false);
    assertEquals("*", q.getEntity().str);
    assertNull(q.getRole());

    q.optimize();
    assertNull(q.getEntity());
    assertEquals(Query.Role.ENTITY, q.getRole());
  }

  /**
   * Assure that {@link WriteTransaction#commit()} calls {@link Query#clearCache()}.
   *
   * <p>Since currently the cache shall be cleared whenever there is a commit.
   */
  @Test
  public void testEtagChangesAfterWrite() {
    String old = Query.getETag();
    assertNotNull(old);

    WriteTransaction w =
        new WriteTransaction(null) {

          @Override
          public boolean useCache() {
            // this function is being overriden purely for the purpose of calling
            // commit() (which is protected)
            try {
              // otherwise the test fails because getAccess() return null;
              setAccess(new InitAccess(null));

              commit();
            } catch (Exception e) {
              fail("this should not happen");
            }
            return false;
          }
        };

    // trigger commit();
    w.useCache();

    String neu = Query.getETag();
    assertNotEquals(old, neu, "old and new tag should not be equal");
  }
}
