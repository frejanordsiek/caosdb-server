package org.caosdb.server.scripting;

public class ScriptingPermissions {

  public static final String PERMISSION_EXECUTION(final String call) {
    StringBuilder ret = new StringBuilder(18 + call.length());
    ret.append("SCRIPTING:EXECUTE:");
    ret.append(call.replace("/", ":"));
    return ret.toString();
  }
}
