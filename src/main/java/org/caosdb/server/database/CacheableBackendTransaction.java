/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019,2020 IndiScale GmbH
 * Copyright (C) 2019,2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database;

import java.io.Serializable;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.caosdb.server.database.exceptions.TransactionException;

public abstract class CacheableBackendTransaction<K, V extends Serializable>
    extends BackendTransaction {

  private Boolean cached = null;
  private ICacheAccess<K, V> cache;

  public CacheableBackendTransaction(ICacheAccess<K, V> cache) {
    this.cache = cache;
  }

  public abstract V executeNoCache() throws TransactionException;

  @Override
  public final void execute() throws TransactionException {
    final V v = execute(getKey());
    if (v != null) {
      process(v);
    }
  }

  private final V execute(final K key) throws TransactionException {
    // get from cache if possible...
    if (cacheIsEnabled() && key != null) {
      final V cached = getCache().get(key);
      if (cached != null) {
        this.cached = true;
        return cached;
      }
    }

    // ... or executeNoCache()
    final V notCached = executeNoCache();
    this.cached = false;
    if (notCached != null) {
      if (cacheIsEnabled() && key != null) {
        // now cache if possible
        getCache().put(key, notCached);
      }
    }
    return notCached;
  }

  protected abstract void process(V t) throws TransactionException;

  protected abstract K getKey();

  protected final ICacheAccess<K, V> getCache() {
    return cache;
  }

  private final boolean cacheIsEnabled() {
    return useCache() && getCache() != null;
  }

  @Override
  public String toString() {
    return super.toString() + " (cached=" + this.cached + ")";
  }
}
