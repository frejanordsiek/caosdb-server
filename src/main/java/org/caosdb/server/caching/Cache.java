/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.caching;

import java.io.Serializable;
import org.apache.commons.jcs.access.behavior.ICacheAccess;

/**
 * Caching Helper Class used for all caches in the CaosDB Server.
 *
 * <p>The actual work is delegated to an instance of {@link JCSCacheHelper}. However, the delegate
 * {@link #DELEGATE} can be be overridden for testing purposes with {@link #setDelegate(Cache)}.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class Cache {

  /** The default cache helper delegate. */
  private static CacheHelper DELEGATE = new JCSCacheHelper();

  public static void setDelegate(CacheHelper delegate) {
    DELEGATE = delegate;
  }

  public static final <K, V extends Serializable> ICacheAccess<K, V> getCache(String region) {
    return DELEGATE.getCache(region);
  }
}
