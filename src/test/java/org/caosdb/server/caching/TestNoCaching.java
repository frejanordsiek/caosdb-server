package org.caosdb.server.caching;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.database.backend.transaction.RetrieveProperties;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestNoCaching {

  @BeforeClass
  public static void init() throws IOException {
    CaosDBServer.initServerProperties();
    CaosDBServer.setProperty(ServerProperties.KEY_CACHE_DISABLE, "TRUE");
    JCSCacheHelper.init();
  }

  @Test
  public void testCacheConfig() {
    CacheAccess<String, String> retrieve_properties_cache =
        JCS.getInstance(RetrieveProperties.CACHE_REGION);
    assertEquals(0, retrieve_properties_cache.getCacheAttributes().getMaxObjects());
  }
}
