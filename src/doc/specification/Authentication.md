# Authentication
 Some features of CaosDB are available to registered users only. Making any changes to the data stock via HTTP requires authentication by `username` _plus_ `password`. They are to be send as a HTTP header, while the password is to be hashed by the sha512 algorithm:

| `username:` | `$username` | 
|-------------|-------------|-
| `password:` | `$SHA512ed_password` |


## Sessions

### Login

#### Request Challenge

 * `GET http://host:port/mpidsserver/login?username=$username`
 * `GET http://host:port/mpidsserver/login` with `username` header

*no password required to be sent over http*

The request returns an AuthToken with a login challenge as a cookie. The AuthToken is a dictionary of the following form:


        {scope=$scope;
        mode=LOGIN;
        offerer=$offerer;
        auth=$auth
        expires=$expires;
        date=$date;
        hash=$hash;
        session=$session;
        }

 $scope:: A uri pattern string. Example: ` {**/*} `
 $mode:: `ONETIME`, `SESSION`, or `LOGIN`
 $offerer:: A valid username
 $auth:: A valid username
 $expires:: A `YYYY-MM-DD HH:mm:ss[.nnnn]` date string
 $date:: A `YYYY-MM-DD HH:mm:ss[.nnnn]` date string
 $hash:: A string
 $session:: A string

The challenge is solved by concatenating the `$hash` string and the user's `$password` string and calculating the sha512 hash of both. Pseudo code:


        $solution = sha512($hash + sha512($password))

#### Send Solution

The old $hash string in the cookie has to be replaces by $solution and the cookie is to be send with the next request:

`PUT http://host:port/mpidsserver/login`

The server will return the user's entity in the HTTP body, e.g.


        <Response ...>
          <User name="$username" ...>
            ...
          </User>
        </Response>

and a new AuthToken with `$mode=SESSION` and a new expiration date and so on. This AuthToken cookie is to be send with every request.

### Logout

Send 

`PUT http://host:port/mpidsserver/logout`

with a valid AuthToken cookie. No new AuthToken will be returned and no AuthToken with that `$session` will be accepted anymore.

### Commandline solution with `curl` ##

To use curl for talking with the server, first save your password into a variable:
`PW=$(cat)`

The create a cookie in `cookie.txt` like this (note that this makes your password visible for a short time to everyone on your system:
`curl -X POST -c cookie.txt -D head.txt -H "Content-Type: application/x-www-form-urlencoded" -d username=<USERNAME> -d password="$PW" --insecure "https://<SERVER>/login`

To use the cookie, pass it on with later requests:
`curl -X GET -b cookie.txt --insecure "https://<SERVER>/Entity/12345"`

