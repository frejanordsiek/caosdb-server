package org.caosdb.server.logging;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.caosdb.server.CaosDBServer;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.restlet.Request;
import org.restlet.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestLogging {

  private Logger logger = LoggerFactory.getLogger(getClass());
  private Logger request_errors_logger =
      LoggerFactory.getLogger(CaosDBServer.REQUEST_ERRORS_LOGGER);
  private Logger request_time_logger = LoggerFactory.getLogger(CaosDBServer.REQUEST_TIME_LOGGER);

  @BeforeClass
  public static void setup() throws IOException {
    File f = new File("testlog/");
    FileUtils.forceDeleteOnExit(f);
  }

  @Test
  public void testLogger() {
    logger.error("error");
    logger.warn("warn");
    logger.info("info");
    logger.debug("debug");
    logger.trace("trace");
    Assert.assertEquals(logger.getName(), "org.caosdb.server.logging.TestLogging");
    Assert.assertTrue(logger.isErrorEnabled());
    Assert.assertTrue(logger.isWarnEnabled());
    Assert.assertTrue(logger.isInfoEnabled());
    Assert.assertTrue(logger.isDebugEnabled());
    Assert.assertTrue(logger.isTraceEnabled());
  }

  @Test
  public void testRequestErrorsLogger() {
    Assert.assertTrue(request_errors_logger.isErrorEnabled());
    request_errors_logger.error(
        "ERROR:REQUEST:" + "ABC0123",
        new RequestErrorLogMessage(new Request(), new Response(null)),
        new Exception("exc"));
  }

  @Test
  public void testRequestTimeLogger() {
    Assert.assertTrue(CaosDBServer.isDebugMode());
    Assert.assertFalse(request_time_logger.isErrorEnabled());
    Assert.assertFalse(request_time_logger.isTraceEnabled());
  }
}
