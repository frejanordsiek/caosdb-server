/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

package org.caosdb.server.jobs.core;

import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.transaction.WriteTransaction;

/**
 * This job constructs an ordinary Property from the State right before the entity is being written
 * to the back-end and after any checks run.
 */
@JobAnnotation(transaction = WriteTransaction.class, stage = TransactionStage.PRE_TRANSACTION)
public class MakeStateProperty extends EntityStateJob {

  @Override
  protected void run() {
    State s = getState();
    if (s != null) {
      try {
        addStateProperty(s);
      } catch (Message e) {
        getEntity().addError(e);
      }
    }
  }

  private void addStateProperty(State stateEntity) throws Message {
    getEntity().addProperty(stateEntity.createStateProperty());
  }
}
