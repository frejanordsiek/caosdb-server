/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

package org.caosdb.server.resource;

import static java.net.URLDecoder.decode;

import java.io.File;
import java.io.IOException;
import org.caosdb.server.FileSystem;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.utils.FileUtils;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.JDOMException;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;

/**
 * Download temporary files via GET method only.
 *
 * @author Daniel Hornung
 */
public class SharedFileResource extends AbstractCaosDBServerResource {

  /**
   * Download a File from the tempfiles folder. Only one File per Request.
   *
   * @author Daniel Hornung
   * @return InputRepresentation
   * @throws IOException
   */
  @Override
  protected Representation httpGetInChildClass() throws Exception {
    final String specifier =
        decode(
            (getRequest().getAttributes().containsKey("path")
                ? (String) getRequest().getAttributes().get("path")
                : ""),
            "UTF-8");

    final File file = getFile(specifier);
    if (file == null) {
      Representation ret =
          error(ServerMessages.ENTITY_DOES_NOT_EXIST, Status.CLIENT_ERROR_NOT_FOUND);
      return ret;
    }

    final MediaType mt = MediaType.valueOf(FileUtils.getMimeType(file));
    final FileRepresentation ret = new FileRepresentation(file, mt);

    // HTML files should be opened in the browser.
    // Any other media type than HTML is attached for download.
    if (!MediaType.TEXT_HTML.includes(mt)) {
      ret.setDisposition(new Disposition(Disposition.TYPE_ATTACHMENT));
    }

    return ret;
  }

  protected File getFile(final String path) throws IOException {
    File ret = FileSystem.getFromShared(path);
    return ret;
  }

  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException, JDOMException {
    this.setStatus(org.restlet.data.Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
    return null;
  }
}
