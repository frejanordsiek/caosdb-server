/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.jobs.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.IndexedSingleValue;
import org.caosdb.server.datatype.ReferenceDatatype;
import org.caosdb.server.datatype.ReferenceDatatype2;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.datatype.TextDatatype;
import org.caosdb.server.entity.ClientMessage;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.permissions.EntityACI;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.query.Query;
import org.caosdb.server.utils.EntityStatus;
import org.jdom2.Element;

/**
 * The EntityStateJob is the abstract base class for four EntityJobs:
 *
 * <p>1. The {@link InitEntityState} job reads ClientMessages or StateProperties with tag state and
 * converts them into instances of State. This job runs during WriteTransactions. This job runs
 * during the INIT Phase and does not perform any checks other than those necessary for the
 * conversion.
 *
 * <p>2. The {@link CheckStateTransition} job checks if the attempted state transition is in
 * compliance with the state model. This job runs during the CHECK phase and should do all necessary
 * consistency and permission checks.
 *
 * <p>3. The {@link MakeStateProperty} job constructs an ordinary Property from the State right
 * before the entity is being written to the back-end and after any checks run.
 *
 * <p>4. The {@link MakeStateMessage} job converts a state property (back) into State messages and
 * appends them to the entity.
 *
 * <p>Only the 4th job ({@link MakeStateMessage}) runs during Retrieve transitions. During
 * WriteTransactions all four jobs do run.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public abstract class EntityStateJob extends EntityJob {

  protected static final String SERVER_PROPERTY_EXT_ENTITY_STATE = "EXT_ENTITY_STATE";

  public static final String TO_STATE_PROPERTY_NAME = "to";
  public static final String FROM_STATE_PROPERTY_NAME = "from";
  public static final String FINAL_STATE_PROPERTY_NAME = "final";
  public static final String INITIAL_STATE_PROPERTY_NAME = "initial";
  public static final String STATE_RECORD_TYPE_NAME = "State";
  public static final String STATE_MODEL_RECORD_TYPE_NAME = "StateModel";
  public static final String TRANSITION_RECORD_TYPE_NAME = "Transition";
  public static final String TRANSITION_XML_TAG = "Transition";
  public static final String TRANSITION_ATTRIBUTE_NAME = "name";
  public static final String TRANSITION_ATTRIBUTE_DESCRIPTION = "description";
  public static final String TO_XML_TAG = "ToState";
  public static final String FROM_XML_TAG = "FromState";
  public static final String STATE_XML_TAG = "State";
  public static final String STATE_ATTRIBUTE_MODEL = "model";
  public static final String STATE_ATTRIBUTE_NAME = "name";
  public static final String STATE_ATTRIBUTE_DESCRIPTION = "description";
  public static final String STATE_ATTRIBUTE_ID = "id";
  public static final String ENTITY_STATE_ROLE_MARKER = "?STATE?";
  public static final String PERMISSION_STATE_TRANSION = "STATE:TRANSITION:";

  public static final Message STATE_MODEL_NOT_FOUND =
      new Message(MessageType.Error, "StateModel not found.");
  public static final Message STATE_NOT_IN_STATE_MODEL =
      new Message(MessageType.Error, "State does not exist in this StateModel.");
  public static final Message COULD_NOT_CONSTRUCT_STATE_MESSAGE =
      new Message(MessageType.Error, "Could not construct the state message.");
  public static final Message COULD_NOT_CONSTRUCT_TRANSITIONS =
      new Message(MessageType.Error, "Could not construct the transitions.");
  public static final Message STATE_MODEL_NOT_SPECIFIED =
      new Message(MessageType.Error, "State model not specified.");
  public static final Message STATE_NOT_SPECIFIED =
      new Message(MessageType.Error, "State not specified.");

  /**
   * Represents a Transition which is identified by a name and the two States from and to which an
   * entity is being transitioned.
   *
   * <p>Currently, only exactly one toState and one fromState can be defined. However, it might be
   * allowed in the future to have multiple states here.
   *
   * @author Timm Fitschen (t.fitschen@indiscale.com)
   */
  public class Transition {

    private String name;
    private String description;
    private State fromState;
    private State toState;
    private Map<String, String> transitionProperties;

    /**
     * @param transition The transition Entity, from which the Transition is created. Relevant
     *     Properties are "to" and "from"
     */
    public Transition(EntityInterface transition) throws Message {
      this.name = transition.getName();
      this.description = transition.getDescription();
      this.fromState = getFromState(transition);
      this.toState = getToState(transition);
      this.transitionProperties = getTransitionProperties(transition);
    }

    private Map<String, String> getTransitionProperties(EntityInterface transition) {
      Map<String, String> result = new LinkedHashMap<>();
      for (Property p : transition.getProperties()) {
        if (p.getDatatype() instanceof TextDatatype) {
          result.put(p.getName(), p.getValue().toString());
        }
      }
      return result;
    }

    private State getToState(EntityInterface transition) throws Message {
      for (Property p : transition.getProperties()) {
        if (p.getName().equals(TO_STATE_PROPERTY_NAME)) {
          return createState(p);
        }
      }
      return null;
    }

    private State getFromState(EntityInterface transition) throws Message {
      for (Property p : transition.getProperties()) {
        if (p.getName().equals(FROM_STATE_PROPERTY_NAME)) {
          return createState(p);
        }
      }
      return null;
    }

    /**
     * @param previousState
     * @return true iff the previous state is a fromState of this transition.
     */
    public boolean isFromState(State previousState) {
      return this.fromState.equals(previousState);
    }

    /**
     * @param nextState
     * @return true iff the next state is a toState of this transition.
     */
    public boolean isToState(State nextState) {
      return this.toState.equals(nextState);
    }

    public State getToState() {
      return this.toState;
    }

    public State getFromState() {
      return this.fromState;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof Transition) {
        Transition that = (Transition) obj;
        return Objects.equals(this.getName(), that.getName())
            && Objects.equals(this.getFromState(), that.getFromState())
            && Objects.equals(this.getToState(), that.getToState());
      }
      return false;
    }

    public String getName() {
      return this.name;
    }

    public String getDescription() {
      return this.description;
    }

    @Override
    public String toString() {
      return "Transition (name="
          + getName()
          + ", from="
          + getFromState().getStateName()
          + ", to="
          + getToState().getStateName()
          + ")";
    }

    public Element toElement() {
      Element result = new Element(TRANSITION_XML_TAG);
      if (this.transitionProperties != null) {
        this.transitionProperties.forEach(
            (String key, String value) -> {
              result.setAttribute(key, value);
            });
      }
      if (this.name != null) {
        result.setAttribute(TRANSITION_ATTRIBUTE_NAME, this.name);
      }
      if (this.description != null) {
        result.setAttribute(TRANSITION_ATTRIBUTE_DESCRIPTION, this.description);
      }
      Element to = new Element(TO_XML_TAG);
      to.setAttribute(STATE_ATTRIBUTE_NAME, this.toState.stateName);
      if (this.toState.stateDescription != null) {
        to.setAttribute(STATE_ATTRIBUTE_DESCRIPTION, this.toState.stateDescription);
      }
      Element from = new Element(FROM_XML_TAG);
      from.setAttribute(STATE_ATTRIBUTE_NAME, this.fromState.stateName);
      return result.addContent(from).addContent(to);
    }

    public boolean isPermitted(Subject user) {
      return user.isPermitted(PERMISSION_STATE_TRANSION + this.name);
    }
  }

  /**
   * The State instance represents a single entity state. This class is used for concrete states
   * (the state of a stateful entity, say a Record) and abstract states (states which are part of a
   * {@link StateModel}).
   *
   * <p>States are identified via their name and the name of the model to which they belong.
   *
   * <p>States are represented by Records with the state's name as the Record name. They belong to a
   * StateModel iff the StateModel RecordType references the State Record. Each State should only
   * belong to one StateModel.
   *
   * <p>Furthermore, States are the start or end point of {@link Transition Transitions} which
   * belong to the same StateModel. Each State can be part of several transitions at the same time.
   *
   * <p>Note: The purpose of this should not be confused with {@link EntityStatus} which is purely
   * for internal use.
   *
   * @author Timm Fitschen (t.fitschen@indiscale.com)
   */
  public class State implements ToElementable {

    private String stateModelName = null;
    private String stateName = null;
    private EntityInterface stateEntity = null;
    private EntityInterface stateModelEntity = null;
    private StateModel stateModel;
    private String stateDescription = null;
    private Integer stateId = null;
    private EntityACL stateACL = null;
    private Map<String, String> stateProperties;

    public State(EntityInterface stateEntity, EntityInterface stateModelEntity) throws Message {
      this.stateEntity = stateEntity;
      this.stateDescription = stateEntity.getDescription();
      this.stateId = stateEntity.getId();
      this.stateName = stateEntity.getName();
      this.stateModelEntity = stateModelEntity;
      this.stateModelName = stateModelEntity.getName();
      this.stateACL = createStateACL(stateEntity.getEntityACL());
      this.stateProperties = createStateProperties(stateEntity);
    }

    private Map<String, String> createStateProperties(EntityInterface stateEntity) {
      Map<String, String> result = new LinkedHashMap<>();
      for (Property p : stateEntity.getProperties()) {
        if (p.getDatatype() instanceof TextDatatype) {
          result.put(p.getName(), p.getValue().toString());
        }
      }
      return result;
    }

    private EntityACL createStateACL(EntityACL entityACL) {
      LinkedList<EntityACI> rules = new LinkedList<>();
      for (EntityACI aci : entityACL.getRules()) {
        if (aci.getResponsibleAgent().toString().startsWith(ENTITY_STATE_ROLE_MARKER)) {
          int end = aci.getResponsibleAgent().toString().length() - 1;
          String role = aci.getResponsibleAgent().toString().substring(7, end);
          rules.add(
              new EntityACI(org.caosdb.server.permissions.Role.create(role), aci.getBitSet()));
        }
      }
      return new EntityACL(rules);
    }

    public EntityACL getStateACL() {
      return this.stateACL;
    }

    public String getStateDescription() throws Message {
      return this.stateDescription;
    }

    public Integer getStateId() throws Message {
      return this.stateId;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof State) {
        State that = (State) obj;
        return Objects.equals(that.getStateName(), this.getStateName())
            && Objects.equals(that.getStateModelName(), this.getStateModelName());
      }
      return false;
    }

    @Override
    public int hashCode() {
      return 21364234 + this.getStateName().hashCode() + this.getStateModelName().hashCode();
    }

    /**
     * Serialize this State into XML.
     *
     * <p>The result looks approximately like this: {@code <State name="My name" model="Model's
     * name"/>}
     */
    @Override
    public void addToElement(Element ret) {
      Element e = new Element(STATE_XML_TAG);
      if (this.stateProperties != null) {
        this.stateProperties.forEach(
            (String key, String value) -> {
              e.setAttribute(key, value);
            });
      }
      if (this.stateModelName != null) {
        e.setAttribute(STATE_ATTRIBUTE_MODEL, this.stateModelName);
      }
      if (this.stateName != null) {
        e.setAttribute(STATE_ATTRIBUTE_NAME, this.stateName);
      }
      if (this.stateDescription != null) {
        e.setAttribute(STATE_ATTRIBUTE_DESCRIPTION, this.stateDescription);
      }
      if (this.stateId != null) {
        e.setAttribute(STATE_ATTRIBUTE_ID, Integer.toString(this.stateId));
      }
      if (this.stateModel != null) {
        this.stateModel.transitions.forEach(
            (Transition t) -> {
              if (t.isFromState(this) && t.isPermitted(getUser())) {
                e.addContent(t.toElement());
              }
            });
      }
      ret.addContent(e);
    }

    public String getStateModelName() {
      return this.stateModelName;
    }

    private String getStateName() {
      return this.stateName;
    }

    public StateModel getStateModel() throws Message {
      if (this.stateModel == null) {
        this.stateModel = new StateModel(this.stateModelEntity);
      }
      return this.stateModel;
    }

    /**
     * @return true iff this state is an initial state of its StateModel.
     * @throws Message
     */
    public boolean isInitial() throws Message {
      return Objects.equals(this, getStateModel().initialState);
    }

    /**
     * @return true iff this state is a final state of its StateModel.
     * @throws Message
     */
    public boolean isFinal() throws Message {
      return Objects.equals(this, getStateModel().finalState);
    }

    /**
     * Create a Property which represents the current entity state of a stateful entity.
     *
     * @return stateProperty
     * @throws Message
     */
    public Property createStateProperty() throws Message {
      EntityInterface stateRecordType = getStateRecordType();
      Property stateProperty = new Property(stateRecordType);
      stateProperty.setDatatype(new ReferenceDatatype2(stateRecordType));
      stateProperty.setValue(new ReferenceValue(getStateEntity(), false));
      stateProperty.setStatementStatus(StatementStatus.FIX);
      return stateProperty;
    }

    public EntityInterface getStateEntity() {
      return this.stateEntity;
    }

    public EntityInterface getStateModelEntity() {
      return this.stateModelEntity;
    }

    @Override
    public String toString() {
      String isInitial = null;
      String isFinal = null;
      try {
        isInitial = String.valueOf(isInitial());
      } catch (Message e) {
        isInitial = "null";
      }
      try {
        isFinal = String.valueOf(isFinal());
      } catch (Message e) {
        isFinal = "null";
      }
      return "State (name="
          + getStateName()
          + ", model="
          + getStateModelName()
          + ", initial="
          + isInitial
          + ", final="
          + isFinal
          + ")";
    }
  }

  /**
   * A StateModel is an abstract definition of a Finite State Machine for entities.
   *
   * <p>It consists of a set of States, a set of transitions, a initial state and a final state.
   *
   * <p>If the StateModel has no initial state, it cannot be initialized (no entity will ever be in
   * any of the StateModel's states) without using the forceInitialState flag.
   *
   * <p>If the StateModel has not final state, an entity with any of the states from this StateModel
   * cannot leave this StateModel (and cannot be deleted either) without using the forceFinalState
   * flag.
   *
   * @author Timm Fitschen (t.fitschen@indiscale.com)
   */
  public class StateModel {

    private String name;
    private Set<State> states;
    private Set<Transition> transitions;
    private State initialState;
    private State finalState;

    public StateModel(EntityInterface stateModelEntity) throws Message {
      this.name = stateModelEntity.getName();
      this.transitions = getTransitions(stateModelEntity);
      this.states = getStates(transitions, this);
      this.finalState = getFinalState(stateModelEntity);
      this.initialState = getInitialState(stateModelEntity);
    }

    private State getInitialState(EntityInterface stateModelEntity) throws Message {
      // TODO maybe check if there is more than one "initial" Property?
      for (Property p : stateModelEntity.getProperties()) {
        if (p.getName().equals(INITIAL_STATE_PROPERTY_NAME)) {
          return createState(p);
        }
      }
      return null;
    }

    private State getFinalState(EntityInterface stateModelEntity) throws Message {
      // TODO maybe check if there is more than one "final" Property?
      for (Property p : stateModelEntity.getProperties()) {
        if (p.getName().equals(FINAL_STATE_PROPERTY_NAME)) {
          return createState(p);
        }
      }
      return null;
    }

    /** Transitions are taken from list Property with name="Transition". */
    private Set<Transition> getTransitions(EntityInterface stateModelEntity) throws Message {
      for (Property p : stateModelEntity.getProperties()) {
        if (p.getName().equals(TRANSITION_RECORD_TYPE_NAME)) {
          return createTransitions(p);
        }
      }
      return null;
    }

    /**
     * Read out the "Transition" property and create Transition instances.
     *
     * @param p the transition property
     * @return a set of transitions
     * @throws Message if the transitions could ne be created.
     */
    private Set<Transition> createTransitions(Property p) throws Message {
      Set<Transition> result = new LinkedHashSet<>();
      try {
        if (!(p.getDatatype() instanceof AbstractCollectionDatatype)) {
          // FIXME raise an exception instead?
          return result;
        }
        p.parseValue();
        CollectionValue vals = (CollectionValue) p.getValue();
        for (IndexedSingleValue val : vals) {
          if (val.getWrapped() instanceof ReferenceValue) {
            Integer refid = ((ReferenceValue) val.getWrapped()).getId();

            String key = "transition" + Integer.toString(refid);
            EntityInterface transition = getCached(key);
            if (transition == null) {
              transition = retrieveValidEntity(refid);
              putCache(key, transition);
            }
            result.add(new Transition(transition));
          }
        }
      } catch (Exception e) {
        throw COULD_NOT_CONSTRUCT_TRANSITIONS;
      }
      return result;
    }

    /**
     * Collect all possible states from the set of transitions.
     *
     * <p>This function does not perform any consistency checks. It only add all toStates and
     * fromStates of the transitions to the result.
     *
     * @param transitions
     * @param stateModel
     * @return set of states.
     * @throws Message
     */
    private Set<State> getStates(Set<Transition> transitions, StateModel stateModel)
        throws Message {
      // TODO Move outside of this class
      Iterator<Transition> it = transitions.iterator();
      Set<State> result = new LinkedHashSet<>();
      while (it.hasNext()) {
        Transition t = it.next();
        result.add(t.getFromState());
        result.add(t.getToState());
      }
      return result;
    }

    public String getName() {
      return this.name;
    }

    public Set<State> getStates() {
      return this.states;
    }

    public Set<Transition> getTransitions() {
      return this.transitions;
    }

    public State getFinalState() {
      return this.finalState;
    }

    public State getInitialState() {
      return this.initialState;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof StateModel) {
        return ((StateModel) obj).getName().equals(this.getName());
      }
      return false;
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("StateModel (name=");
      sb.append(this.getName());
      sb.append(", initial=");
      sb.append(this.getInitialState().stateName);
      sb.append(", final=");
      sb.append(this.getFinalState().stateName);
      sb.append(", transitions=[");
      Iterator<Transition> iterator = this.transitions.iterator();
      while (iterator.hasNext()) {
        sb.append(iterator.next().name);
        sb.append(" -> ");
        sb.append(iterator.next().name);
        sb.append(", ");
      }
      sb.append("])");
      return sb.toString();
    }
  }

  private EntityInterface retrieveStateEntity(String stateName) throws Message {
    try {
      return retrieveValidEntity(retrieveValidIDByName(stateName));
    } catch (EntityDoesNotExistException e) {
      throw STATE_NOT_IN_STATE_MODEL;
    }
  }

  private EntityInterface retrieveStateModelEntity(String stateModel) throws Message {
    try {
      return retrieveValidEntity(retrieveValidIDByName(stateModel));
    } catch (EntityDoesNotExistException e) {
      throw STATE_MODEL_NOT_FOUND;
    }
  }

  protected EntityInterface getStateRecordType() throws Message {
    EntityInterface stateRecordType = getCached(STATE_RECORD_TYPE_NAME);
    if (stateRecordType == null) {
      stateRecordType = retrieveValidSparseEntityByName(STATE_RECORD_TYPE_NAME);
      putCache(STATE_RECORD_TYPE_NAME, stateRecordType);
    }
    return stateRecordType;
  }

  protected State getState() {
    return getState(false);
  }

  protected State getState(EntityInterface entity) {
    return getState(entity, false);
  }

  protected State getState(EntityInterface entity, boolean remove) {
    Iterator<ToElementable> messages = entity.getMessages().iterator();
    while (messages.hasNext()) {
      ToElementable s = messages.next();
      if (s instanceof State) {
        if (remove) {
          messages.remove();
        }
        return (State) s;
      }
    }
    return null;
  }

  protected State getState(boolean remove) {
    return getState(getEntity(), remove);
  }

  /** Return (and possibly remove) the States Properties of `entity`. */
  protected List<Property> getStateProperties(EntityInterface entity, boolean remove) {
    Iterator<Property> it = entity.getProperties().iterator();
    List<Property> result = new ArrayList<>();
    while (it.hasNext()) {
      Property p = it.next();
      if (Objects.equals(p.getName(), STATE_RECORD_TYPE_NAME)) {
        if (!(p.getDatatype() instanceof ReferenceDatatype)) {
          continue;
        }
        if (remove) {
          it.remove();
        }
        result.add(p);
      }
    }
    return result;
  }

  protected List<Property> getStateProperties(boolean remove) {
    return getStateProperties(getEntity(), remove);
  }

  /** Get the {@code ClientMessage}s which denote a state. */
  protected List<ClientMessage> getStateClientMessages(EntityInterface entity, boolean remove) {
    Iterator<ToElementable> stateMessages = entity.getMessages().iterator();
    List<ClientMessage> result = new ArrayList<>();
    while (stateMessages.hasNext()) {
      ToElementable s = stateMessages.next();
      if (s instanceof ClientMessage && STATE_XML_TAG.equals(((ClientMessage) s).getType())) {
        if (remove) {
          stateMessages.remove();
        }
        result.add((ClientMessage) s);
      }
    }
    return result;
  }

  protected List<ClientMessage> getStateClientMessages(boolean remove) {
    return getStateClientMessages(getEntity(), remove);
  }

  protected State createState(ClientMessage s) throws Message {
    String stateModel = s.getProperty(STATE_ATTRIBUTE_MODEL);
    if (stateModel == null) {
      throw STATE_MODEL_NOT_SPECIFIED;
    }
    String stateName = s.getProperty(STATE_ATTRIBUTE_NAME);
    if (stateName == null) {
      throw STATE_NOT_SPECIFIED;
    }
    String stateModelKey = "statemodel:" + stateModel;

    EntityInterface stateModelEntity = getCached(stateModelKey);
    if (stateModelEntity == null) {
      stateModelEntity = retrieveStateModelEntity(stateModel);
      putCache(stateModelKey, stateModelEntity);
    }

    String stateKey = "namestate:" + stateName;

    EntityInterface stateEntity = getCached(stateKey);
    if (stateEntity == null) {
      stateEntity = retrieveStateEntity(stateName);
      putCache(stateKey, stateEntity);
    }
    return new State(stateEntity, stateModelEntity);
  }

  /**
   * Create a State instance from the value of the state property.
   *
   * <p>This method also retrieves the state entity from the back-end. The StateModel is deduced
   * from finding an appropriately referencing StateModel Record.
   *
   * @param p the entity's state property
   * @return The state of the entity
   * @throws Message
   */
  protected State createState(Property p) throws Message {
    try {
      p.parseValue();
      ReferenceValue refid = (ReferenceValue) p.getValue();
      String key = "idstate" + Integer.toString(refid.getId());

      EntityInterface stateEntity = getCached(key);
      if (stateEntity == null) {
        stateEntity = retrieveValidEntity(refid.getId());
        putCache(key, stateEntity);
      }

      EntityInterface stateModelEntity = findStateModel(stateEntity);
      return new State(stateEntity, stateModelEntity);
    } catch (Message e) {
      throw e;
    } catch (Exception e) {
      throw COULD_NOT_CONSTRUCT_STATE_MESSAGE;
    }
  }

  private static final Map<String, EntityInterface> cache = new HashMap<>();
  private static final Set<Integer> id_in_cache = new HashSet<>();

  EntityInterface findStateModel(EntityInterface stateEntity) throws Exception {
    boolean cached = true;
    String key = "modelof" + Integer.toString(stateEntity.getId());

    EntityInterface result = getCached(key);
    if (result != null && cached) {
      return result;
    }
    // TODO This should throw a meaningful Exception if no matching StateModel can be found.
    TransactionContainer c = new TransactionContainer();
    Query query =
        new Query(
            "FIND RECORD "
                + STATE_MODEL_RECORD_TYPE_NAME
                + " WHICH REFERENCES "
                + TRANSITION_RECORD_TYPE_NAME
                + " WHICH REFERENCES "
                + Integer.toString(stateEntity.getId()),
            getUser(),
            c);
    query.execute(getTransaction().getAccess());
    result = retrieveValidEntity(c.get(0).getId());
    putCache(key, result);
    return result;
  }

  private EntityInterface getCached(String key) {
    EntityInterface result;
    synchronized (cache) {
      result = cache.get(key);
    }
    return result;
  }

  private void putCache(String key, EntityInterface value) {
    synchronized (cache) {
      if (value instanceof DeleteEntity) {
        throw new RuntimeException("Delete entity in cache. This is an implementation error.");
      }
      id_in_cache.add(value.getId());
      cache.put(key, value);
    }
  }

  protected void removeCached(EntityInterface entity) {
    synchronized (cache) {
      if (id_in_cache.contains(entity.getId())) {
        id_in_cache.remove(entity.getId());

        List<String> remove = new LinkedList<>();
        for (Entry<String, EntityInterface> entry : cache.entrySet()) {
          if (entry.getValue().getId().equals(entity.getId())) {
            remove.add(entry.getKey());
          }
        }
        for (String key : remove) {
          cache.remove(key);
        }
      }
    }
  }
}
