Author: Timm Fitschen

Email: timm.fitschen@ds.mpg.de

Date: 2013-02-23

# No Proposal
http://caosdb/register

# Proposal

## Add User

* POST Request is to be send to `http://host:port/User`.
* This requires authetication as user _admin_ (default password: _adminpw_).
* Http body:


        <Post>
          <User name="${username}" password="${md5ed_password} />
        </Post>

## Delete User

* DELETE Request
* admin authentication required.
* Http body:


        <Delete>
          <User name="${username}/>
        </Delete>

The user to be deleted may also be identified by his id (`id="${id}"`) instead of his name.
