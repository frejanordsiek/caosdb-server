/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.xml;

import static org.junit.Assert.assertEquals;

import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.query.Query.Selection;
import org.jdom2.Element;
import org.junit.Before;
import org.junit.Test;

public class PropertyToElementStrategyTest {

  public static Entity house = null;
  public static Property houseHeight = null;
  public static Entity window = null;
  public static Property windowHeight = null;
  public static Entity houseOwner = null;
  public static Property windowProperty = null;

  /**
   * Create a nested selection out of the dot-separated parts of <code>select</code>.
   *
   * <p>The returned Selection has nested subselections, so that each subselection corresponds to
   * the next part and the remainder of the initial <code>select</code> String.
   */
  public static Selection parse(String select) {
    String[] split = select.split("\\.");
    Selection result = new Selection(split[0]);
    Selection next = result;

    for (int i = 1; i < split.length; i++) {
      next.setSubSelection(new Selection(split[i]));
      next = next.getSubselection();
    }
    return result;
  }

  @Before
  public void setup() {
    window = new Entity(1234, Role.Record);
    windowHeight = new Property(new Entity("height", Role.Property));
    window.addProperty(windowHeight);
    windowHeight.setValue(new GenericValue("windowHeight"));

    houseOwner = new Entity("The Queen", Role.Record);

    house = new Entity("Buckingham Palace", Role.Record);
    houseHeight = new Property(new Entity("height", Role.Property));
    houseHeight.setValue(new GenericValue("houseHeight"));
    house.addProperty(houseHeight);
    windowProperty = new Property(2345);
    windowProperty.setName("window");
    windowProperty.setDatatype("window");
    windowProperty.setValue(new ReferenceValue(window.getId()));
    house.addProperty(windowProperty);

    house.addProperty(new Property());
    house.addProperty(new Property(houseHeight));
  }

  @Test
  public void test() {
    PropertyToElementStrategy strategy = new PropertyToElementStrategy();
    SetFieldStrategy setFieldStrategy = new SetFieldStrategy().addSelection(parse("height"));
    EntityInterface property = windowProperty;
    ((ReferenceValue) property.getValue()).setEntity(window, true);
    Element element = strategy.toElement(property, setFieldStrategy);
    assertEquals("Property", element.getName());
    assertEquals("2345", element.getAttributeValue("id"));
    assertEquals("window", element.getAttributeValue("name"));
    assertEquals(1, element.getChildren().size());
    assertEquals("Record", element.getChildren().get(0).getName());

    Element recordElement = element.getChild("Record");
    assertEquals("1234", recordElement.getAttributeValue("id"));
    assertEquals(1, recordElement.getChildren().size());
    assertEquals("windowHeight", recordElement.getChild("Property").getText());
  }
}
