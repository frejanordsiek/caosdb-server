/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetIDByNameImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLGetIDByName extends MySQLTransaction implements GetIDByNameImpl {

  public MySQLGetIDByName(final Access access) {
    super(access);
  }

  /**
   * Resolves the (primary) name of an entity to an id. This query is not necessarily unique.
   * Therefore {@link #STMT_AND_ROLE}, {@link #STMT_NOT_ROLE}, and {@link #STMT_LIMIT} can as
   * additional conditions.
   */
  public static final String STMT_GET_ID_BY_NAME =
      "Select n.entity_id AS id "
          + "FROM name_data AS n JOIN entities AS e "
          + "ON (n.domain_id=0 AND n.property_id=20 AND e.id = n.entity_id)"
          + "WHERE n.value=?";

  public static final String STMT_AND_ROLE = " AND e.role=?";
  public static final String STMT_NOT_ROLE = " AND e.role!='ROLE'";
  public static final String STMT_LIMIT = " LIMIT ";

  @Override
  public List<Integer> execute(final String name, final String role, final String limit)
      throws TransactionException {
    try {
      final String stmtStr =
          STMT_GET_ID_BY_NAME
              + (role != null ? STMT_AND_ROLE : STMT_NOT_ROLE)
              + (limit != null ? STMT_LIMIT + limit : "");
      final PreparedStatement stmt = prepareStatement(stmtStr);

      stmt.setString(1, name);
      if (role != null) {
        stmt.setString(2, role);
      }
      try (ResultSet rs = stmt.executeQuery()) {
        final ArrayList<Integer> ret = new ArrayList<Integer>();
        while (rs.next()) {
          ret.add(rs.getInt("id"));
        }

        return ret;
      }
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
