/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.FileDatatype;
import org.caosdb.server.datatype.IndexedSingleValue;
import org.caosdb.server.datatype.ReferenceDatatype2;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check if the referenced entity is in the scope of the data type. E.g. if the data type is
 * 'Person' the referenced entity is to be a child of 'Person'.
 *
 * @author tf
 */
public class CheckRefidIsaParRefid extends EntityJob implements Observer {

  private void doJob() {
    try {
      if (!getEntity().hasValue()) {
        return;
      } else {
        getEntity().parseValue();
        if (getEntity().getEntityStatus() == EntityStatus.UNQUALIFIED) {
          return;
        }
      }

      if (getEntity().getDatatype() instanceof ReferenceDatatype2) {
        checkRefidIsInScope(
            (ReferenceValue) getEntity().getValue(),
            (ReferenceDatatype2) getEntity().getDatatype(),
            getEntity());
      } else if (getEntity().getDatatype() instanceof AbstractCollectionDatatype) {
        final AbstractCollectionDatatype dt =
            (AbstractCollectionDatatype) getEntity().getDatatype();
        final CollectionValue vals = (CollectionValue) getEntity().getValue();

        if (dt.getDatatype() instanceof ReferenceDatatype2) {
          for (final IndexedSingleValue v : vals) {
            if (v != null && v.getWrapped() != null) {
              checkRefidIsInScope(
                  (ReferenceValue) v.getWrapped(),
                  (ReferenceDatatype2) dt.getDatatype(),
                  getEntity());
            }
          }
        } else if (dt.getDatatype() instanceof FileDatatype) {
          for (final IndexedSingleValue v : vals) {
            if (v != null && v.getWrapped() != null) {
              final ReferenceValue rv = (ReferenceValue) v.getWrapped();
              if (rv.getEntity() != null
                  && rv.getEntity().hasRole()
                  && rv.getEntity().getRole() == Role.File) {
              } else if (rv.getId() != null
                  && rv.getId() < 0
                  && getEntityById(rv.getId()) != null
                  && getEntityById(rv.getId()).getRole() == Role.File) {
              } else if (rv.getId() == null
                  && rv.getName() != null
                  && getEntityByName(rv.getName()) != null
                  && getEntityByName(rv.getName()).getRole() == Role.File) {
              } else if (rv.getId() != null
                  && rv.getId() > 0
                  && retrieveValidSparseEntityById(rv.getId(), rv.getVersion()).getRole()
                      == Role.File) {
              } else if (rv.getName() != null
                  && retrieveValidSparseEntityByName(rv.getName()).getRole() == Role.File) {
              } else {
                throw ServerMessages.REFERENCE_IS_NOT_ALLOWED_BY_DATATYPE;
              }
            }
          }
        }
      }
    } catch (final Message m) {
      getEntity().addMessage(m);
      getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
    }
  }

  private void checkRefidIsInScope(
      final ReferenceValue value, final ReferenceDatatype2 dt, final EntityInterface entity)
      throws Message {

    runJobFromSchedule(entity, CheckRefidValid.class);
    if (entity.getEntityStatus() == EntityStatus.UNQUALIFIED) {
      return;
    }

    if (!isSubType(value.getId(), dt.getId())) {
      throw ServerMessages.REFERENCE_IS_NOT_ALLOWED_BY_DATATYPE;
    }
  }

  private boolean isSubType(final Integer child, final Integer parent) throws Message {
    if (child.equals(parent)) {
      return true;
    } else if (child > 0 && parent > 0) {
      // check with database
      return isValidSubType(child, parent);
    } else if (child < 0) {
      // get parent of ref from container
      final EntityInterface refEntity = getEntityById(child);
      for (final Parent par : refEntity.getParents()) {
        if (par.getId() == null) {
          runJobFromSchedule(refEntity, CheckParValid.class);
        }
        if (par.getId() == null) {
          getEntity()
              .addInfo(
                  new Message(
                      MessageType.Info,
                      0,
                      "Could not resolve all parents of the entity with id "
                          + child.toString()
                          + ". Problematic parent: "
                          + (par.hasName()
                              ? par.getName()
                              : (par.hasCuid() ? par.getCuid() : (par.toString())))));
          throw ServerMessages.ENTITY_HAS_INVALID_REFERENCE;
        }
        if (isSubType(par.getId(), parent)) {
          return true;
        }
      }
      return false;
    } else {
      return false;
    }
  }

  @Override
  public final void run() {

    if (getEntity().hasDatatype()) {
      doJob();
    } else {
      getEntity().acceptObserver(this);
    }
  }

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    if (e == Entity.DATATYPE_CHANGED_EVENT && o == getEntity()) {
      doJob();
      return false;
    }
    return true;
  }
}
