/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import static org.caosdb.server.transaction.Transaction.ERROR_INTEGRITY_VIOLATION;

import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.InsertSparseEntityImpl;
import org.caosdb.server.database.exceptions.IntegrityException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Version;
import org.caosdb.server.utils.Undoable;

public class InsertSparseEntity extends BackendTransaction {

  private final EntityInterface entity;

  public InsertSparseEntity(final EntityInterface entity) {
    this.entity = entity;
  }

  private Integer oldId;

  @Override
  public void execute() throws TransactionException {
    final InsertSparseEntityImpl t = getImplementation(InsertSparseEntityImpl.class);

    final SparseEntity e = this.entity.getSparseEntity();

    try {
      t.execute(e);
    } catch (final IntegrityException exc) {
      this.entity.addError(ERROR_INTEGRITY_VIOLATION);
      throw exc;
    }

    getUndoHandler()
        .append(
            new Undoable() {

              @Override
              public void undo() {
                InsertSparseEntity.this.entity.setId(InsertSparseEntity.this.oldId);
              }

              @Override
              public void cleanUp() {}
            });
    this.entity.setId(e.id);
    this.entity.setVersion(new Version(e.versionId));
    this.entity.getVersion().setHead(true);
  }
}
