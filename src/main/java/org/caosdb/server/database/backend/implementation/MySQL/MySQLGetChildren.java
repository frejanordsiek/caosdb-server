/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetChildrenImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLGetChildren extends MySQLTransaction implements GetChildrenImpl {

  public MySQLGetChildren(final Access access) {
    super(access);
  }

  public static final String STMT_GET_CHILDREN =
      "Select child from isa_cache where parent=? and rpath=child";

  @Override
  public List<Integer> execute(final Integer entity) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_GET_CHILDREN);

      stmt.setInt(1, entity);

      ResultSet rs = null;
      try {
        rs = stmt.executeQuery();
        final ArrayList<Integer> ret = new ArrayList<Integer>();
        while (rs.next()) {
          ret.add(rs.getInt(1));
        }
        return ret;
      } finally {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
      }
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
