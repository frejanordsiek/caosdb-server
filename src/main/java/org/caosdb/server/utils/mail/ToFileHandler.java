/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils.mail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;

public class ToFileHandler implements MailHandler {

  public ToFileHandler() {}

  @Override
  public void handleMail(final Mail mail) {
    final String loc =
        CaosDBServer.getServerProperty(ServerProperties.KEY_MAIL_TO_FILE_HANDLER_LOC);
    File f = new File(loc);
    if ((f.exists() && f.isDirectory()) || !f.exists() && loc.endsWith("/")) {
      f = new File(f.getAbsolutePath() + "/OUTBOX");
    }
    if (!f.exists()) {
      f.getParentFile().mkdirs();
      try {
        f.createNewFile();
      } catch (final IOException e) {
        e.printStackTrace();
      }
    }

    PrintWriter p = null;
    FileOutputStream s = null;
    try {
      s = new FileOutputStream(f, true);
      p = new PrintWriter(s);
      p.print((new SimpleMailFormatter()).format(mail));

    } catch (final FileNotFoundException e) {
      e.printStackTrace();
    } finally {
      if (p != null) {
        p.close();
      }
      if (s != null) {
        try {
          s.close();
        } catch (final IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
