/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity;

import org.caosdb.server.entity.xml.ToElementable;
import org.jdom2.Element;

public class Message extends Exception implements Comparable<Message>, ToElementable {

  protected final String type;
  private final Integer code;
  private final String description;
  private final String body;

  private static final long serialVersionUID = -3005017964769041935L;

  public enum MessageType {
    Warning,
    Error,
    Info
  };

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public String toString() {
    return this.type.toString()
        + " ("
        + (this.code != null ? Integer.toString(this.code) : "")
        + ") - "
        + (this.description != null ? this.description : "");
  }

  @Override
  public boolean equals(final Object obj) {
    return obj.toString().equals(toString());
  }

  public final String getDescription() {
    return this.description;
  }

  public final String getBody() {
    return this.body;
  }

  public final String getType() {
    return this.type;
  }

  public Message(final String type, final String description, final String body) {
    this(type, null, description, body);
  }

  public Message(final String type, final Integer code) {
    this(type, code, null, null);
  }

  public Message(Integer code, String description) {
    this(MessageType.Info, code, description);
  }

  public Message(final MessageType type, final Integer code, final String description) {
    this(type.toString(), code, description, null);
  }

  public Message(
      final MessageType type, final Integer code, final String description, final String body) {
    this(type.toString(), code, description, body);
  }

  public Message(final String type, final Integer code, final String description) {
    this(type, code, description, null);
  }

  public Message(MessageType type, String description) {
    this(type.toString(), 0, description);
  }

  public Message(
      final String type, final Integer code, final String description, final String body) {
    this.code = code;
    this.description = description;
    this.body = body;
    this.type = type;
  }

  public Message(
      final Integer code, final String description, final String body, final MessageType type) {
    this.code = code;
    this.description = description;
    this.body = body;
    this.type = type.toString();
  }

  public Message(final String string) {
    this.code = null;
    this.description = string;
    this.body = null;
    this.type = MessageType.Info.toString();
  }

  public Integer getCode() {
    return this.code;
  }

  public Element toElement() {
    final Element e = new Element(this.type);
    if (this.code != null) {
      e.setAttribute("code", Integer.toString(this.code));
    }
    if (this.description != null) {
      e.setAttribute("description", this.description);
    }
    if (this.body != null) {
      e.setText(this.body);
    }
    return e;
  }

  @Override
  public void addToElement(final Element parent) {
    final Element e = toElement();
    parent.addContent(e);
  }

  /** Print this entity to the standard outputs. Just for debugging. */
  public final void print() {
    print("");
  }

  public final void print(final String indent) {
    System.out.println(indent + "+---| " + this.type + " |------------------------ ");
    System.out.println(
        indent + "|        Code: " + (this.code != null ? Integer.toString(this.code) : "null"));
    System.out.println(indent + "| Description: " + this.description);
    System.out.println(indent + "|        Body: " + this.body);
    System.out.println(indent + "+------------------------------------------------------ ");
  }

  @Override
  public int compareTo(final Message o) {
    final int tc = this.type.compareToIgnoreCase(o.type);
    if (tc != 0) {
      return tc;
    }
    if (this.code == o.code) {
      return 0;
    }
    if (this.code == null) {
      return -1;
    }
    if (o.code == null) {
      return +1;
    }
    return this.code.compareTo(o.code);
  }
}
