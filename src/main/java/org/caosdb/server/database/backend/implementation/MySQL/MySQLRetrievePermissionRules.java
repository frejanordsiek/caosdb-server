/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrievePermissionRulesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.permissions.PermissionRule;
import org.eclipse.jetty.util.ajax.JSON;

public class MySQLRetrievePermissionRules extends MySQLTransaction
    implements RetrievePermissionRulesImpl {

  public MySQLRetrievePermissionRules(final Access access) {
    super(access);
  }

  public static final String STMT_SELECT_PERMISSION_RULES =
      "SELECT permissions FROM permissions WHERE role=?";

  @Override
  public HashSet<PermissionRule> retrievePermissionRule(final String role)
      throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_SELECT_PERMISSION_RULES);
      stmt.setString(1, role);
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        final String json = rs.getString("permissions");
        return parse(json);
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
    return new HashSet<>();
  }

  @SuppressWarnings("unchecked")
  private HashSet<PermissionRule> parse(final String json) {
    final Object[] maps = (Object[]) JSON.parse(json);
    final HashSet<PermissionRule> ret = new HashSet<PermissionRule>();
    for (final Object map : maps) {
      ret.add(PermissionRule.parse((Map<String, String>) map));
    }

    return ret;
  }
}
