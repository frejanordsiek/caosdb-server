#!/usr/bin/env python3
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 IndiScale GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

"""Creates an XLS(X?) file from an url-encoded CSV string.
"""

import argparse
import datetime
import io
import os
import sys

import pandas as pd


def _parse_to_dataframe(tsv_file):
    """Attempts to create a valid dataframe from a TSV file.

Parameters
----------
tsv_file : path to a tsv file.

Returns
-------
out : The created dataframe.
    """
    dataframe = pd.read_csv(tsv_file, sep="\t")
    return dataframe


def _write_xls(dataframe, directory):
    """Writes a dataframe into a file.

The file is named `seleceted_data.{datetime}.xlsx`, where `{datetime}` is an
ISO8601 date and time string, formatted like "%Y-%m-%dT%H_%M_%S". The file name
does not have any magic functionality and the date and time is only there for
the user's convenience.

Parameters
----------
dataframe : pd.DataFrame
  The data frame to be written.
directory : str
  The string representation of the directory where the file shall be written.

Returns
-------
out : str
  The filename (last component of directory and basename).
    """
    now = datetime.datetime.now()
    filename = "selected_data.{time}.xlsx".format(
        time=now.strftime("%Y-%m-%dT%H_%M_%S"))
    filepath = os.path.abspath(os.path.join(directory, filename))
    try:
        dataframe.to_excel(filepath, index=False)
    except ImportError as imp_e:
        print("Error importing Python module:\n" + str(imp_e),
              file=sys.stderr)
        sys.exit(1)

    randname = os.path.basename(os.path.abspath(directory))
    filename = os.path.join(randname, filename)

    return filename


def _parse_arguments():
    """Parses the command line arguments.

    Takes into account defaults from the environment (where known).
    """
    parser = argparse.ArgumentParser(description='__doc__')
    tempdir = os.environ["SHARED_DIR"]
    parser.add_argument('-t', '--tempdir', required=False, default=tempdir,
                        help="Temporary dir for saving the result.")
    parser.add_argument('-a', '--auth-token', required=False,
                        help=("An authentication token (not needed, only for "
                              "compatibility)."))
    parser.add_argument('tsv', help="The tsv file.")
    return parser.parse_args()


def main():
    args = _parse_arguments()
    dataframe = _parse_to_dataframe(args.tsv)
    filename = _write_xls(dataframe, directory=args.tempdir)
    print(filename)


if __name__ == "__main__":
    main()
