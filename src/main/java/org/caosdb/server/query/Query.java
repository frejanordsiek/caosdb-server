/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.query;

import static org.caosdb.server.database.DatabaseUtils.bytes2UTF8;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLHelper;
import org.caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.misc.DBHelper;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.query.CQLParser.CqContext;
import org.caosdb.server.query.CQLParsingErrorListener.ParsingError;
import org.caosdb.server.transaction.TransactionInterface;
import org.jdom2.Element;
import org.slf4j.Logger;

public class Query implements QueryInterface, ToElementable, TransactionInterface {

  /** Class which represents the selection of (sub)properties. */
  public static class Selection {
    private final String selector;
    private Selection subselection = null;

    public Selection setSubSelection(final Selection sub) {
      if (this.subselection != null) {
        throw new UnsupportedOperationException("SubSelection is immutable!");
      }
      this.subselection = sub;
      return this;
    }

    /** No parsing, just sets the selector string. */
    public Selection(final String selector) {
      this.selector = selector.trim();
    }

    public String getSelector() {
      return this.selector;
    }

    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder(this.selector);
      if (this.subselection != null) {
        sb.append(".");
        sb.append(this.subselection.toString());
      }
      return sb.toString();
    }

    public Selection getSubselection() {
      return this.subselection;
    }

    public Element toElement() {
      final Element ret = new Element("Selector");
      ret.setAttribute("name", toString());
      return ret;
    }
  }

  public enum Role {
    RECORD,
    RECORDTYPE,
    PROPERTY,
    ENTITY,
    FILE,
    QUERYTEMPLATE
  }

  public enum Type {
    FIND,
    COUNT
  };

  public static final class Pattern {
    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_LIKE = 1;
    public static final int TYPE_REGEXP = 2;
    public final int type;
    public final String str;

    public Pattern(final String str, final int type) {
      this.type = type;
      this.str = str;
    }

    @Override
    public String toString() {
      switch (this.type) {
        case TYPE_LIKE:
          return this.str.replaceAll("\\*", "%").replace("_", "\\_");
        default:
          return this.str;
      }
    }
  }

  public static final class ParsingException extends Query.QueryException {

    public ParsingException(final String string) {
      super(string);
    }

    /** */
    private static final long serialVersionUID = -7142620266283649346L;
  }

  public static class QueryException extends RuntimeException {

    private static final long serialVersionUID = -7142620266283649346L;

    public QueryException(final String string) {
      super(string);
    }

    public QueryException(final Throwable t) {
      super(t);
    }
  }

  public static class IdVersionPair {
    public IdVersionPair(Integer id, String version) {
      this.id = id;
      this.version = version;
    }

    public Integer id;
    public String version;

    @Override
    public String toString() {
      if (version == null) {
        return Integer.toString(id);
      }
      return Integer.toString(id) + "@" + version;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof IdVersionPair) {
        IdVersionPair that = (IdVersionPair) obj;
        return this.id == that.id && this.version == that.version;
      }
      return false;
    }

    @Override
    public int hashCode() {
      return toString().hashCode();
    }
  }

  private boolean filterEntitiesWithoutRetrievePermisions =
      !CaosDBServer.getServerProperty(
              ServerProperties.KEY_QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS)
          .equalsIgnoreCase("FALSE");

  private Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());
  List<IdVersionPair> resultSet = null;
  private final String query;
  private Pattern entity = null;
  private Role role = null;
  private String sourceSet = null;
  private String targetSet = null;
  private List<Selection> selections = null;
  private String parseTree = null;
  private final TransactionContainer container;
  private EntityFilterInterface filter = null;
  private CQLParsingErrorListener el = null;
  private final Subject user;
  private Type type = null;
  private final ArrayList<ToElementable> messages = new ArrayList<>();
  private Access access;
  private boolean versioned = false;
  private static ICacheAccess<String, Serializable> cache =
      Cache.getCache("HIGH_LEVEL_QUERY_CACHE");
  /** Cached=true means that the results of this query have actually been pulled from the cache. */
  private boolean cached = false;
  /**
   * Cachable=false means that the results of this query cannot be used for the cache, because the
   * query evaluation contains complex permission checking which could only be cached on a per-user
   * basis (maybe in the future).
   */
  private boolean cachable = true;

  /**
   * Tags the query cache and is renewed each time the cache is being cleared, i.e. each time the
   * database is being updated.
   *
   * <p>As the name suggests, the idea is similar to the ETag header of the HTTP protocol.
   */
  private static String cacheETag = UUID.randomUUID().toString();

  public Type getType() {
    return this.type;
  }

  public Query(final String query) {
    this(query, null);
  }

  public Query(final String query, final Subject user) {
    this(query, user, null);
  }

  public Query(final String query, final Subject user, final TransactionContainer container) {
    this.container = container;
    this.query = query;
    this.user = user;
  };

  /**
   * Fill the initially empty resultSet with all entities which match the name, or the id. Then
   * calculate the transitive closure of child entities and add it to the resultSet.
   *
   * @throws SQLException
   */
  private void initResultSetWithNameIDAndChildren() throws SQLException {
    final CallableStatement callInitEntity =
        getConnection().prepareCall("call initEntity(?,?,?,?,?,?)");

    try {
      callInitEntity.setInt(1, Integer.parseInt(this.entity.toString()));
    } catch (final NumberFormatException e) {
      callInitEntity.setNull(1, Types.INTEGER);
    }
    switch (this.entity.type) {
      case Pattern.TYPE_NORMAL:
        callInitEntity.setString(2, this.entity.toString());
        callInitEntity.setNull(3, Types.VARCHAR);
        callInitEntity.setNull(4, Types.VARCHAR);
        break;
      case Pattern.TYPE_LIKE:
        callInitEntity.setNull(2, Types.VARCHAR);
        callInitEntity.setString(3, this.entity.toString());
        callInitEntity.setNull(4, Types.VARCHAR);
        break;
      case Pattern.TYPE_REGEXP:
        callInitEntity.setNull(2, Types.VARCHAR);
        callInitEntity.setNull(3, Types.VARCHAR);
        callInitEntity.setString(4, this.entity.toString());
        break;
      default:
        break;
    }
    callInitEntity.setString(5, this.sourceSet);
    callInitEntity.setBoolean(6, this.versioned);
    callInitEntity.execute();
    callInitEntity.close();
  }

  /**
   * @return The result set table name.
   * @throws QueryException
   */
  private String sourceStrategy(final String sourceSet) throws QueryException {
    try {
      this.sourceSet = sourceSet;
      initResultSetWithNameIDAndChildren();

      if (this.role != Role.QUERYTEMPLATE) {
        applyQueryTemplates(this, getSourceSet());
      }

      if (this.role != null && this.role != Role.ENTITY) {
        final RoleFilter roleFilter = new RoleFilter(this.role, "=", this.versioned);
        roleFilter.apply(this);
      }

      if (this.filter != null) {
        this.filter.apply(this);
      }

      return this.sourceSet;
    } catch (final SQLException e) {
      e.printStackTrace();
      throw new TransactionException(e);
    }
  }

  /**
   * Finds all QueryTemplates in the resultSet and applies them to the same resultSet. The ids of
   * the QueryTemplates themselves are then removed from the resultSet. If the current user doesn't
   * have the RETRIEVE:ENTITY permission for a particular QueryTemplate it will be ignored.
   *
   * @param resultSet
   * @throws SQLException
   * @throws QueryException
   */
  public void applyQueryTemplates(final QueryInterface query, final String resultSet)
      throws QueryException {
    try {
      final Map<Integer, String> queryTemplates = getQueryTemplates(query, resultSet);

      final PreparedStatement removeQTStmt =
          query.getConnection().prepareStatement("DELETE FROM `" + resultSet + "` WHERE id=?");

      // Run thru all QTs found...
      for (final Entry<Integer, String> q : queryTemplates.entrySet()) {
        // ... remove the QT from resultSet...
        removeQTStmt.setInt(1, q.getKey());
        removeQTStmt.execute();

        // ... check for RETRIEVE:ENTITY permission...
        final EntityInterface e =
            execute(new RetrieveSparseEntity(q.getKey(), null), query.getAccess()).getEntity();
        final EntityACL entityACL = e.getEntityACL();
        if (!entityACL.isPermitted(query.getUser(), EntityPermission.RETRIEVE_ENTITY)) {
          // ... and ignore if not.
          continue;
        }

        // ... apply them...
        final Query subQuery = new Query(q.getValue(), query.getUser());
        subQuery.setAccess(query.getAccess());
        subQuery.parse();

        // versioning for QueryTemplates is not supported and probably never will.
        final String subResultSet = subQuery.executeStrategy(false);

        // ... and merge the resultSets.
        union(query, resultSet, subResultSet);
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    }
  }

  private static void union(final QueryInterface query, final String target, final String source)
      throws SQLException {
    final PreparedStatement unionStmt =
        query.getConnection().prepareCall("call calcUnion('" + target + "','" + source + "')");
    unionStmt.execute();
  }

  /**
   * Return the definitions of all QueryTemplates in the resultSet as a List.
   *
   * @param resultSet
   * @return A list of query strings.
   * @throws SQLException
   */
  private static Map<Integer, String> getQueryTemplates(
      final QueryInterface query, final String resultSet) throws SQLException {
    ResultSet rs = null;
    try {
      final HashMap<Integer, String> ret = new HashMap<Integer, String>();
      final CallableStatement stmt =
          query
              .getConnection()
              .prepareCall(
                  "SELECT q.id, q.definition FROM query_template_def AS q INNER JOIN `"
                      + resultSet
                      + "` AS r ON (r.id=q.id);");
      rs = stmt.executeQuery();
      while (rs.next()) {
        ret.put(rs.getInt("id"), rs.getString("definition"));
      }
      return ret;
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * @return The result set table name.
   * @throws QueryException
   */
  private String targetStrategy(final String targetSet) throws QueryException {
    this.targetSet = targetSet;

    // set entities table as source
    this.sourceSet = "entities";

    if (this.filter != null) {

      // apply filters
      this.filter.apply(this);

      this.sourceSet = null;
    }

    // filter by role
    if (this.role != null && this.role != Role.ENTITY) {
      final RoleFilter roleFilter = new RoleFilter(this.role, "=", this.versioned);
      roleFilter.apply(this);
    }

    return this.targetSet;
  }

  private MySQLHelper getMySQLHelper(final Access access) {
    final DBHelper h = access.getHelper("MySQL");
    if (h == null) {
      final MySQLHelper ret = new MySQLHelper();
      access.setHelper("MySQL", ret);
      return ret;
    } else {
      return (MySQLHelper) h;
    }
  }

  private String initQuery(boolean versioned) throws QueryException {
    String sql = "call initQuery(" + versioned + ")";
    try (final CallableStatement callInitQuery = getConnection().prepareCall(sql)) {
      ResultSet initQueryResult = null;
      initQueryResult = callInitQuery.executeQuery();
      if (!initQueryResult.next()) {
        throw new QueryException("No resultSet table created.");
      }
      return bytes2UTF8(initQueryResult.getBytes("tablename"));
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
  }

  /**
   * Parse the query and run optimize() if the parameter `optimize` is true.
   *
   * @param optimize whether to run optimize() immediately.
   * @throws ParsingException
   */
  public void parse(boolean optimize) throws ParsingException {
    final long t1 = System.currentTimeMillis();
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    parser.removeErrorListeners();
    this.el = new CQLParsingErrorListener(CQLLexer.UNKNOWN_CHAR);
    parser.addErrorListener(this.el);
    final CqContext cq = parser.cq();
    if (this.el.hasErrors()) {
      throw new ParsingException("Parsing finished with errors.");
    }

    this.entity = cq.e;
    this.versioned = cq.v != VersionFilter.UNVERSIONED;
    this.role = cq.r;
    this.parseTree = cq.toStringTree(parser);
    this.type = cq.t;
    this.filter = cq.filter;
    this.selections = cq.s;
    final long t2 = System.currentTimeMillis();
    if (t2 - t1 > 1000) {
      addBenchmark("LONG_PARSING: " + this.query, t2 - t1);
    }

    if (optimize) {
      optimize();
    }
  }

  /**
   * Parse the query and run optimize() immediately.
   *
   * @throws ParsingException
   */
  public void parse() throws ParsingException {
    parse(true);
  }

  /**
   * Optimize the query after parsing. The optimization is purely based on formal rules.
   *
   * <p>Implemented rules:
   *
   * <ol>
   *   <li>FIND * -> FIND ENTITY (which basically prevents to copy the complete entity table just to
   *       read out the ids immediately).
   * </ol>
   */
  public void optimize() {
    // basic optimization
    if (this.entity != null
        && this.entity.type == Pattern.TYPE_LIKE
        && this.entity.str.equals("*")) {
      this.entity = null;
      if (this.role == null) {
        this.role = Role.ENTITY;
      }
    }
  }

  private String executeStrategy(boolean versioned) throws QueryException {
    if (this.entity != null) {
      return sourceStrategy(initQuery(versioned));
    } else if (this.role == Role.ENTITY && this.filter == null) {
      return "entities";
    } else {
      return targetStrategy(initQuery(versioned));
    }
  }

  /**
   * Generate a SQL statement which reads out the resulting ids (and version ids if `versioned` is
   * true).
   *
   * <p>If the parameter `resultSetTableName` is "entities" actually the entity_version table is
   * used to fetch all ids.
   *
   * @param resultSetTableName name of the table with all the resulting entities
   * @param versioned whether the query was versioned
   * @return an SQL statement
   * @throws QueryException
   */
  private String generateSelectStatementForResultSet(
      final String resultSetTableName, boolean versioned) {
    if (resultSetTableName.equals("entities")) {
      return "SELECT entity_id AS id"
          + (versioned ? ", version AS version" : "")
          + " FROM entity_version"
          + (versioned ? "" : " WHERE `_iversion` = 1");
    }
    return "SELECT results.id AS id"
        + (versioned ? ", ev.version AS version" : "")
        + " FROM `"
        + resultSetTableName
        + "` AS results"
        + (versioned
            ? " JOIN entity_version AS ev ON (results.id = ev.entity_id AND results._iversion = ev._iversion)"
            : "");
  }

  /**
   * Return a list of all resulting entities (versions of entities if `versioned` is true).
   *
   * @param resultSetTableName name of the table with all the resulting entities
   * @param versioned whether the query was versioned
   * @return list of results of this query.
   * @throws QueryException
   */
  private List<IdVersionPair> getResultSet(final String resultSetTableName, boolean versioned)
      throws QueryException {
    ResultSet finishResultSet = null;
    try {
      final String sql = generateSelectStatementForResultSet(resultSetTableName, versioned);
      final PreparedStatement finish = getConnection().prepareStatement(sql);
      finishResultSet = finish.executeQuery();
      final List<IdVersionPair> rs = new LinkedList<>();
      while (finishResultSet.next()) {
        final String version = versioned ? finishResultSet.getString("version") : null;
        rs.add(new IdVersionPair(finishResultSet.getInt("id"), version));
      }
      return rs;
    } catch (final SQLException e) {
      throw new QueryException(e);
    } finally {
      if (finishResultSet != null) {
        try {
          finishResultSet.close();
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Whether the transaction allows this query instance to use the query cache. This is controlled
   * by the "cache" flag.
   *
   * @see {@link NoCache}
   * @return true if caching is encouraged.
   */
  private boolean useCache() {
    return getAccess().useCache();
  }

  /**
   * Execute the query.
   *
   * <p>First try the cache and only then use the back-end.
   *
   * @param access
   * @return
   * @throws ParsingException
   */
  public Query execute(final Access access) throws ParsingException {
    parse();
    setAccess(access);
    if (useCache()) {
      this.resultSet = getCached(getCacheKey());
    }

    if (this.resultSet == null) {
      executeNoCache(access);
      if (this.cachable) {
        setCache(getCacheKey(), this.resultSet);
      }
      this.logger.debug("Uncached query {}", this.query);
    } else {
      this.logger.debug("Using cached result for {}", this.query);
      this.cached = true;
    }

    this.resultSet = filterEntitiesWithoutRetrievePermission(this.resultSet);

    // Fill resulting entities into container
    if (this.container != null && this.type == Type.FIND) {
      for (final IdVersionPair p : this.resultSet) {

        if (p.id > 99) {
          final Entity e = new RetrieveEntity(p.id, p.version);

          // if query has select-clause:
          if (this.selections != null && !this.selections.isEmpty()) {
            e.addSelections(this.selections);
          }
          this.container.add(e);
        }
      }
    }
    return this;
  }

  /** Remove all cached queries from the cache. */
  public static void clearCache() {
    cacheETag = UUID.randomUUID().toString();
    cache.clear();
  }

  /**
   * Cache a query result.
   *
   * @param key
   * @param resultSet
   */
  private void setCache(String key, List<IdVersionPair> resultSet) {
    synchronized (cache) {
      if (resultSet instanceof Serializable) {
        cache.put(key, (Serializable) resultSet);
      } else {
        cache.put(key, new ArrayList<>(resultSet));
      }
    }
  }

  /**
   * Retrieve a result set of entity ids (and the version) from the cache.
   *
   * @param key
   * @return
   */
  @SuppressWarnings("unchecked")
  private List<IdVersionPair> getCached(String key) {
    return (List<IdVersionPair>) cache.get(key);
  }

  protected void executeNoCache(Access access) {
    try {
      this.resultSet = getResultSet(executeStrategy(this.versioned), this.versioned);
    } finally {
      cleanUp();
    }
  }

  private void addWarning(final String w) {
    this.messages.add(new Message(MessageType.Warning, 0, w));
  }

  private void cleanUp() {
    if (getConnection() != null) {
      ResultSet rs = null;
      try {
        rs = getConnection().prepareCall("call cleanUpQuery()").executeQuery();
        while (rs.next()) {
          addWarning(bytes2UTF8(rs.getBytes("warning")));
        }
      } catch (final SQLException e) {
        throw new QueryException(e);
      } finally {
        try {
          if (rs != null) {
            rs.close();
          }
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void setAccess(final Access access) {
    this.access = access;
  }

  /**
   * Filter out all entities which may not be retrieved by this user due to a missing RETRIEVE
   * permission. This one is also designed for filtering of intermediate results.
   *
   * @param resultSet
   * @throws SQLException
   * @throws TransactionException
   */
  public void filterEntitiesWithoutRetrievePermission(final String resultSet)
      throws SQLException, TransactionException {
    if (!filterEntitiesWithoutRetrievePermisions) {
      return;
    }
    cachable = false;
    try (final Statement stmt = this.getConnection().createStatement()) {
      final ResultSet rs = stmt.executeQuery("SELECT id from `" + resultSet + "`");
      final List<Integer> toBeDeleted = new LinkedList<Integer>();
      while (rs.next()) {
        final long t1 = System.currentTimeMillis();
        final Integer id = rs.getInt("id");
        if (id > 99
            && !execute(new RetrieveSparseEntity(id, null), this.getAccess())
                .getEntity()
                .getEntityACL()
                .isPermitted(this.getUser(), EntityPermission.RETRIEVE_ENTITY)) {
          toBeDeleted.add(id);
        }
        final long t2 = System.currentTimeMillis();
        this.addBenchmark("filterEntitiesWithoutRetrievePermission", t2 - t1);
      }
      rs.close();
      for (final Integer id : toBeDeleted) {
        stmt.execute("DELETE FROM `" + resultSet + "` WHERE id = " + id);
      }
    }
  }

  /**
   * Filter out all entities which may not be retrieved by this user due to a missing RETRIEVE
   * permission. This one is for the filtering of the final result set and not for the filtering of
   * any intermediate results.
   *
   * @param entities
   * @throws TransactionException
   * @return the filtered list.
   */
  private List<IdVersionPair> filterEntitiesWithoutRetrievePermission(
      final List<IdVersionPair> entities) throws TransactionException {
    if (!filterEntitiesWithoutRetrievePermisions) {
      return entities;
    }

    List<IdVersionPair> result = new ArrayList<>();
    final Iterator<IdVersionPair> iterator = entities.iterator();
    while (iterator.hasNext()) {
      final long t1 = System.currentTimeMillis();
      final IdVersionPair next = iterator.next();
      if (next.id > 99
          && execute(new RetrieveSparseEntity(next.id, next.version), getAccess())
              .getEntity()
              .getEntityACL()
              .isPermitted(getUser(), EntityPermission.RETRIEVE_ENTITY)) {
        result.add(next);
      }
      final long t2 = System.currentTimeMillis();
      addBenchmark("filterEntitiesWithoutRetrievePermission", t2 - t1);
    }
    return result;
  }

  @Override
  public String toString() {
    return this.query;
  }

  @Override
  public String getSourceSet() {
    return this.sourceSet;
  }

  @Override
  public Connection getConnection() {
    try {
      return getMySQLHelper(getAccess()).getConnection();
    } catch (final SQLException e) {
      e.printStackTrace();
    } catch (final ConnectionException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public Access getAccess() {
    return this.access;
  }

  /** @return the number of entities in the resultset. Might be updated by the filters. */
  @Override
  public int getTargetSetCount() {
    return this.targetSetCount;
  }

  public void setTargetSetCount(final Integer c) {
    this.targetSetCount = c;
  }

  private int targetSetCount = -1;

  private TransactionBenchmark benchmark;

  @Override
  public void addToElement(final Element parent) {
    final Element ret = new Element("Query");
    if (this.query == null) {
      parent.addContent(ret);
      return;
    }
    ret.setAttribute("string", this.query);
    if (this.resultSet != null) {
      ret.setAttribute("results", Integer.toString(this.resultSet.size()));
    } else {
      ret.setAttribute("results", "0");
    }
    ret.setAttribute("cached", Boolean.toString(this.cached));
    ret.setAttribute("etag", cacheETag);

    final Element parseTreeElem = new Element("ParseTree");
    if (this.el.hasErrors()) {
      for (final ParsingError m : this.el.getErrors()) {
        parseTreeElem.addContent(m.toElement());
      }
    } else {
      parseTreeElem.setText(this.parseTree);
    }
    ret.addContent(parseTreeElem);

    final Element roleElem = new Element("Role");
    if (this.role != null) {
      roleElem.setText(this.role.toString());
    }
    ret.addContent(roleElem);

    final Element entityElem = new Element("Entity");
    try {
      entityElem.setText(this.entity.toString());
    } catch (final NullPointerException exc) {
    }
    ret.addContent(entityElem);

    if (this.filter != null) {
      final Element filterElem = new Element("Filter");
      filterElem.addContent(this.filter.toElement());
      ret.addContent(filterElem);
    }

    for (final ToElementable m : this.messages) {
      m.addToElement(ret);
    }
    if (getSelections() != null && !getSelections().isEmpty()) {
      final Element selection = new Element("Selection");
      for (final Selection s : getSelections()) {
        selection.addContent(s.toElement());
      }
      ret.addContent(selection);
    }

    parent.addContent(ret);
  }

  @Override
  public Subject getUser() {
    return this.user;
  }

  @Override
  public Query getQuery() {
    return this;
  }

  @Override
  public String getTargetSet() {
    return this.targetSet;
  }

  @Override
  public void execute() throws Exception {
    execute(getAccess());
  }

  public List<Selection> getSelections() {
    return this.selections;
  }

  @Override
  public void addBenchmark(final String str, final long time) {
    getTransactionBenchmark()
        .addMeasurement(this.getClass().getSimpleName().toString() + "." + str, time);
  }

  @Override
  public TransactionBenchmark getTransactionBenchmark() {
    if (benchmark == null) {
      if (container != null) {
        benchmark = container.getTransactionBenchmark().getBenchmark(getClass());
      } else {
        benchmark = TransactionBenchmark.getRootInstance().getBenchmark(getClass());
      }
    }
    return benchmark;
  }

  @Override
  public boolean isVersioned() {
    return this.versioned;
  }

  /**
   * Return a key for the query cache. The key should describe the query with all the filters but
   * without the FIND, COUNT and SELECT ... FROM parts.
   *
   * @return A Cache key.
   */
  String getCacheKey() {
    StringBuilder sb = new StringBuilder();
    if (this.versioned) sb.append("versioned");
    if (this.role != null) sb.append(this.role.toString());
    if (this.entity != null) sb.append(this.entity.toString());
    if (this.filter != null) sb.append(this.filter.getCacheKey());
    return sb.toString();
  }

  public Pattern getEntity() {
    return this.entity;
  }

  public Role getRole() {
    return this.role;
  }

  /**
   * Return the ETag.
   *
   * <p>The ETag tags the query cache and is renewed each time the cache is being cleared, i.e. each
   * time the database is being updated.
   *
   * @return The ETag
   */
  public static String getETag() {
    return cacheETag;
  }
}
