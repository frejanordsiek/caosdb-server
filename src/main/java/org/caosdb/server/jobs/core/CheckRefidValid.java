/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 *   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.database.exceptions.EntityWasNotUniqueException;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.IndexedSingleValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether a reference property is pointing to a valid entity.
 *
 * @author tf
 */
public class CheckRefidValid extends EntityJob implements Observer {
  @Override
  public final void run() {
    try {
      if (assureReference(getEntity())) {
        if (getEntity().hasValue()) {

          // parse referenced id
          getEntity().parseValue();
          if (getEntity().getEntityStatus() == EntityStatus.UNQUALIFIED) {
            return;
          }

          if (getEntity().isReference()) {
            checkRefValue((ReferenceValue) getEntity().getValue());
          } else if (getEntity().isReferenceList()) {
            final CollectionValue vals = (CollectionValue) getEntity().getValue();
            for (final IndexedSingleValue v : vals) {
              if (v != null && v.getWrapped() != null) {
                checkRefValue((ReferenceValue) v.getWrapped());
              }
            }
          }
        }
      }
    } catch (final Message m) {
      getEntity().addError(m);
      getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
    } catch (final EntityDoesNotExistException e) {
      getEntity().addError(ServerMessages.REFERENCED_ENTITY_DOES_NOT_EXIST);
      getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
    } catch (final EntityWasNotUniqueException e) {
      getEntity().addError(ServerMessages.REFERENCE_NAME_DUPLICATES);
      getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
    }
  }

  private void checkRefValue(final ReferenceValue ref) throws Message {
    if (ref.getId() != null) {
      if (ref.getId() >= 0) {
        final EntityInterface referencedValidEntity =
            retrieveValidSparseEntityById(ref.getId(), ref.getVersion());
        assertAllowedToUse(referencedValidEntity);

        // link the entity as versioned entity iff the reference specified a version
        ref.setEntity(referencedValidEntity, ref.getVersion() != null);

      } else {

        // is the referenced entity yet linked to this refid
        // property?
        if (ref.getEntity() == null) {

          // link the entity with the corresponding
          // negative id to this reference object
          final EntityInterface referencedEntity = getEntityById(ref.getId());
          if (referencedEntity != null) {
            assertAllowedToUse(referencedEntity);

            // link the entity as versioned entity iff the reference specified a version
            ref.setEntity(referencedEntity, ref.getVersion() != null);
          } else {
            throw ServerMessages.REFERENCED_ENTITY_DOES_NOT_EXIST;
          }
        }
        ref.getEntity().acceptObserver(this);
        checkRefEntity(ref);
      }
    } else if (ref.getName() != null) {
      // is the referenced entity yet linked to this
      // refid property?
      if (ref.getEntity() == null) {
        // the entity is in this container?
        final EntityInterface referencedEntity = getEntityByName(ref.getName());

        if (referencedEntity != null) {
          assertAllowedToUse(referencedEntity);

          // link the entity as versioned entity iff the reference specified a version
          ref.setEntity(referencedEntity, ref.getVersion() != null);
          if (checkRefEntity(ref)) {
            ref.getEntity().acceptObserver(this);
          }
        } else {
          final EntityInterface referencedValidEntity =
              retrieveValidSparseEntityByName(ref.getName());
          assertAllowedToUse(referencedValidEntity);

          // link the entity as versioned entity iff the reference specified a version
          ref.setEntity(referencedValidEntity, ref.getVersion() != null);
        }
      }
    }
  }

  private void assertAllowedToUse(final EntityInterface referencedEntity) throws Message {
    checkPermission(referencedEntity, EntityPermission.USE_AS_REFERENCE);
  }

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    if ((e == Entity.DATATYPE_CHANGED_EVENT || e == Entity.ENTITY_STATUS_CHANGED_EVENT)
        && o == getEntity()) {
      return checkRefEntity((ReferenceValue) getEntity().getValue());
    }
    return true;
  }

  private boolean checkRefEntity(final ReferenceValue ref) {

    if (ref.getEntity().hasEntityStatus()) {
      switch (ref.getEntity().getEntityStatus()) {
        case UNQUALIFIED:
          getEntity().addError(ServerMessages.ENTITY_HAS_INVALID_REFERENCE);
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
          return false;
        case DELETED:
        case NONEXISTENT:
          getEntity().addError(ServerMessages.REFERENCED_ENTITY_DOES_NOT_EXIST);
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
          return false;
        default:
          return true;
      }
    }
    return true;
  }

  /**
   * Return true if this is a reference or a list of reference property.
   *
   * <p>If the data type is not present (yet), append a data type listener which calls this job
   * again when the data type is present.
   */
  private final boolean assureReference(final EntityInterface entity) {
    if (entity.hasDatatype()) {
      return entity.isReference() || entity.isReferenceList();
    } else {
      entity.acceptObserver(this);
    }
    return false;
  }
}
