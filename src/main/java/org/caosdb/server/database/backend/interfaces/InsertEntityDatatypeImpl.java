package org.caosdb.server.database.backend.interfaces;

import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.utils.Undoable;

public interface InsertEntityDatatypeImpl extends BackendTransactionImpl, Undoable {
  public abstract void execute(SparseEntity entity);
}
