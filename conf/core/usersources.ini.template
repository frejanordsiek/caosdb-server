# -*- mode:conf; -*-

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

# This file configures external authentication providers. The CaosDB realm is
# always available (without being defined here).

# `realms` is a comma and/or space separated list of realms which users can 
# use for authentication
# Currently available: PAM
realms = PAM

# This is the default realm, to be used when no other realms is specified
defaultRealm = PAM

# Each realm has one section with specific options.  The options for a specific 
# realm can be looked up in that realm's documentation.
#
# Hint: Realms are implemented by classes which are typically in the
# org.caosdb.server.accessControl.Pam package and implement the UserSource interface.

# Options for authentication against Linux' PAM.
[PAM]
class = org.caosdb.server.accessControl.Pam
# The script which does the actual checking.
; pam_script = ./misc/pam_authentication/pam_authentication.sh
default_status = ACTIVE
# Only users which fulfill these criteria are accepted.
;include.user = [uncomment and put your users here]
;include.group = [uncomment and put your groups here]
;exclude.user = [uncomment and put excluded users here]
;exclude.group = [uncomment and put excluded groups here]

# It is typically necessary to add at least one admin
;user.[uncomment a set a username here].roles = administration
