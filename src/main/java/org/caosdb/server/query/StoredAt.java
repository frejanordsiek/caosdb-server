/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import static java.sql.Types.VARCHAR;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.caosdb.server.query.Query.QueryException;
import org.jdom2.Element;

public class StoredAt implements EntityFilterInterface {

  public static final java.util.regex.Pattern PATTERN =
      // heading and escaped * and \ removed, then
      // (part of directory name: no \, no *)?(**)?(*)?
      Pattern.compile("((?:(?:\\\\[\\*\\\\])|[^\\*\\\\])+)?(\\*{2,})?(\\*)?");
  String location;
  final String likeLocation;
  boolean pattern_matching = false;

  public StoredAt(final String loc) {
    Path locPath = Paths.get(loc);
    this.location = locPath.normalize().toString() + (loc.endsWith("/") ? "/" : "");
    this.pattern_matching = requiresPatternMatching(this.location);

    if (this.pattern_matching) {
      this.likeLocation = convertLikeLocation(this.location);
    } else {
      this.likeLocation = null;
    }
  }

  /**
   * Does some special character escaping for LIKE query.
   *
   * <p>The following rules should take place in the future (for now, only a subset is implemented
   * correctly):
   *
   * <ul>
   *   <li>\* -> * (No special meaning in SQL, even numbers of \ are ignored.)
   *   <li>* -> %% (Wildcard, to be converted to a more complicated regex without / later.
   *       Exception: at the beginning of a search expression, convert to %.)
   *   <li>** -> % (Wildcard, also ***, ****, ... -> %)
   *   <li>_ -> \_ (Prevent special meaning in SQL)
   *   <li>% -> \% (Prevent special meaning in SQL)
   *   <li>Heading `/` is removed
   * </ul>
   *
   * @param String location The String to be converted.
   * @return The converted String.
   */
  static String convertLikeLocation(String location) {
    final String evenB = "(^|[^b*])(bb)*";
    final String singleEscapedAst = "b\\*";
    final String findSingleAst =
        (evenB + "(" + singleEscapedAst + ")?" + "\\*([^*]|$)").replace("b", "\\\\");
    final String findMultipleAst =
        (evenB + "(" + singleEscapedAst + ")?" + "\\*{2,}").replace("b", "\\\\");
    java.util.regex.Pattern singlePat = Pattern.compile(findSingleAst);

    // Simple SQL escape replacements
    String converted = location.replaceFirst("^/", "").replace("%", "\\%").replace("_", "\\_");
    // * -> %%, has to run mutliple times, because otherwise the separation between patterns would
    // need to be larger than one character
    while (singlePat.matcher(converted).find()) {
      converted = converted.replaceAll(findSingleAst, "$1$2$3%%$4");
    }
    // ** -> %
    converted = converted.replaceAll(findMultipleAst, "$1$2$3%");
    // All remaining asterisks are now guaranteed to have an odd number of backslashes in front

    // So we can now do \* -> *
    converted = converted.replaceAll(singleEscapedAst.replace("b", "\\\\"), "*");

    // Final exceptions
    if (location.startsWith("/*") && !location.startsWith("/**")) {
      converted = converted.replaceFirst("^/", "");
    } else {
      converted = converted.replaceFirst("^%{2,}", "%"); // .replaceFirst("^/", "");
    }

    return converted;
  }

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    final long t1 = System.currentTimeMillis();
    try {
      final CallableStatement callSAT = query.getConnection().prepareCall("call applySAT(?,?,?,?)");

      // The wildcards for directories are a bit tricky: * matches
      // anything
      // but the slash. But there is no such option in ordinary MySQL
      // LIKE.
      // Therefore we first include everything which matches an ordinary
      // LIKE
      // ...
      callSAT.setString(1, query.getSourceSet()); // sourceSet
      if (query.getTargetSet() != null) { // targetSet
        callSAT.setString(2, query.getTargetSet());
      } else {
        callSAT.setNull(2, VARCHAR);
      }
      if (this.location == null) { // location
        callSAT.setNull(3, VARCHAR);
      } else {
        callSAT.setString(
            3, (this.pattern_matching ? this.likeLocation : this.location.replaceFirst("^/", "")));
      }

      callSAT.setString(4, (this.pattern_matching ? "LIKE" : "="));
      callSAT.execute();

      // ... and then cancel out the wrong guys ('*' matching '/') via regexp
      if (this.pattern_matching && this.likeLocation.contains("%%")) {

        // make a MySQL-conform regexp for this.location:
        // 1) escape literal dots (. = \.)
        // 2) use Pattern.quote()
        // 3) replace \Q with ^ (MySQL doesn't know \Q)
        // 4) replace \E with $ (MySQL doesn't know \E)
        // 5) replace non-escaped %% with [^/]*
        // 6) replace non-escaped % with .*
        final String nloc =
            Pattern.quote(this.likeLocation.replace(".", "\\."))
                .replaceFirst("..$", "\\$")
                .replaceFirst("^..", "^")
                .replaceAll("(?<!\\\\)%%", "[^/]*")
                .replaceAll("(?<!\\\\)%", ".*");

        if (query.getTargetSet() != null) {
          callSAT.setString(1, query.getTargetSet());
        } else {
          callSAT.setString(1, query.getSourceSet());
        }
        callSAT.setNull(2, VARCHAR);
        callSAT.setString(3, nloc);
        callSAT.setString(4, "RLIKE");
        callSAT.execute();
      }
      callSAT.close();
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
    query.addBenchmark(this.getClass().getSimpleName(), System.currentTimeMillis() - t1);
  }

  /**
   * Tests if the location String is intended for pattern matching.
   *
   * @param String location The String to be converted.
   * @return True if the location requires pattern matching for a LIKE query, false otherwise.
   */
  static boolean requiresPatternMatching(String location) {
    // Looking for *, preceded by no or an even number of backslashes
    String requiredPatternStr = "(^|[^b])(bb)*\\*";
    requiredPatternStr = requiredPatternStr.replace("b", "\\\\");
    java.util.regex.Pattern REQPATTERN = Pattern.compile(requiredPatternStr);
    final Matcher m = REQPATTERN.matcher(location);
    return m.find();
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("StoredAt");
    ret.setAttribute("location", this.location);
    return ret;
  }

  /**
   * For testing purposes, create a string representing the corresponding query.
   *
   * @return The String "SAT(<location>)", where "<location>" is the query string.
   */
  @Override
  public String toString() {
    return "SAT(" + (this.pattern_matching ? this.likeLocation : this.location) + ")";
  }

  @Override
  public String getCacheKey() {
    return toString();
  }
}
