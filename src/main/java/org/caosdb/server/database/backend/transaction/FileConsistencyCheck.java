/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.FileCheckHash;
import org.caosdb.server.database.backend.interfaces.FileCheckSize;
import org.caosdb.server.database.backend.interfaces.FileExists;
import org.caosdb.server.database.backend.interfaces.FileWasModifiedAfter;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.utils.Hasher;

public class FileConsistencyCheck extends BackendTransaction {

  public static final int NONE = -1;
  public static final int OK = 0;
  public static final int FILE_DOES_NOT_EXIST = 1;
  public static final int FILE_MODIFIED = 2;
  public static final int UNKNOWN_FILE = 3;
  private final String path;
  private final String hash;
  private final Long timestamp;
  private final Hasher hasher;
  private int result = NONE;
  private final long size;

  public int getResult() {
    return this.result;
  }

  public FileConsistencyCheck(
      final String path,
      final long size,
      final String hash,
      final Long timestamp,
      final Hasher hasher) {
    this.path = path;
    this.hash = hash;
    this.timestamp = timestamp;
    this.hasher = hasher;
    this.size = size;
  }

  @Override
  protected void execute() throws TransactionException {

    final FileExists t = getImplementation(FileExists.class);
    if (!t.execute(this.path)) {
      this.result = FILE_DOES_NOT_EXIST;
    } else {

      final FileWasModifiedAfter s = getImplementation(FileWasModifiedAfter.class);
      if (s.execute(this.path, this.timestamp)) {
        // check size
        final FileCheckSize v = getImplementation(FileCheckSize.class);
        if (v.execute(this.path, this.size)) {
          this.result = OK;
        } else {
          this.result = FILE_MODIFIED;
          return;
        }

        // check hash if available
        if (this.hash != null) {
          final FileCheckHash u = getImplementation(FileCheckHash.class);
          if (u.execute(this.path, this.hasher, this.hash)) {
            this.result = OK;
          } else {
            this.result = FILE_MODIFIED;
          }
        }
      } else {
        this.result = OK;
      }
    }
  }
}
