/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs;

import org.caosdb.server.jobs.core.Strict;
import org.caosdb.server.transaction.Transaction;
import org.caosdb.server.utils.UndoHandler;

/**
 * Any {@link Transaction} of an Entity consists of sequence of stages. Jobs which have a {@link
 * JobAnnotation} can specify the transaction stage in which they are to be executed. By default,
 * any job is executed during the CHECK stage.
 * <li>INIT - The transaction is being initialized (create schedule, aquire locks (one writing
 *     thread, many reading threads permitted).
 * <li>PRE_CHECK - Prepare entities (e.g. check if any updates are to be processed, load/generate
 *     acl, cast objects into more specialized classes.)
 * <li>CHECK - Do the actual consistency checking.
 * <li>POST_CHECK - Do more consistency checking (reserved for those jobs which need the normal
 *     checks to be done already, e.g. the {@link Strict} job).
 * <li>PRE_TRANSACTION - Prepare the entities for the transaction (e.g. for paging, translate the
 *     state messages into state properties). Also, in this stage, the full read and write lock is
 *     aquired.
 * <li>TRANSACTION - Do the actual transaction.
 * <li>POST_TRANSACTION - Post-process entities (Success messages, write history, transform stage
 *     properties into messages).
 * <li>CLEANUP - Release all locks, remove temporary files, clean-up the {@link UndoHandler}s.
 * <li>ROLL_BACK - In case an error occured in any of the stages, this special stage rolls-back any
 *     changes and calls the {@link UndoHandler#undo()} of the {@link UndoHandler}s.
 *
 * @see {@link Transaction#execute()}.
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public enum TransactionStage {
  INIT,
  PRE_CHECK,
  CHECK,
  POST_CHECK,
  PRE_TRANSACTION,
  TRANSACTION,
  POST_TRANSACTION,
  CLEANUP,
  ROLL_BACK
}
