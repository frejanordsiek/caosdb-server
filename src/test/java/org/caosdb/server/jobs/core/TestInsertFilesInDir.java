package org.caosdb.server.jobs.core;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import org.caosdb.server.CaosDBServer;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestInsertFilesInDir {

  @BeforeClass
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testExclude() throws IOException {
    final InsertFilesInDir job = new InsertFilesInDir();
    job.init(null, null, null);
    job.parseValue("-e ^.*test.*$ test");
    final File testFile = new File("test.dat");
    assertTrue(job.isExcluded(testFile));
  }
}
