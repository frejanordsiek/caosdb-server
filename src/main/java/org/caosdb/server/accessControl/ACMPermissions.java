/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

public class ACMPermissions {

  public static final String PERMISSION_ACCESS_SERVER_PROPERTIES = "ACCESS_SERVER_PROPERTIES";
  public static final String PERMISSION_RETRIEVE_SERVERLOGS = "SERVERLOGS:RETRIEVE";

  public static final String PERMISSION_RETRIEVE_USER_ROLES(
      final String realm, final String username) {
    return "ACM:USER:RETRIEVE:ROLES:" + realm + ":" + username;
  }

  public static final String PERMISSION_RETRIEVE_USER_INFO(
      final String realm, final String username) {
    return "ACM:USER:RETRIEVE:INFO:" + realm + ":" + username;
  }

  public static String PERMISSION_DELETE_USER(final String realm, final String username) {
    return "ACM:USER:DELETE:" + realm + ":" + username;
  }

  public static String PERMISSION_INSERT_USER(final String realm) {
    return "ACM:USER:INSERT:" + realm;
  }

  public static String PERMISSION_UPDATE_USER_PASSWORD(final String realm, final String username) {
    return "ACM:USER:UPDATE_PASSWORD:" + realm + ":" + username;
  }

  public static String PERMISSION_UPDATE_USER_EMAIL(final String realm, final String username) {
    return "ACM:USER:UPDATE:EMAIL:" + realm + ":" + username;
  }

  public static String PERMISSION_UPDATE_USER_STATUS(final String realm, final String username) {
    return "ACM:USER:UPDATE:STATUS:" + realm + ":" + username;
  }

  public static String PERMISSION_UPDATE_USER_ENTITY(final String realm, final String username) {
    return "ACM:USER:UPDATE:ENTITY:" + realm + ":" + username;
  }

  public static String PERMISSION_UPDATE_USER_ROLES(final String realm, final String username) {
    return "ACM:USER:UPDATE:ROLES:" + realm + ":" + username;
  }

  public static String PERMISSION_INSERT_ROLE() {
    return "ACM:ROLE:INSERT";
  }

  public static String PERMISSION_UPDATE_ROLE_DESCRIPTION(final String role) {
    return "ACM:ROLE:UPDATE:DESCRIPTION:" + role;
  }

  public static String PERMISSION_RETRIEVE_ROLE_DESCRIPTION(final String role) {
    return "ACM:ROLE:RETRIEVE:DESCRIPTION:" + role;
  }

  public static String PERMISSION_DELETE_ROLE(final String role) {
    return "ACM:ROLE:DELETE:" + role;
  }

  public static String PERMISSION_UPDATE_ROLE_PERMISSIONS(final String role) {
    return "ACM:ROLE:UPDATE:PERMISSIONS:" + role;
  }

  public static String PERMISSION_RETRIEVE_ROLE_PERMISSIONS(final String role) {
    return "ACM:ROLE:RETRIEVE:PERMISSIONS:" + role;
  }

  public static String PERMISSION_ASSIGN_ROLE(final String role) {
    return "ACM:ROLE:ASSIGN:" + role;
  }
}
