package org.caosdb.server.database.backend.interfaces;

import java.util.List;
import org.caosdb.server.database.proto.SparseEntity;

/**
 * Interface for the retrieval of all known names from the backend.
 *
 * <p>The returned list contains SparseEntities which have only their name, role and acl defined.
 */
public interface GetAllNamesImpl extends BackendTransactionImpl {

  List<SparseEntity> execute();
}
