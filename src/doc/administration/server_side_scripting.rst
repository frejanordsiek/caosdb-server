Server-Side Scripting
=====================

Introduction
------------

Small computation task, like some visualization, might be easily implemented in Python or some other language, but cumbersome to integrate into the server. Furthermore, the CaosDB server should stay a general tool without burden from specific projects. Also, one might want to trigger some standardized processing task from the web interface for convenience. For these situations the "server side scripting" is intended.

Concepts
------------

The basic idea is that a script or program (script in the following) can be called to run on the server (or elsewhere in future) to do some calculations. This triggering of the script is done over the API so it can be done with any client. Input arguments can be passed to the script and the STDOUT and STDERR are returned.

Each script is executed in a temporary home directory, which is automatically clean up. However, scripts can store files in the "$SHARED" folder and for example provide users a link that allows them to download files.

Write and Install a Script
--------------------------

A server-side script must accept at least the ``--auth-token=AUTH_TOKEN`` option. All other command-line parameters which are passed to the script are not specified by the API and maybe defined by the script itself.

So a minimal bash script would be

.. code-block:: sh

    #!/bin/bash
    echo Hello, World!

thereby just ignoring the ``--auth-token`` option.

The script has to be executable and must be placed somewhere in one of the directory trees which are configured by the server config :doc:`SERVER_SIDE_SCRIPTING_BIN_DIRS <configuration>`.

Users will need the ``SCRIPTING:EXECUTE:path:to:the:script`` permission. Here the path to the script is of course relativet to the ``SERVER_SIDE_SCRIPTING_BIN_DIRS`` where it is located.

For more information see the :doc:`specification of the API <../specification/Server-side-scripting>`

Environment
------------

The script is called with several special environment variables to accommodate
for its special location.

`HOME`
^^^^^^^^^^^^
To be able to run with reduced privileges, the script has its `HOME` environment
variable set to a special directory with write access.  This directory will be
deleted after the script has terminated.  Its content is freshly copied for each
script invocation from a skeleton directory, located in the server directory, in
`scripting/home/`.  By default, this directory contains the following:

- `readme.md` :: A small text file describing the purpose of the directory.

Users of CaosDB are invited to populate the directory with whatever their
scripts need (for example a `.pycaosdb.ini` file).

Invocation
------------

Server side scripts are triggered by sending a POST to the `/scripting` resource. There are the following arguments that can be provided:

- `call`: the name of the script to be called
- `-pN`: positional arguments (e.g. `-p0`, `-p1` etc.)
- `-ONAME`: named arguments (e.g. `-Otest`, `-Onumber` etc.)

The arguments will be passed to the script.

An invocation via a button in javascript could look like:

.. code-block:: javascript

    var _make_sss_button = function (entity) {
      const script = "script.py";

      const scripting_form = $(`
        <form class="btn-group-xs ${_css_class_export_button}"
              method="POST"
              action="/scripting">
          <input type="hidden" name="call" value="${script}"/>
          <input name="-p0" value=""/>
          <button type="submit" class="btn btn-link">Start script</button>
        </form>`);
      return scripting_form[0];
    }

For more information see the :doc:`specification of the API <../specification/Server-side-scripting>`

