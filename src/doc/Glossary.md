# Glossary

## Valid ID

The ID of an existing entity. It is by definition unique among the IDs of all existing entities and is a positive integer."

## Valid Unique Existing Name

A name of an exiting entity which is unique among the names of all existing entities.

## Valid Unique Prospective Name

A name of a to-be-inserted/updated entity _e_ which is unique among the names of all other entities that are to be inserted or updated along with the entity _e_.

## Valid Unique Temporary ID

The negative integer ID of a to-be-inserted entity _e_ which is unique among the ids of all other entities that are to be inserted along with the entity _e_. 
