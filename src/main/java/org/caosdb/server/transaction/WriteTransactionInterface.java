package org.caosdb.server.transaction;

import org.caosdb.server.database.access.Access;

public interface WriteTransactionInterface extends TransactionInterface {

  public Access getAccess();
}
