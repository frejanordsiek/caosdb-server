/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.PropertyToElementStrategyTest;
import org.caosdb.server.query.Query.Selection;
import org.junit.Test;

public class RetrieveFullEntityTest {

  @Test
  public void testRetrieveSubEntities() {
    RetrieveFullEntityTransaction r =
        new RetrieveFullEntityTransaction(0) {

          /** Mock-up */
          @Override
          public void retrieveFullEntity(EntityInterface e, List<Selection> selections) {
            // The id of the referenced window
            assertEquals(1234, (int) e.getId());

            // The level of selectors has been reduced by 1
            assertEquals("description", selections.get(0).getSelector());

            e.setDescription("A heart-shaped window.");
          };
        };

    Property window = new Property(2345);
    window.setName("Window");
    window.setDatatype("Window");
    window.setValue(new ReferenceValue(1234));

    Entity house = new Entity();
    house.addProperty(window);
    ReferenceValue value = (ReferenceValue) house.getProperties().getEntityById(2345).getValue();
    assertEquals(1234, (int) value.getId());
    assertNull(value.getEntity());

    List<Selection> selections = new ArrayList<>();
    selections.add(PropertyToElementStrategyTest.parse("window.description"));

    r.retrieveSubEntities(house, selections);

    assertEquals(1234, (int) value.getId());
    assertNotNull(value.getEntity());
    assertEquals("A heart-shaped window.", value.getEntity().getDescription());
  }
}
