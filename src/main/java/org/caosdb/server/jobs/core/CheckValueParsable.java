/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;

/**
 * Check whether the value of an entity is parsable according to the entity's data type. This job
 * observes the entity's data type. If the entity's data type is valid or set to a valid data type,
 * this job performs the test. This Job also invokes the parsing. Furthermore, this job checks all
 * properties of the entity in the same manner.
 *
 * @author tf
 */
public class CheckValueParsable extends EntityJob implements Observer {
  @Override
  public final void run() {

    // Does this entity have a value which is to be parsed?this.entity
    if (getEntity().hasValue()) {

      // Does this entity have a data type?
      if (getEntity().hasDatatype()) {
        // Parse the value according to the data type of the entity.
        parseValue();
      } else {
        // This entity doesn't have a data type. Wait until it gets one.
        // Add observer to the entity's data type. See notifyObserver()
        // below.
        getEntity().acceptObserver(this);
      }
    }

    // Check all Properties
    checkProperties();
  }

  /**
   * Try parsing the value. If it fails, the parser will throw an error message. Catch the message
   * and add it to the entity.
   */
  private void parseValue() {
    try {
      getEntity().parseValue();

    } catch (final Message m) {
      getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
      getEntity().addMessage(m);
    }
  }

  /** Initialize a CheckValueParsable job for all properties and run it. */
  private void checkProperties() {
    for (final EntityInterface p : getEntity().getProperties()) {
      if (p.getEntityStatus() != EntityStatus.UNQUALIFIED) {
        // final CheckValueParsable checkProperties = new
        // CheckValueParsable();
        // checkProperties.init(getMode(), p, getContainer(),
        // getTransaction());
        // getTransaction().getSchedule().add(checkProperties);
        // getTransaction().getSchedule().runJob(checkProperties);
        appendJob(p, CheckValueParsable.class);
      }
    }
  }

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    if (e == Entity.DATATYPE_CHANGED_EVENT) {
      // the notification concerns this entity only, not its properties.
      // Therefore, not the whole test has to be run again.
      if (getEntity().hasDatatype()) {
        parseValue();
        return false;
      }
    }
    return true;
  }
}
