/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import java.util.Map;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.entity.Message;
import org.caosdb.server.transaction.RetrievePasswordValidatorTransaction;
import org.caosdb.server.transaction.RetrieveUserTransaction;
import org.caosdb.server.utils.ServerMessages;

public class InternalUserSource implements UserSource {

  @Override
  public Set<String> resolveRolesForUsername(final String username) {
    try {
      final RetrieveUserTransaction t = new RetrieveUserTransaction(getName(), username);
      t.execute();
      return t.getRoles();
    } catch (final Exception e) {
      throw new AuthorizationException(e);
    }
  }

  @Override
  public void setMap(final Map<String, String> map) {
    // this is not applicable for the internal user source. All
    // configuration is done otherwise.
  }

  @Override
  public String getName() {
    return "CaosDB";
  }

  @Override
  public boolean isUserExisting(final String username) {
    try {
      final RetrievePasswordValidatorTransaction t =
          new RetrievePasswordValidatorTransaction(username);
      t.execute();
      return true;
    } catch (final Message m) {
      if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
        return false;
      }
      throw new AuthenticationException(m);
    } catch (final Exception e) {
      throw new AuthenticationException(e);
    }
  }

  @Override
  public UserStatus getDefaultUserStatus(final String username) {
    return UserStatus.INACTIVE;
  }

  @Override
  public String getDefaultUserEmail(final String username) {
    return null;
  }

  @Override
  public boolean isValid(final String username, final String password) {
    final RetrievePasswordValidatorTransaction t =
        new RetrievePasswordValidatorTransaction(username);
    try {
      t.execute();
    } catch (final Exception e) {
      if (e == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
        return false;
      }
      e.printStackTrace();
      return false;
    }
    final CredentialsValidator<String> validator = t.getValidator();
    if (validator.isValid(password)) {
      return true;
    } else {
      return false;
    }
  }
}
