/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.ArrayList;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.CacheableBackendTransaction;
import org.caosdb.server.database.DatabaseUtils;
import org.caosdb.server.database.backend.interfaces.RetrievePropertiesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoProperty;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.wrapper.Property;

public class RetrieveProperties
    extends CacheableBackendTransaction<String, ArrayList<ProtoProperty>> {

  private final EntityInterface entity;
  public static final String CACHE_REGION = "BACKEND_EntityProperties";
  private static final ICacheAccess<String, ArrayList<ProtoProperty>> cache =
      Cache.getCache(CACHE_REGION);

  /**
   * To be called by DeleteEntityProperties on execution.
   *
   * @param id
   */
  protected static void removeCached(final String idVersion) {
    if (idVersion != null && cache != null) {
      cache.remove(idVersion);
    }
  }

  public RetrieveProperties(final EntityInterface entity) {
    super(cache);
    this.entity = entity;
  }

  @Override
  public ArrayList<ProtoProperty> executeNoCache() throws TransactionException {
    final RetrievePropertiesImpl t = getImplementation(RetrievePropertiesImpl.class);
    return t.execute(this.entity.getId(), this.entity.getVersion().getId());
  }

  @Override
  protected void process(final ArrayList<ProtoProperty> t) throws TransactionException {
    this.entity.getProperties().clear();

    final ArrayList<Property> props = DatabaseUtils.parseFromProtoProperties(t);

    for (final Property p : props) {
      // retrieve sparse properties stage 1
      final RetrieveSparseEntity t1 = new RetrieveSparseEntity(p);
      execute(t1);

      // add default data type for record types
      if (!p.hasDatatype() && p.getRole() == Role.RecordType) {
        p.setDatatype(p.getName());
      }

      // retrieve sparse properties stage 2
      for (final EntityInterface subP : p.getProperties()) {
        final RetrieveSparseEntity t2 = new RetrieveSparseEntity(subP);
        execute(t2);

        // add default data type for record types
        if (!subP.hasDatatype() && subP.getRole() == Role.RecordType) {
          subP.setDatatype(subP.getName());
        }
      }
    }

    DatabaseUtils.transformToDeepPropertyTree(this.entity, props);
  }

  @Override
  protected String getKey() {
    return this.entity.getIdVersion();
  }
}
