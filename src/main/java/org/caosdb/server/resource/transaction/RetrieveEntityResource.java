/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.resource.transaction;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.resource.AbstractCaosDBServerResource;
import org.caosdb.server.transaction.Retrieve;
import org.jdom2.Document;
import org.jdom2.Element;
import org.restlet.representation.Representation;

/**
 * Handles GET requests for different subclasses which all have in common that they retrieve
 * Entities (plus other information in some cases).
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public abstract class RetrieveEntityResource extends AbstractCaosDBServerResource {

  /**
   * Parse the segment which specifies the entities which are to be retrieved
   *
   * @param container
   */
  protected void handleRetrieveContainer(RetrieveContainer container) {

    for (final String item : getRequestedItems()) {
      String[] elem = item.split("@", 2);
      Integer id = null;
      String name = null;
      String version = null;
      try {
        id = Integer.parseInt(elem[0]);
      } catch (NumberFormatException e) {
        name = elem[0];
      }
      if (elem.length > 1) {
        version = elem[1];
      }

      if (id != null) {
        container.add(id, version);
      } else {
        container.add(name);
      }
    }
  }

  /** Handle the GET request. */
  @Override
  protected final Representation httpGetInChildClass()
      throws ConnectionException, IOException, SQLException, CaosDBException,
          NoSuchAlgorithmException, Exception {

    final RetrieveContainer entityContainer =
        new RetrieveContainer(getUser(), getTimestamp(), getSRID(), getFlags());

    handleRetrieveContainer(entityContainer);

    final long t2 = System.currentTimeMillis();
    final Retrieve retrieve = new Retrieve(entityContainer);
    retrieve.execute();

    final long t3 = System.currentTimeMillis();
    entityContainer
        .getTransactionBenchmark()
        .addMeasurement(
            getClass().getSimpleName() + ".httpGetInChildClass#retrieve.execute", t3 - t2);

    final Element rootElem = generateRootElement();
    entityContainer.addToElement(rootElem);
    final Document doc = new Document();
    doc.setRootElement(rootElem);

    final long t4 = System.currentTimeMillis();
    entityContainer
        .getTransactionBenchmark()
        .addMeasurement(
            getClass().getSimpleName() + ".httpGetInChildClass#element_handling", t4 - t3);

    return ok(doc);
  }
}
