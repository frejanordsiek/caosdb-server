package org.caosdb.server.permissions;

import java.util.Collection;

public class EntityACLFactory extends AbstractEntityACLFactory<EntityACL> {

  @Override
  protected EntityACL create(final Collection<EntityACI> acis) {
    return new EntityACL(acis);
  }
}
