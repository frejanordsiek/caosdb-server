/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils.mail;

import java.util.Date;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;

public class Mail {

  private static MailHandler handler = null;

  static {
    String clazz = CaosDBServer.getServerProperty(ServerProperties.KEY_MAIL_HANDLER_CLASS);
    try {
      @SuppressWarnings("unchecked")
      Class<? extends MailHandler> mailHandlerClass =
          (Class<? extends MailHandler>) Class.forName(clazz);
      handler = mailHandlerClass.getDeclaredConstructor().newInstance();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private String fromName;
  private String fromAddr;
  private String toName;
  private String toAddr;
  private String subject;
  private String textData;
  private Date date = null;

  public Mail(
      String fromName,
      String fromAddr,
      String toName,
      String toAddr,
      String subject,
      String textData) {
    this.setFromName(fromName);
    this.setFromAddr(fromAddr);
    this.setToName(toName);
    this.setToAddr(toAddr);
    this.setSubject(subject);
    this.setTextData(textData);
  }

  public void send() {
    setDate(new Date());
    handler.handleMail(this);
  }

  public String getFromName() {
    return fromName;
  }

  public void setFromName(String fromName) {
    this.fromName = fromName;
  }

  public String getFromAddr() {
    return fromAddr;
  }

  public void setFromAddr(String fromAddr) {
    this.fromAddr = fromAddr;
  }

  public String getToName() {
    return toName;
  }

  public void setToName(String toName) {
    this.toName = toName;
  }

  public String getToAddr() {
    return toAddr;
  }

  public void setToAddr(String toAddr) {
    this.toAddr = toAddr;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getTextData() {
    return textData;
  }

  public void setTextData(String textData) {
    this.textData = textData;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }
}
