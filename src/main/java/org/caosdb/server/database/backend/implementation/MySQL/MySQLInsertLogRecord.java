/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.LogRecord;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.InsertLogRecordImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLInsertLogRecord extends MySQLTransaction implements InsertLogRecordImpl {

  public MySQLInsertLogRecord(final Access access) {
    super(access);
  }

  public static final String STMT_INSERT_LOG_RECORD =
      "INSERT INTO logging (level, logger, message, millis, logRecord) VALUES (?,?,?,?,?)";

  @Override
  public void insert(final List<LogRecord> toBeFlushed) throws TransactionException {
    try {
      final PreparedStatement prepInsert = prepareStatement(STMT_INSERT_LOG_RECORD);
      for (final LogRecord record : toBeFlushed) {
        prepInsert.setInt(1, record.getLevel().intValue());
        prepInsert.setString(2, record.getLoggerName());
        prepInsert.setString(3, record.getMessage());
        prepInsert.setLong(4, record.getMillis());
        prepInsert.setObject(5, record);
        prepInsert.executeUpdate();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
