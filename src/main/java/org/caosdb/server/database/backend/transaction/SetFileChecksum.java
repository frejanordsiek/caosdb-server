package org.caosdb.server.database.backend.transaction;

import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.SetFileChecksumImpl;
import org.caosdb.server.entity.EntityInterface;

public class SetFileChecksum extends BackendTransaction {

  private EntityInterface entity;

  public SetFileChecksum(EntityInterface entity) {
    this.entity = entity;
  }

  @Override
  protected void execute() {
    RetrieveSparseEntity.removeCached(this.entity);
    if (entity.hasFileProperties()) {
      GetFileRecordByPath.removeCached(this.entity.getFileProperties().getPath());

      final SetFileChecksumImpl t = getImplementation(SetFileChecksumImpl.class);

      t.execute(this.entity.getId(), this.entity.getFileProperties().getChecksum());
    }
  }
}
