/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.UnixFileSystem;

import org.caosdb.server.database.misc.DBHelper;
import org.caosdb.server.transaction.TransactionInterface;

public class UnixFileSystemHelper implements DBHelper {

  private final String basepath;

  public UnixFileSystemHelper(final String basepath) {
    this.basepath = basepath;
  }

  @Override
  public void setHelped(final TransactionInterface transaction) {}

  @Override
  public void cleanUp() {}

  @Override
  public void commit() throws Exception {}

  public String getBasePath() {
    return this.basepath;
  }
}
