package org.caosdb.server.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.restlet.data.Reference;

public class WebinterfaceUtilsTest {

  @Rule public ExpectedException exceptionRule = ExpectedException.none();

  @BeforeClass
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testGetWebinterfaceReference() {
    WebinterfaceUtils utils = new WebinterfaceUtils(new Reference("https://host:2345/some_path"));
    String buildNumber = utils.getBuildNumber();
    String ref = utils.getWebinterfaceURI("sub");
    String contextRoot = CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT);
    contextRoot =
        contextRoot != null && contextRoot.length() > 0
            ? "/" + contextRoot.replaceFirst("^/", "").replaceFirst("/$", "")
            : "";

    assertEquals("https://host:2345" + contextRoot + "/webinterface/" + buildNumber + "/sub", ref);
  }

  @Test
  public void testGetPublicFile() {
    WebinterfaceUtils utils = new WebinterfaceUtils(new Reference("https://host:2345/some_path"));
    assertNull(utils.getPublicFile("../"));
  }

  @Test
  public void testGetPublicFilePath() {
    WebinterfaceUtils utils = new WebinterfaceUtils(new Reference("https://host:2345/some_path"));
    assertNull(utils.getPublicFilePath("../"));
    assertNotNull(utils.getPublicFilePath("./"));
    assertNotNull(utils.getPublicFilePath("bla"));
  }
}
