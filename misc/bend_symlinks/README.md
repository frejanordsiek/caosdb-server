# About

./bend_symlinks.sh - fix broken symlinks in the internal file system

# Copyright and License Disclaimer:

    Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
    Copyright (C) 2019 IndiScale (info@indiscale.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


# Version
    0.1

# Usage

    ./bend_symlinks.sh [-D] FILE_SYSTEM_DIR TARGET_PATTERN TARGET_REPLACEMENT

        Find all broken symlinks below FILE_SYSTEM_DIR which targets match
        TARGET_PATTERN and replace the targets (i.e. overwrite the symlinks)
        with TARGET_REPLACEMENT according to the rules of sed's replace
        function.

        Print OLD_TARGET<tab>NEW_TARGET to stdout for each replaced symlink.

    ./bend_symlinks.sh -d [-D] FILE_SYSTEM_DIR OLD_TARGET_PREFIX NEW_TARGET_PREFIX

        Find all broken symlinks below FILE_SYSTEM_DIR which targets a
        path prefixed by OLD_TARGET_PREFIX and replace the targets (i.e.
        overwrite the symlinks) by changing only the directory prefix to
        NEW_TARGET_PREFIX. This is the preferred way to fix symlinks which
        targets have just been moved to another directory while the structure
        under this directory stayed the same.

        Print OLD_TARGET<tab>NEW_TARGET to stdout for each replaced symlink.

    ./bend_symlinks.sh (-h|-v)

# Parameters

    FILE_SYSTEM_DIR     A directory of the internal file system's back-end
                        storage.
                        All symlinks below this directory are being process by
                        this script.
                        E.g. '/mnt/caosdb_fs/ExperimentalData/'
    TARGET_PATTERN      A (extended sed-style) regular expression for matching
                        broken symlink targets.
    TARGET_REPLACEMENT  A (sed-style) replacement string for the new (fixed)
                        symlink targets.
    OLD_TARGET_PREFIX   The directory of old and broken symlink targets.
                        E.g. '/mnt/data/current/experiments/'
    NEW_TARGET_PREFIX   The directory of the new symlink targets.
                        E.g. '/mnt/data/archive/experiments/2019/'
    -d                  Bend all symlinks under FILE_SYSTEM_DIR which target a
                        file prefixed by OLD_TARGET_PREFIX to point to
                        NEW_TARGET_PREFIX and keep the substructure the same.
                        This is the most useful special case for the scenario
                        where orginal data files have just been moved from one
                        folder into another and the symlinks need to updated
                        accordingly.
    -D                  Dry-run: Only print what would happen.
    -h                  Print this help message and exit.
    -v                  Print the version of this script and ext.

# Examples

  1. Files have been moved from '/mnt/data/current/experiments/' to
     /mnt/data/archive/experiments/2019/'.  Execute the script in the root
     directory of the caosdb server's internal file system:

       $ ./bend_symlinks.sh -d ./ /mnt/data/current/experiments /mnt/data/archive/experiments/2019

  2. A File was renamed from '/mnt/data/procotol.pdf' to
     '/mnt/data/protocol.pdf'.  The symlink is located at
     '/mnt/caosdb_fs/procotol.pdf'.  Execute the script in the root directory
     of the caosdb server's internal file system:

       $ ./bend_symlinks.sh ./ procotol\.pdf$ protocol.pdf

  3. In order to print a table which contains the corrected name from example 2
     and which can be understood by the the
     [move_files.py](../move_files/move_files.py) script pipe the standard
     output like this.

       $ ./bend_symlinks.sh ./ procotol\.pdf$ protocol.pdf | sed -e 's/\/mnt\/data// > changes.tsv

     Then the changes.tsv file contains 'procotol.pdf<tab>protocol.pdf<EOF>'.


# Tests

Run test suite with

    $ ./test/test_suite.sh
