# Release Guidelines for the CaosDB MySQL Backend

This document specifies release guidelines in addition to the generel release
guidelines of the CaosDB Project
([RELEASE_GUIDELINES.md](https://gitlab.com/caosdb/caosdb/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* FEATURES.md is up-to-date and a public API is being declared in that document.
* CHANGELOG.md is up-to-date.
* DEPENDENCIES.md is up-to-date.

## Steps

1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process. Naming: `release-<VERSION>`

2. Check all general prerequisites.

3. Update the version property in [pom.xml](./pom.xml) (probably this means to
   remove the `-SNAPSHOT`) and in `src/doc/conf.py`.

4. Merge the release branch into the main branch.

5. Tag the latest commit of the main branch with `v<VERSION>`.

6. Delete the release branch.

7. Merge the main branch back into the dev branch.

8. Update the version property in [pom.xml](./pom.xml) for the next
   developlement round (with a `-SNAPSHOT` suffix).
