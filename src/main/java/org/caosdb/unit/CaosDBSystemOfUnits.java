/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.unit;

import de.timmfitschen.easyunits.BaseUnit;
import de.timmfitschen.easyunits.DefaultSystemOfUnitsFactory;
import de.timmfitschen.easyunits.DerivedUnit;
import de.timmfitschen.easyunits.SystemOfUnits;
import de.timmfitschen.easyunits.SystemOfUnitsException;
import de.timmfitschen.easyunits.UnknownUnitException;
import de.timmfitschen.easyunits.conversion.Prefix;
import de.timmfitschen.easyunits.parser.ParserException;

public class CaosDBSystemOfUnits {

  public static Unit getUnit(final String s) throws ParserException {
    try {
      return new WrappedUnit(system.getUnit(s));
    } catch (final UnknownUnitException e) {
      return new UnknownUnit(new BaseUnit(s));
    }
  }

  private static SystemOfUnits system = createSystemOfUnits();

  private CaosDBSystemOfUnits() {}

  private static SystemOfUnits createSystemOfUnits() {
    final DefaultSystemOfUnitsFactory f = new DefaultSystemOfUnitsFactory();
    try {
      // Prefixes
      f.add(new Prefix("E", 10, 18));
      f.add(new Prefix("P", 10, 15));
      f.add(new Prefix("T", 10, 12));
      f.add(new Prefix("G", 10, 9));
      f.add(new Prefix("M", 10, 6));
      f.add(new Prefix("k", 10, 3));
      f.add(new Prefix("h", 10, 2));
      f.add(new Prefix("da", 10, 1));
      f.add(new Prefix("d", 10, -1));
      f.add(new Prefix("c", 10, -2));
      f.add(new Prefix("m", 10, -3));
      f.add(new Prefix("µ", 10, -6));
      f.add(new Prefix("n", 10, -9));
      f.add(new Prefix("p", 10, -12));
      f.add(new Prefix("f", 10, -15));
      f.add(new Prefix("a", 10, -18));

      // Base Units
      f.add(new BaseUnit("m"));
      f.add(new BaseUnit("K"));
      f.add(new BaseUnit("s"));
      f.add(new BaseUnit("g"));

      // Derived Units
      f.add(new DerivedUnit("°C", new BaseUnit("K").add(-273.15)));

    } catch (final SystemOfUnitsException e) {
      e.printStackTrace();
      System.exit(1);
    }
    return f.create();
  }
}
