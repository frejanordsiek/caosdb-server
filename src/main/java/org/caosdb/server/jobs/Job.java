/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 *   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.transaction.GetIDByName;
import org.caosdb.server.database.backend.transaction.IsSubType;
import org.caosdb.server.database.backend.transaction.RetrieveFullEntityTransaction;
import org.caosdb.server.database.backend.transaction.RetrieveParents;
import org.caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import org.caosdb.server.database.exceptions.EntityWasNotUniqueException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.datatype.ReferenceDatatype2;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.container.ParentContainer;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.jobs.core.JobFailureSeverity;
import org.caosdb.server.transaction.Transaction;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;
import org.reflections.Reflections;

/**
 * This is a Job.
 *
 * @todo Describe me.
 */
public abstract class Job {
  /** All known Job classes, by name (actually lowercase getSimpleName()). */
  static HashMap<String, Class<? extends Job>> allClasses = null;

  private final JobConfig jobConfig = JobConfig.getInstance();

  private static List<Class<? extends Job>> loadAlways;

  private Transaction<? extends TransactionContainer> transaction = null;
  private JobFailureSeverity failureSeverity = null;
  private final TransactionStage stage;
  private EntityInterface entity = null;

  public abstract JobTarget getTarget();

  protected <S, T> HashMap<S, T> getCache(final String name) {
    return getTransaction().getCache(name);
  }

  protected ScheduledJob appendJob(final Class<? extends Job> clazz) {
    return appendJob(getEntity(), clazz);
  }

  protected String getRequestId() {
    return getContainer().getRequestId();
  }

  protected Subject getUser() {
    return getTransaction().getTransactor();
  }

  public <K extends BackendTransaction> K execute(final K t) {
    return getTransaction().execute(t, getTransaction().getAccess());
  }

  protected ScheduledJob appendJob(final EntityInterface entity, final Class<? extends Job> clazz) {
    try {
      final Job job = clazz.getDeclaredConstructor().newInstance();
      job.init(getFailureSeverity(), entity, getTransaction());
      return getTransaction().getSchedule().add(job);
    } catch (InstantiationException
        | IllegalAccessException
        | IllegalArgumentException
        | InvocationTargetException
        | NoSuchMethodException
        | SecurityException e) {
      throw new CaosDBException(e);
    }
  }

  protected EntityInterface getEntityById(final int id) {
    return getContainer().getEntityById(id);
  }

  protected EntityInterface getEntityByName(final String name) {
    return getContainer().getEntityByName(name);
  }

  protected Job() {
    if (this.getClass().isAnnotationPresent(JobAnnotation.class)) {
      this.stage = this.getClass().getAnnotation(JobAnnotation.class).stage();
    } else {
      this.stage = TransactionStage.CHECK;
    }
  }

  public final Job init(
      final JobFailureSeverity severity,
      final EntityInterface entity,
      final Transaction<? extends TransactionContainer> transaction) {
    this.failureSeverity = severity;
    this.entity = entity;
    this.transaction = transaction;
    return this;
  }

  /** to be overridden */
  protected abstract void run();

  protected void runJobFromSchedule(
      final EntityInterface entity, final Class<? extends Job> jobclass) {
    getTransaction().getSchedule().runJob(entity, jobclass);
  }

  public EntityInterface getEntity() {
    return this.entity;
  }

  protected final JobFailureSeverity getFailureSeverity() {
    return this.failureSeverity;
  }

  protected final void setFailureSeverity(final JobFailureSeverity severiy) {
    this.failureSeverity = severiy;
  }

  TransactionContainer getContainer() {
    return getTransaction().getContainer();
  }

  /**
   * Check if an entity ('child') is a direct or indirect child of another entity ('targetParent').
   *
   * <p>This assumes that the parent has either an id (persistent or temporary) or a name.
   *
   * <p>If the targetParent is not in the set of parents of the child, this method iterates through
   * the direct parents of the child.
   *
   * <p>If both entities are part of this transaction, they are resolved within this transaction.
   * Otherwise they are fetched from the database backend or the whole evaluation takes place in the
   * backend (if both have persistent ids and are not part of this transaction).
   *
   * <p>If both entities have the same id or name, the return value is true without any further
   * checks.
   *
   * @param child the child entity
   * @param targetParent the parent entity
   * @return true iff targetParent is a direct or indirect parent of the child or when the ids or
   *     names match.
   * @throws Message
   */
  protected final boolean isSubType(final EntityInterface child, final EntityInterface targetParent)
      throws EntityWasNotUniqueException {
    if (targetParent.hasId()) {
      if (targetParent.getId().equals(child.getId())) {
        return true;
      }

    } else if (targetParent.hasName()) {
      if (targetParent.getName().equals(child.getName())) {
        return true;
      }
    }

    // check direct parents of child
    ParentContainer directParents;
    if (child.hasParents()) {
      directParents = child.getParents();
    } else {
      directParents = resolve(child).getParents();
    }
    for (final Parent directParent : directParents) {
      EntityInterface resolvedDirectParent = null;
      if (directParent.hasId()) {
        resolvedDirectParent = getEntityById(directParent.getId());
      } else if (directParent.hasName()) {
        resolvedDirectParent = getEntityByName(directParent.getName());
      }

      if (resolvedDirectParent != null) {
        if (isSubType(resolvedDirectParent, targetParent)) {
          return true;
        }
      } else if (directParent.hasId()) {
        if (isValidSubType(directParent.getId(), targetParent.getId())) {
          return true;
        }
      } else if (directParent.hasName()) {
        if (isValidSubType(resolve(directParent).getId(), targetParent.getId())) {
          return true;
        }
      }
    }

    return false;
  }

  protected final boolean isValidSubType(final int child, final int parent) {
    if (!"false".equals(getContainer().getFlags().get("cache"))) {
      final HashMap<String, Boolean> isSubTypeCache = getCache("isSubType");
      final String key = child + "->" + parent;
      final Boolean cached = isSubTypeCache.get(key);
      if (cached == null) {
        final Boolean toCache = isValidSubTypeNoCache(child, parent);
        isSubTypeCache.put(key, toCache);
        return toCache;
      } else {
        return cached;
      }
    } else {
      return isValidSubTypeNoCache(child, parent);
    }
  }

  protected final boolean isValidSubTypeNoCache(final int child, final int parent) {
    return child == parent || execute(new IsSubType(child, parent)).isSubType();
  }

  protected final EntityInterface retrieveValidSparseEntityByName(final String name)
      throws Message {
    return retrieveValidSparseEntityById(retrieveValidIDByName(name), null);
  }

  protected final EntityInterface retrieveValidSparseEntityById(
      final Integer id, final String version) throws Message {

    String resulting_version = version;
    if (version == null || version.equals("HEAD")) {
      // the targeted entity version is the entity after the transaction or the
      // entity without a specific version. Thus we have to fetch the entity
      // from the container if possible.
      final EntityInterface ret = getEntityById(id);
      if (ret != null) {
        return ret;
      }
    } else if (version.startsWith("HEAD~")) {
      final EntityInterface entById = getEntityById(id);
      if (entById != null && entById.getEntityStatus() != EntityStatus.VALID) {
        // if version is HEAD~{OFFSET} with {OFFSET} > 0 and the targeted entity is is to be
        // updated, the actual offset has to be reduced by 1. HEAD always denotes the entity@HEAD
        // *after* the successful transaction, so that it is consistent with subsequent retrieves.
        final int offset = Integer.parseInt(version.substring(5)) - 1;
        if (offset == 0) {
          // special case HEAD~1
          resulting_version = "HEAD";
        } else {
          resulting_version = new StringBuilder().append("HEAD~").append(offset).toString();
        }
      }
    }

    final EntityInterface ret =
        execute(new RetrieveSparseEntity(id, resulting_version)).getEntity();
    if (ret.getEntityStatus() == EntityStatus.NONEXISTENT) {
      throw ServerMessages.ENTITY_DOES_NOT_EXIST;
    }
    return ret;
  }

  protected final EntityInterface retrieveValidEntity(final Integer id) {
    return execute(new RetrieveFullEntityTransaction(id)).getContainer().get(0);
  }

  protected final Integer retrieveValidIDByName(final String name) {
    return execute(new GetIDByName(name)).getId();
  }

  protected EntityInterface retrieveParentsOfValidEntity(final EntityInterface entity) {
    execute(new RetrieveParents(entity));
    return entity;
  }

  protected final void checkPermission(final EntityInterface entity, final Permission permission)
      throws Message {
    if (!entity.getEntityACL().isPermitted(SecurityUtils.getSubject(), permission)) {
      throw ServerMessages.AUTHORIZATION_ERROR;
    }
  }

  /**
   * Create a Job object with the given parameters.
   *
   * <p>This static method is used by other classes to create Job objects, instead of the private
   * constructor.
   *
   * @return The generated Job object.
   */
  public static Job getJob(
      final String job,
      final JobFailureSeverity severiy,
      final EntityInterface entity,
      final Transaction<? extends TransactionContainer> transaction) {
    // Fill `allClasses` with available subclasses
    scanJobClasspath();

    // Get matching class for Job and generate it.
    final Class<? extends Job> jobClass = allClasses.get(job.toLowerCase());
    return getJob(jobClass, severiy, entity, transaction);
  }

  /**
   * Initialize {@code allClasses} with all {@code Job} classes found in the classpath.
   *
   * @todo Details when this has any effect.
   */
  private static void scanJobClasspath() {
    if (allClasses == null || loadAlways == null) {
      allClasses = new HashMap<>();
      loadAlways = new ArrayList<>();
      Reflections jobPackage = new Reflections("org.caosdb.server.jobs.core");
      Set<Class<? extends Job>> allClassesSet = jobPackage.getSubTypesOf(Job.class);
      allClassesSet.addAll(jobPackage.getSubTypesOf(FlagJob.class));
      for (final Class<? extends Job> c : allClassesSet) {
        allClasses.put(c.getSimpleName().toLowerCase(), c);
        if (c.isAnnotationPresent(JobAnnotation.class)) {
          final String flagName = c.getAnnotation(JobAnnotation.class).flag();
          if (flagName.length() > 0) {
            allClasses.put(flagName.toLowerCase(), c);
          }
          if (c.getAnnotation(JobAnnotation.class).loadAlways()) {
            loadAlways.add(c);
          }
        }
      }
      // TODO merge these two parts of this function. Its the same!
      jobPackage = new Reflections("org.caosdb.server.jobs.extension");
      allClassesSet = jobPackage.getSubTypesOf(Job.class);
      for (final Class<? extends Job> c : allClassesSet) {
        allClasses.put(c.getSimpleName().toLowerCase(), c);
        if (c.isAnnotationPresent(JobAnnotation.class)) {
          final String flagName = c.getAnnotation(JobAnnotation.class).flag();
          if (flagName.length() > 0) {
            allClasses.put(flagName.toLowerCase(), c);
          }
          if (c.getAnnotation(JobAnnotation.class).loadAlways()) {
            loadAlways.add(c);
          }
        }
      }
    }
  }

  protected List<Job> loadDataTypeSpecificJobs() {
    return loadDataTypeSpecificJobs(getEntity(), getTransaction());
  }

  public List<Job> loadDataTypeSpecificJobs(
      final EntityInterface entity, final Transaction<? extends TransactionContainer> transaction) {

    return loadDataTypeSpecificJobs(entity.getDatatype(), entity, transaction);
  }

  private List<Job> loadDataTypeSpecificJobs(
      final AbstractDatatype dt,
      final EntityInterface entity,
      final Transaction<? extends TransactionContainer> transaction) {
    if (dt == null) {
      return new ArrayList<Job>();
    }
    if (dt instanceof ReferenceDatatype2) {
      return jobConfig.getConfiguredJobs(0, 17, entity, transaction);
    } else if (dt instanceof AbstractCollectionDatatype) {
      final AbstractDatatype datatype = ((AbstractCollectionDatatype) dt).getDatatype();
      return loadDataTypeSpecificJobs(datatype, entity, transaction);
    } else if (dt.getId() != null) {
      return jobConfig.getConfiguredJobs(0, dt.getId(), entity, transaction);
    } else {
      return null;
    }
  }

  public List<Job> loadStandardJobs(
      final EntityInterface entity, final Transaction<? extends TransactionContainer> transaction) {

    final ArrayList<Job> jobs = new ArrayList<>();
    // load permanent jobs
    for (final Class<? extends Job> j : loadAlways) {
      if (EntityJob.class.isAssignableFrom(j)
          && j.getAnnotation(JobAnnotation.class).transaction().isInstance(transaction)) {
        jobs.add(getJob(j, JobFailureSeverity.ERROR, entity, transaction));
      }
    }

    // load general rules
    final List<Job> generalRules = jobConfig.getConfiguredJobs(0, 0, entity, transaction);
    if (generalRules != null) {
      jobs.addAll(generalRules);
    }

    // load Role specific rules
    if (entity.hasRole()) {
      final List<Job> roleRules =
          jobConfig.getConfiguredJobs(0, entity.getRole().getId(), entity, transaction);
      if (roleRules != null) {
        jobs.addAll(roleRules);
      }
    }

    // load data type specific rules
    final List<Job> datatypeRules = loadDataTypeSpecificJobs(entity, transaction);
    if (datatypeRules != null) {
      jobs.addAll(datatypeRules);
    }

    return jobs;
  }

  private static Job getJob(
      final Class<? extends Job> jobClass,
      final JobFailureSeverity severity,
      final EntityInterface entity,
      final Transaction<? extends TransactionContainer> transaction) {
    Job ret;
    try {

      if (jobClass != null) {
        ret = jobClass.getDeclaredConstructor().newInstance();
        ret.init(severity, entity, transaction);
        return ret;
      }
      return null;
    } catch (final InstantiationException
        | IllegalAccessException
        | IllegalArgumentException
        | InvocationTargetException
        | NoSuchMethodException
        | SecurityException e) {
      throw new TransactionException(e);
    }
  }

  public List<Job> loadJobs(
      final EntityInterface entity, final Transaction<? extends TransactionContainer> transaction) {
    final LinkedList<Job> jobs = new LinkedList<>();

    // general rules, role rules, data type rules
    jobs.addAll(loadStandardJobs(entity, transaction));

    // load flag jobs
    if (!entity.getFlags().isEmpty()) {
      for (final String key : entity.getFlags().keySet()) {
        final Job j = getJob(key, JobFailureSeverity.ERROR, entity, transaction);
        if (j != null) {
          if (j instanceof FlagJob) {
            ((FlagJob) j).setValue(entity.getFlag(key));
          }
          jobs.add(j);
        }
      }
    }

    // load parent flag jobs
    if (entity.hasParents()) {
      for (final EntityInterface p : entity.getParents()) {
        if (!p.getFlags().isEmpty()) {
          for (final String key : p.getFlags().keySet()) {
            final Job j = getJob(key, JobFailureSeverity.ERROR, entity, transaction);
            if (j != null) {
              if (j instanceof FlagJob) {
                ((FlagJob) j).setValue(p.getFlag(key));
              }
              jobs.add(j);
            }
          }
        }
      }
    }

    // load property flag jobs
    // FIXME unnecessary since properties do run the flag stuff again (see
    // next for loop)?
    if (entity.hasProperties()) {
      for (final EntityInterface p : entity.getProperties()) {
        if (!p.getFlags().isEmpty()) {
          for (final String key : p.getFlags().keySet()) {
            final Job j = getJob(key, JobFailureSeverity.ERROR, entity, transaction);
            if (j != null) {
              if (j instanceof FlagJob) {
                ((FlagJob) j).setValue(p.getFlag(key));
              }
              jobs.add(j);
            }
          }
        }
      }
    }

    // load jobs for the properties
    if (entity.hasProperties()) {
      for (final EntityInterface p : entity.getProperties()) {
        jobs.addAll(loadJobs(p, transaction));
      }
    }

    return jobs;
  }

  public Transaction<? extends TransactionContainer> getTransaction() {
    return this.transaction;
  }

  @Override
  public String toString() {
    return "JOB["
        + this.getClass().getSimpleName()
        + "-"
        + (getEntity() != null ? getEntity().toString() : "NOENTITY")
        + " #"
        + getFailureSeverity().toString()
        + "]";
  }

  public void print() {
    System.out.println(toString());
  }

  public TransactionStage getTransactionStage() {
    return this.stage;
  }

  /**
   * Resolve an entity (which might only be specified by it's name or id) to its full representation
   * (with properties, parents and all).
   *
   * @param entity the entity to be resolved.
   * @return the resolved entity.
   * @throws EntityWasNotUniqueException if the resolution failed due to ambuiguity of the name.
   */
  protected EntityInterface resolve(final EntityInterface entity)
      throws EntityWasNotUniqueException {
    EntityInterface resolvedEntity = null;
    if (!entity.hasId() && entity.hasName()) {
      resolvedEntity = getEntityByName(entity.getName());
      if (resolvedEntity == null) {
        final Integer eid = retrieveValidIDByName(entity.getName());
        entity.setId(eid);
      }
    }

    if (entity.hasId()) {
      // get entity from container
      resolvedEntity = getEntityById(entity.getId());
      if (resolvedEntity == null && entity.getId() > 0) {
        resolvedEntity = retrieveValidEntity(entity.getId());
      }
    }

    return resolvedEntity;
  }

  /**
   * Return those matching jobs which are annotated with the "loadAlways" attribute.
   *
   * @return A list with the jobs.
   */
  public static List<Job> loadPermanentContainerJobs(final Transaction<?> transaction) {
    final ArrayList<Job> jobs = new ArrayList<>();
    // load permanent jobs: ContainerJob classes with the correct transaction
    for (final Class<? extends Job> j : loadAlways) {
      if (ContainerJob.class.isAssignableFrom(j)
          && j.getAnnotation(JobAnnotation.class).transaction().isInstance(transaction)) {
        jobs.add(getJob(j, JobFailureSeverity.ERROR, null, transaction));
      }
    }
    return jobs;
  }
}
