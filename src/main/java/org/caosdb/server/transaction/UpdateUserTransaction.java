/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import org.apache.shiro.SecurityUtils;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.accessControl.UserStatus;
import org.caosdb.server.database.backend.transaction.RetrieveUser;
import org.caosdb.server.database.backend.transaction.SetPassword;
import org.caosdb.server.database.backend.transaction.UpdateUser;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.server.utils.Utils;
import org.jdom2.Element;

public class UpdateUserTransaction extends AccessControlTransaction {

  private final String password;
  private final ProtoUser user = new ProtoUser();

  public UpdateUserTransaction(
      final String realm,
      final String username,
      final UserStatus status,
      final String email,
      final Integer entity,
      final String password) {
    this.user.realm =
        (realm == null
            ? UserSources.guessRealm(username, UserSources.getInternalRealm().getName())
            : realm);
    this.user.name = username;
    this.user.status = status;
    this.user.email = email;
    this.user.entity = entity;
    this.password = password;
  }

  @Override
  protected void transaction() throws Exception {
    if (!UserSources.isUserExisting(new Principal(this.user.realm, this.user.name))) {
      throw ServerMessages.ACCOUNT_DOES_NOT_EXIST;
    }

    if (this.password != null) {
      // passwords cannot be updated for any but the internal password
      // storage.
      if (!this.user.realm.equals(UserSources.getInternalRealm().getName())) {
        throw new TransactionException(
            "The password from the realm '"
                + this.user.realm
                + "' cannot be updated. Only the users from the realm '"
                + UserSources.getInternalRealm().getName()
                + "' can change their passwords from within caosdb.");
      }

      SecurityUtils.getSubject()
          .checkPermission(
              ACMPermissions.PERMISSION_UPDATE_USER_PASSWORD(this.user.realm, this.user.name));

      if (this.password.isEmpty()) {
        // reset password
        execute(new SetPassword(this.user.name, null), getAccess());
      } else {
        Utils.checkPasswordStrength(this.password);
        execute(new SetPassword(this.user.name, this.password), getAccess());
      }
    }

    if (isToBeUpdated()) {
      execute(new UpdateUser(this.user), getAccess());
    }
  }

  private boolean isToBeUpdated() throws Exception {
    boolean isToBeUpdated = false;
    final ProtoUser validUser =
        execute(new RetrieveUser(new Principal(this.user.realm, this.user.name)), getAccess())
            .getUser();
    if (this.user.status != null && (validUser == null || this.user.status != validUser.status)) {
      SecurityUtils.getSubject()
          .checkPermission(
              ACMPermissions.PERMISSION_UPDATE_USER_STATUS(this.user.realm, this.user.name));
      isToBeUpdated = true;
    } else if (validUser != null) {
      this.user.status = validUser.status;
    }
    if (this.user.email != null
        && (validUser == null || !this.user.email.equals(validUser.email))) {
      SecurityUtils.getSubject()
          .checkPermission(
              ACMPermissions.PERMISSION_UPDATE_USER_EMAIL(this.user.realm, this.user.name));
      if (!Utils.isRFC822Compliant(this.user.email)) {
        throw ServerMessages.EMAIL_NOT_WELL_FORMED;
      }

      isToBeUpdated = true;
    } else if (validUser != null) {
      this.user.email = validUser.email;
    }
    if (this.user.entity != null
        && (validUser == null || !this.user.entity.equals(validUser.entity))) {
      SecurityUtils.getSubject()
          .checkPermission(
              ACMPermissions.PERMISSION_UPDATE_USER_ENTITY(this.user.realm, this.user.name));
      isToBeUpdated = true;

      if (this.user.entity.equals(0)) {
        // this means that the entity is to be reset.
        this.user.entity = null;
      } else {
        checkEntityExists(this.user.entity);
      }
    } else if (validUser != null) {
      this.user.entity = validUser.entity;
    }

    return isToBeUpdated;
  }

  public static void checkEntityExists(final int entity) throws Exception {
    final RetrieveContainer c = new RetrieveContainer(null, null, null, null);
    final Entity e = new RetrieveEntity(entity);
    c.add(e);
    final Retrieve t = new Retrieve(c);
    t.execute();
    if (e.getEntityStatus() != EntityStatus.VALID) {
      throw ServerMessages.ENTITY_DOES_NOT_EXIST;
    }
  }

  public Element getUserElement() {
    return RetrieveUserTransaction.getUserElement(this.user);
  }
}
