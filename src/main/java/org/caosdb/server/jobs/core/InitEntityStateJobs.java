/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.entity.ClientMessage;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.WritableEntity;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.transaction.WriteTransaction;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;

/**
 * Initialize the other entity jobs by converting the client message with type "State" or
 * StateProperties into {@link State} instances.
 *
 * <p>This job also needs to initialize the other jobs even if the current entity version does not
 * have a state anymore but the previous version had, because it has to be checked if the stateModel
 * allows to leave in this state.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
@JobAnnotation(
    loadAlways = true,
    stage = TransactionStage.INIT,
    transaction = WriteTransaction.class)
public class InitEntityStateJobs extends EntityStateJob implements Observer {

  @Override
  protected void run() {
    if ("ENABLED".equals(CaosDBServer.getServerProperty(SERVER_PROPERTY_EXT_ENTITY_STATE))) {
      State newState = handleNewState();
      State oldState = handleOldState(newState);
      if (newState != null || oldState != null) {
        if (!(getEntity() instanceof DeleteEntity)) {

          appendJob(MakeStateProperty.class);
        }
        appendJob(CheckStateTransition.class);
        appendJob(MakeStateMessage.class);
      } else if (newState == null
          && getEntity().getRole() == Role.Record
          && getEntity() instanceof InsertEntity) {
        appendJob(InheritInitialState.class);
        appendJob(CheckStateTransition.class);
        appendJob(MakeStateProperty.class);
        appendJob(MakeStateMessage.class);
      }
      if (getEntity() instanceof WritableEntity || getEntity() instanceof DeleteEntity) {
        removeCached(getEntity());
      }
    }
  }

  /**
   * Converts the state property of the original entity into a state message (only needed for
   * updates).
   *
   * <p>Also, this method adds an observer to the entity state which handles a corner case where the
   * entity changes the state, but no other property changes. In this case Update.deriveUpdate
   * cannot detect any changes and will mark this entity as "to-be-skipped". The observer waits for
   * that to happen and changes the {@EntityStatus} back to normal.
   *
   * @param newState
   * @return The old state or null.
   */
  private State handleOldState(State newState) {
    State oldState = null;
    try {
      if (getEntity() instanceof UpdateEntity) {
        List<State> states = initStateMessage(((UpdateEntity) getEntity()).getOriginal());
        oldState = null;
        if (states.size() == 1) {
          oldState = states.get(0);
          if (newState != null) {
            ((UpdateEntity) getEntity()).getOriginal().setEntityACL(getEntity().getEntityACL());
          }
        }
        if (!Objects.equals(newState, oldState)) {
          getEntity().acceptObserver(this);
        }
      }
    } catch (Message m) {
      getEntity().addWarning(STATE_ERROR_IN_ORIGINAL_ENTITY(m));
    }
    return oldState;
  }

  /**
   * Converts the state property of this entity into a state message.
   *
   * @return The new state or null.
   */
  private State handleNewState() {
    State newState = null;
    try {
      List<State> states = initStateMessage(getEntity());
      if (states.size() > 1) {
        throw new Message(
            MessageType.Error, "Currently, each entity can only have one state at a time.");
      } else if (states.size() == 1) {
        newState = states.get(0);
        if (getEntity().getRole() == Role.Record) {
          transferEntityACL(getEntity(), newState);
        }
      }
    } catch (Message m) {
      getEntity().addError(m);
    }

    return newState;
  }

  private void transferEntityACL(EntityInterface entity, State newState) throws Message {
    newState.getStateEntity();
    entity.setEntityACL(newState.getStateACL());
  }

  private static final Message STATE_ERROR_IN_ORIGINAL_ENTITY(Message m) {
    return new Message(
        MessageType.Warning, "State error in previous entity version\n" + m.getDescription());
  }

  /**
   * Return a list of states from their representations as properties or client messages in the
   * entity.
   *
   * @param entity
   * @return list of state instances for the entity.
   * @throws Message
   */
  private List<State> initStateMessage(EntityInterface entity) throws Message {
    List<ClientMessage> stateClientMessages = getStateClientMessages(entity, true);
    List<State> result = new ArrayList<>();
    if (stateClientMessages != null) {
      for (ClientMessage s : stateClientMessages) {
        State stateMessage = createState(s);
        entity.addMessage(stateMessage);
        result.add(stateMessage);
      }
    }
    List<Property> stateProperties = getStateProperties(entity, true);
    if (stateProperties != null) {
      for (Property p : stateProperties) {
        State stateMessage = createState(p);
        entity.addMessage(stateMessage);
        result.add(stateMessage);
      }
    }
    return result;
  }

  @Override
  public boolean notifyObserver(String e, Observable o) {
    if (e == Entity.ENTITY_STATUS_CHANGED_EVENT) {
      if (o == getEntity() && getEntity().getEntityStatus() == EntityStatus.VALID) {
        // The Update.deriveUpdate method didn't recognize that the state is changing and set the
        // entity to "VALID"
        getEntity().setEntityStatus(EntityStatus.QUALIFIED);
        return false;
      }
    }
    return true;
  }
}
