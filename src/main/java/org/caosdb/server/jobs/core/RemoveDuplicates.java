/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import java.util.HashSet;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.jobs.ContainerJob;

public class RemoveDuplicates extends ContainerJob {

  @Override
  protected void run() {
    // collect duplicates
    final HashSet<EntityInterface> duplicates = new HashSet<EntityInterface>();
    for (final EntityInterface e : getContainer()) {
      if (e.hasId() && !duplicates.contains(e)) {
        for (final EntityInterface e2 : getContainer()) {
          if (e2 != e && e.getIdVersion().equals(e2.getIdVersion())) {
            // this is a duplicate of another entity in this container
            duplicates.add(e2);
          }
        }
      }
    }
    // remove duplicates.
    for (final EntityInterface e : duplicates) {
      getContainer().remove(e);
    }
  }
}
