# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

* An openAPI specification of the XML api

### Changed

### Deprecated

### Removed

### Fixed

* #127 "nan" as value (list item) in properties with data type "LIST<DOUBLE>"
  return with "Cannot parse value to double" error.
* #170 Updating an abstract list-type property with a default value fails with
  "unkown error".
* #145 Documentation of importances and inheritance
* Missing sources of the easy-unit dependency.

### Security

## [v0.4.0] - 2021-06-21

### Added

* Related to #146, a new flag for entities and complete transactions:
  `force-missing-obligatory=[ignore|warn|error]`. The flag overrides the
  default behavior of the server (throwing an error when an obligatory property
  is missing). `ignore` just discards the consistency check, `warn` only issues
  a warning when obligatory properties are missing and `error` throws an error
  in that case. The flag can be set for the complete transaction and each
  single entity, while the entity flag takes precedence.
* New EntityState plug-in. The plug-in disabled by default and can be enabled
  by setting the server property `EXT_ENTITY_STATE=ENABLED`. See
  [!62](https://gitlab.com/caosdb/caosdb-server/-/merge_requests/62) for more
  information.
* `ETag` property for the query. The `ETag` is assigned to the query cache
  each time the cache is cleared (currently whenever the server state is being
  updated, i.e. the stored entities change).
  This can be used to debug the query cache and also allows a client
  to determine whether the server's state has changed between queries.
* Basic caching for queries. The caching is enabled by default and can be
  controlled by the usual "cache" flag.
* Documentation for the overall server structure.
* Add `BEFORE`, `AFTER`, `UNTIL`, `SINCE` keywords for query transaction

### Changed

* The default session timeout changed from 10 min to 60 min. Please set it to
  your needs via the server config option `SESSION_TIMEOUT_MS`.

### Deprecated

### Removed

### Fixed

* #146 - Default behavior for missing obligatory properties
* #131 - CQL Parsing error when white space characters before some units.
* #134 - CQL Parsing error when multiple white space characters after `FROM`.
* #130 - Error during `FIND ENTITY` when
  `QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS=False`.
* #125 - `bend_symlinks` script did not allow whitespace in filename.
* #122 - Dead-lock due to error in the DatabaseAccessManager.
* #120 - Editing entities that were created with a no longer existing user
  leads to a server error.
* #31 - Queries with keywords in the path (e.g. `... STORED AT 0in.txt`)
* #116 - Queries `FIND [ANY VERSION OF] *` and `FIND [ANY VERSION OF] ENTITY`.

### Security

## [0.3.0] - 2021-02-10

### Added

* New version history feature. The "H" container flag retrieves the full
  version history during a transaction (e.g. during Retrievals) and constructs
  a tree of successors and predecessors of the requested entity version.
* New query functionality: `ANY VERSION OF` modifier. E.g. `FIND ANY VERSION OF
  RECORD WITH pname=val` returns all current and old versions of records where
  `pname=val`. For further information, examples and limitations see the wiki
  page on [CQL](https://gitlab.com/caosdb/caosdb/-/wikis/manuals/CQL/CaosDB%20Query%20Language)
* New server property `SERVER_SIDE_SCRIPTING_BIN_DIRS` which accepts a comma or
  space separated list as values. The server looks for scripts in all
  directories in the order or the list and uses the first matching file.
* Automated documentation builds: `make doc`

### Changed

* Server can be started without TLS even when not in debug mode.
* Select queries would originally only select the returned properties by their
  names and would not check if a property is a subtype of a selected property. This
  has changed now and select queries will also return subtypes of selected
  properties.

### Deprecated

* `SERVER_SIDE_SCRIPTING_BIN_DIR` property is deprecated.
  `SERVER_SIDE_SCRIPTING_BIN_DIRS` should be used instead (note the plural
  form!)

### Removed

* Text user interface (CaosDBTerminal).

### Fixed

* Bug: When the user password is updated the user is deactivated.
* Semi-fixed a bug which occurs when retrieving old versions of entities which
  reference entities which have been deleted in the mean time. The current fix
  adds a warning message to the reference property in question and sets the
  value to NULL. This might even be desired behavior, however this would have
  to finally specified during the Delete/Forget phase of the implementation of
  the versioning.
- Inheritance job cannot handle inheritance from same container (!54)
* Bug in the query parser (MR!56) - The parser would throw an error when the
  query contains a conjunction or disjunction filter with a first element which
  is another disjunction or conjunction and being wrapped into parenthesis.

### Security

## [0.2.0] - 2020-09-02

### Added

- Support for deeply nested selectors in SELECT queries.
- One-time Authentication Tokens for login without credentials and login with
  particular permissions and roles for the course of the session.
- `Entity/names` resource for retrieving all known entity names.
- Scripting is simplified by adding a `home` directory, of which a copy is
  created for each called script and set as the `HOME` environment variable.
- [bend_symlinks.sh](misc/bend_symlinks/bend_symlinks.sh) (version 0.1, experimental)
  fix broken symlinks in the internal file system. See
  [README.md](misc/bend_symlinks/README.md)
- [move_files.py](misc/move_files/move_files.py) (version 0.1, experimental)
  Script for moving files (change their path) in the internal file system based
  on a two-column tsv file (with columns "from" and "to"). See
  [README.md](misc/move_files/README.md).
- LDAP server may now be given and may be different from LDAP domain. See
  `misc/pam_authentication/ldap.conf`
- #47 - Sub-properties can now be queried, such as in
  `SELECT window.width FROM house`.
- Added support for versioning, if it is enabled on the backend.


### Changed

* All caosdb server java classes moved from `caosdb.[...]` to
  `org.caosdb.[...]` because the new root package is compliant with the java
  package naming conventions while the old was not. This has some implications
  for configuring the server. See [README_SETUP.md](./README_SETUP.md), section
  "Migration" for additional information.
- The server by default now only serves TLS 1.2 and 1.3, all previous versions
  have been disabled in the default settings.  Make sure that your clients
  (especially the Python client) are up to date.

### Deprecated

- CaosDBTerminal

### Removed

### Fixed

* Missing handling of list of reference properties in SELECT queries.
* #51 - name queries (e.g. `FIND ENTITY WITH name = ...`)
- #27 - star matches slashes (e.g. for `FIND ... STORED AT /*.dat`).
- #30 - file path cannot be in quotes
- #46 - Server-side scripting failed as an unprivileged user because there was
  no writable home directory.
- NaN Double Values (see #41)
- #14 - Handle files on file system without File entity: Those entries are
  returned without ID but with a notice now.
- #11 - pam_authentication leaks the password to unprivileged processes on the
  same machine.
- #39 - quotes around datetimes in queries
- #99 - Checksum updating resulted in infinite loop on server.

### Security (in case of vulnerabilities)

- TLS is by default restricted to v1.2 and v1.3 now.
- #11 - PAM and LDAP authentication no longer leak the password to unprivileged
  processes on the same machine.
- #68 - Shadow sensitive information when logging for debugging purposes.

## [0.1.0] - 2018-10-09

Tag `v0.1` - Commit 3b17b49

### Added

- everything
