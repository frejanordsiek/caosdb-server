# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import os
import random
import unittest

import pandas as pd

import caosdb as db
from move_files import rename


def create_filename():
    name = os.path.normpath("".join([
        random.choice("qwertyuiopkkhgfdsxcvbnm/") for el in range(30)]))

    if not name.startswith("/"):
        name = "/" + name

    return name


class TestMoveFiles(unittest.TestCase):
    """
    Files are being created, changed and then it is checked whether the changes
    were correct.
    """

    def setUp(self):
        self.files_to_be_changed = [create_filename() for i in range(40)]
        self.files_not_to_be_changed = [create_filename() for i in range(20)]
        self.new_names = [f+"new" for f in self.files_to_be_changed]

        table = pd.DataFrame([self.files_to_be_changed, self.new_names])
        table = table.T
        table.columns = ["from", "to"]
        self.table = table
        self.cont = db.Container()
        self.cont.extend([db.File(path=f, file=__file__)
                          for f in self.files_to_be_changed
                          + self.files_not_to_be_changed])
        self.cont.insert()
        print("inserted")

    def test_move(self):
        rename(self.table)
        self.cont.retrieve()

        for i, (fi, name) in enumerate(zip(
                self.cont,
                self.files_to_be_changed + self.files_not_to_be_changed)):

            if i < len(self.files_to_be_changed):
                self.assertEqual(fi.path, self.new_names[i])
            else:
                self.assertEqual(fi.path, name)

    def tearDown(self):
        self.cont.delete()
        print("deleted")
