/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import static org.caosdb.server.utils.ServerMessages.ENTITY_DOES_NOT_EXIST;

import com.google.common.base.Objects;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.database.exceptions.EntityWasNotUniqueException;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether all properties of an entity are valid or qualified.
 *
 * @author tf
 */
@JobAnnotation(stage = TransactionStage.PRE_CHECK)
public class CheckPropValid extends EntityJob {
  @Override
  public final void run() {

    // loop over all properties of the entity
    for (final Property property : getEntity().getProperties()) {
      try {
        if (property.getEntityStatus() == EntityStatus.QUALIFIED) {
          // this property is to be tested.

          // does this property have an id at all?
          if (property.hasId()) {
            if (property.getId() >= 0) {

              final EntityInterface abstractProperty =
                  retrieveValidSparseEntityById(property.getId(), null);

              assertAllowedToUse(abstractProperty);

              deriveOverrideStatus(property, abstractProperty);
              continue;
            } else {
              // task here: find the corresponding abstract
              // property (or rarely any other entity) in this
              // container which has the same (negative) id.

              // fetch the abstractProperty from the container.
              final EntityInterface abstractProperty = getEntityById(property.getId());

              if (abstractProperty != null) {
                assertAllowedToUse(abstractProperty);

                // link the id of the property to the id of
                // the abstractProperty means. This has the
                // effect that the property will have a
                // valid id as soon as the abstractProperty
                // has been inserted.
                property.linkIdToEntity(abstractProperty);
                deriveOverrideStatus(property, abstractProperty);
                continue;
              } else if (!property.hasName()) {
                // an abstractProperty with this (negative) id
                // had not been found in this container.
                throw ENTITY_DOES_NOT_EXIST;
              }
            }
          }

          if (property.hasName()) {

            // try and get it from the container
            EntityInterface foreign = getEntityByName(property.getName());
            if (foreign != null) {
              assertAllowedToUse(foreign);

              // link the id of the property to the id of
              // the abstractProperty means. This has the
              // effect that the property will have a
              // valid id as soon as the abstractProperty
              // has been inserted.
              property.linkIdToEntity(foreign);
              deriveOverrideStatus(property, foreign);
            } else {
              foreign = retrieveValidSparseEntityByName(property.getName());

              assertAllowedToUse(foreign);

              property.setId(foreign.getId());

              deriveOverrideStatus(property, foreign);
            }
          }

          if (!property.hasName() && !property.hasId()) {
            // The property has neither an id nor a name.
            // Thus it cannot be identified.

            throw ServerMessages.ENTITY_HAS_NO_NAME_OR_ID;
          }
        }
      } catch (final Message m) {
        addError(property, m);
      } catch (final EntityDoesNotExistException e) {
        addError(property, ENTITY_DOES_NOT_EXIST);
      } catch (final EntityWasNotUniqueException e) {
        addError(property, ServerMessages.ENTITY_NAME_DUPLICATES);
      }
    }

    // process names
    appendJob(ProcessNameProperties.class);
    // final ProcessNameProperties processNameProperties = new
    // ProcessNameProperties();
    // processNameProperties.init(getMode(), getEntity(), getContainer(),
    // getTransaction());
    // getTransaction().getSchedule().add(processNameProperties);
    // getTransaction().getSchedule().runJob(processNameProperties);

  }

  private void assertAllowedToUse(final EntityInterface property) throws Message {
    checkPermission(property, EntityPermission.USE_AS_PROPERTY);
  }

  private void addError(final EntityInterface property, final Message m) {
    property.addError(m);
    property.setEntityStatus(EntityStatus.UNQUALIFIED);
  }

  private static void deriveOverrideStatus(final Property child, final EntityInterface parent) {
    if (!Objects.equal(child.getName(), parent.getName())) {
      if (child.hasName()) {
        child.setNameOverride(true);
      } else {
        child.setName(parent.getName());
      }
    }
    if (!Objects.equal(child.getDescription(), parent.getDescription())) {
      if (child.hasDescription()) {
        child.setDescOverride(true);
      } else {
        child.setDescription(parent.getDescription());
      }
    }
    if (!Objects.equal(child.getDatatype(), parent.getDatatype())) {
      if (child.hasDatatype()
          // FIXME why this?
          && (!child.getDatatype().toString().equals("REFERENCE") || parent.hasDatatype())) {
        child.setDatatypeOverride(true);
      } else {
        child.setDatatype(parent.getDatatype());
      }
    }
  }
}
