
Maintenance of the CaosDB Server
================================

Creating a Backup
-----------------

In order to create a full backup of CaosDB, the state of the SQL-Backend (MySQL, MariaDB)
has to be saved and the internal file system of CaosDB (symbolic links to 
file systems that are mounted and uploaded files) has to be saved.

You find the documentation on how to backup the SQL-Backend  :any:`caosdb-mysqlbackend:Maintenance` 

In order to save the file backend we recommend to tar the file system. However, 
you could use other backup methods that allow to restore the file system.
The CaosDB internal file system is located at the path defined by the 
``FILE_SYSTEM_ROOT`` configuration variable (see :any:`configuration`).

The command could look like::

    tar czvf /path/to/new/backup /path/to/caosdb/filesystem.tar.gz


You can also save the content of CaosDB using XML. This is **not recommended** since it produces
less reproducible results than a plain SQL backup. However there may be cases in which an XML backup
is necessary, e.g., when transferring entities between two different CaosDB instances.

Collect the entities that you want to export in a
:any:`Container<caosdb-pylib:caosdb.common.models.Container>`, named ``cont`` here. Then you can
export the XML with::

     from caosadvancedtools.export_related import invert_ids
     from lxml import etree

     invert_ids(cont)
     xml = etree.tounicode(cont.to_xml(
         local_serialization=True), pretty_print=True)

     with open("caosdb_data.xml"), "w") as fi:
         fi.write(xml)


Restoring a Backup
------------------

.. warning::
    CaosDB should be offline before restoring data.

If you want to restore the internal file system, simply replace it. E.g. if your
backup is a tarball::

    tar xvf /path/to/caosroot.tar.gz


You find the documentation on how to restore the data in the SQL-Backend  :any:`caosdb-mysqlbackend:Maintenance` 


If you want to restore the entities exported to XML, you can do::

     cont = db.Container()
     with open("caosdb_data.xml") as fi:
         cont = cont.from_xml(fi.read())
     cont.insert()

User Management
---------------

The configuration of authentication mechanisms is done via the ``usersources.ini`` file (see
:any:`configuration`).

We recommend the Python tools (:any:`caosdb-pylib:administration`) for further administrative tasks
(e.g. setting user passwords).

