Permissions
===========

CaosDB has a fine grained role based permission system. Each interaction
with the server is governed by the current rules of the user, by default
this is the ``anonymous`` role. The permissions for an action which
involves one or more objects are set either manually or via default
permissions which can be configured. For more detailed information,
there is separate
:doc:``documentation of the permission system<permissions>``.

Permissions are needed to perform particular elementary *actions* during
any interaction with the the server. E.g. retrieving an Entity requires
the requesting user to have the ``RETRIEVE:ENTITY`` permission for that
entity, and ``DELETE:ENTITY`` to delete the entity.

The permissions of every user are calculated from a set of *Permission
Rules*. These rules have several sources. Some are global defaults, some
are defined by administrators or the owners of an entity.

As a side note on the implementation: The server uses `Apache
Shiro <https://shiro.apache.org/documentation.html>`__ for its
permission system.

What is a Permission Rule?
--------------------------

A Permission Rule consists of:

-  A type: Permission Rules can be of ``Grant`` or ``Deny`` type, either
   granting or denying specific permissions.
-  A :doc:`role <roles>` (or user): For which users the
   permission shall be granted or denied.
-  A permission action: Which action shall be permitted or forbidden,
   for example all retrieval, or modifications of a specific entity.
-  An optional priority: May be ``true`` or ``false``. Permissions with
   priority = ``true`` override those without, see the calculation rules
   below.

Permission calculation
----------------------

For each action a user tries to perform, the server tests, in the
following order, which rules apply:

1. *Grant* rules, without priority.
2. *Deny* rules, without priority.
3. *Grant* rules, with priority.
4. *Deny* rules, with priority.

If at the end the user’s permission is *granted*, the action may take
place. Otherwise (the result is *denied* or the permission still
undefined), the action can not take place. In other words, if you have
not been given the permission explicitly at some point, you don’t have
it.

Possible actions
----------------

Until it is completely added to this documentation, a detailed
description of the actions governed by these permissions can be found
`in the
sources <https://gitlab.com/caosdb/caosdb-server/-/blob/dev/src/main/java/org/caosdb/server/permissions/EntityPermission.java#L119>`__.

Typical permissions are:

-  ``RETRIEVE:ENTITY`` :: To retrieve the full entity (name,
   description, data type, …) with all parents and properties (unless
   prohibited by another rule on the property level).
-  ``RETRIEVE:ACL`` :: To retrieve the full and final ACL of this
   entity.
-  ``RETRIEVE:ENTITY:1234`` :: To retrieve the entity ``1234``.
-  ``RETRIEVE:*:1234`` :: For all “retrieve” actions concerning the
   entity ``1234``.

How to set permissions
----------------------

-  Config file :: Some default permissions are typically set in the
   ``global_entity_permissions.xml`` file, see also the `documentation
   for that
   file </manuals/server/conf/global_entity_permissions.xml>`__.
-  API :: The REST API allows to set the permissions. (*to be documented
   here*)
-  The Python library :: The permissions can also conveniently be set
   via the Python library. Currently the best documentation is inside
   various files which use the permission API:

   -  The `example
      file <https://gitlab.com/caosdb/caosdb-pylib/blob/HEAD/examples/set_permissions.py>`__
   -  The ```caosdb_admin.py`` utility
      script <https://gitlab.com/caosdb/caosdb-pylib/blob/HEAD/src/caosdb/utils/caosdb_admin.py>`__
   -  The `integration
      tests <https://gitlab.com/caosdb/caosdb-pyinttest/blob/HEAD/tests/test_permissions.py>`__
      also cover quite a bit of the permission API.

-  WebUI :: Not implemented (or documented?) yet.
