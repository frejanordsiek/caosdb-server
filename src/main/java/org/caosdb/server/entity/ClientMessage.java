/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

package org.caosdb.server.entity;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.jdom2.Attribute;
import org.jdom2.Element;

/**
 * Class which represents client messages. Client messages is a way to extend the Entity API with
 * special properties which may be used by plug-ins.
 *
 * <p>If no plug-in handles the client message, it is printed back to the response unaltered.
 *
 * <p>Client message can have arbitrary key-value (string-string typed) tuples {@link #properties}.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class ClientMessage extends Message {

  private static final long serialVersionUID = 1L;
  private Map<String, String> properties = new HashMap<>();

  public ClientMessage(String type, String body) {
    super(type, null, null, null);
  }

  @Override
  public Element toElement() {
    final Element e = new Element(this.type);
    for (Entry<String, String> a : this.properties.entrySet()) {
      e.setAttribute(a.getKey(), a.getValue());
    }
    return e;
  }

  @Override
  public void addToElement(final Element parent) {
    final Element e = toElement();
    parent.addContent(e);
  }

  /** NB: This is the only place where properties are set in this class. */
  public static ClientMessage fromXML(Element pe) {
    ClientMessage result = new ClientMessage(pe.getName(), pe.getText());
    for (Attribute a : pe.getAttributes()) {
      result.properties.put(a.getName(), a.getValue());
    }
    return result;
  }

  public String getProperty(String key) {
    return properties.get(key);
  }

  @Override
  public String toString() {
    return this.type + " - " + this.properties.toString();
  }

  @Override
  public int hashCode() {
    return type.hashCode()
        + (this.getBody() == null ? 0 : this.getBody().hashCode())
        + this.properties.hashCode();
  }
}
