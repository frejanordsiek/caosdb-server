/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity.container;

import java.util.HashMap;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.entity.FileProperties;

public class WritableContainer extends TransactionContainer {
  private static final long serialVersionUID = -4097777313518959519L;

  public WritableContainer(
      final Subject user,
      final Long timestamp,
      final String srid,
      final HashMap<String, String> flags) {
    super(user, timestamp, srid, flags);
  }

  public WritableContainer() {
    this(SecurityUtils.getSubject(), System.currentTimeMillis(), null, null);
  }

  @Override
  public void addFile(final String uploadId, final FileProperties fileProperties) {
    super.addFile(uploadId, fileProperties);
  }
}
