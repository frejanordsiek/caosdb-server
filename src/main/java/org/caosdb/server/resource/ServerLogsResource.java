/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.XMLFormatter;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.transaction.RetrieveLogRecordTransaction;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

public class ServerLogsResource extends AbstractCaosDBServerResource {

  @Override
  protected Representation httpGetInChildClass()
      throws ConnectionException, IOException, SQLException, CaosDBException,
          NoSuchAlgorithmException, Exception {

    Level level = null;
    String message = null;
    final String logger =
        getRequest().getResourceRef().getRemainingPart(true, false).replaceAll("/", ".");
    final Form form = getRequest().getResourceRef().getQueryAsForm(true);
    if (form != null) {
      final String levelStr = form.getFirstValue("level");
      if (levelStr != null) {
        try {
          level = Level.parse(levelStr);
        } catch (final IllegalArgumentException e) {
          level = Level.OFF;
        }
      }
      message = form.getFirstValue("message");
    }

    final RetrieveLogRecordTransaction t = new RetrieveLogRecordTransaction(logger, level, message);
    t.execute();

    generateRootElement();
    final List<LogRecord> logRecords = t.getLogRecords();

    final XMLFormatter xmlFormatter = new XMLFormatter();
    String ret = xmlFormatter.getHead(null);
    for (final LogRecord r : logRecords) {
      ret += xmlFormatter.format(r);
    }
    ret += xmlFormatter.getTail(null);

    return new StringRepresentation(ret, MediaType.TEXT_XML);
  }
}
