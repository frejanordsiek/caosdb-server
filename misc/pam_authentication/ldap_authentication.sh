#!/bin/bash

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (c) 2020 IndiScale GmbH
# Copyright (c) 2020 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# Try to authenticate a user ($1) via LDAP, either via stdin or a password file ($2, if given).

[[ "$#" == "1" || "$#" == "2" ]] || {
    echo "Call this script as: $0 <user> [<password file>]"
    exit 1
}

# set LDAP_SERVER here
# e.g. `LDAP_SERVER=example.com`
exe_dir=$(dirname $0)
. "$exe_dir/"ldap.conf

# If the second argument is empty or "-", take password from stdin, else use the argument as a file.
testpw() {
    username="${1}@${LDAP_DOMAIN}"
    pwfile="$2"
    pwargs=("-w" "$pwfile")
    if [[ $pwfile == "-" ]] ; then
        pwargs=("-W")
    fi

    export LDAPTLS_REQCERT=ALLOW
    if timeout 5s ldapwhoami -x -H "ldaps://$LDAP_SERVER" -D "$username" "${pwargs[@]}"; then
        return 0
    else
        ret_code="$?"
    fi

    # Possibly try a second time
    if [[ "$ret_code" != "124" ]] ; then
        return "$ret_code"
    fi

    ldapwhoami -x -H "ldaps://$LDAP_SERVER" -D "$username" "${pwargs[@]}"

}

if testpw "$1" "$2" ; then
    echo "[OK]"
    exit 0
else
    echo "[FAILED]"
    exit 1
fi
