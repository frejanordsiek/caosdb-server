/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020-2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020-2021 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.xml;

import org.apache.shiro.SecurityUtils;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.IndexedSingleValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.TransactionLogMessage;
import org.jdom2.Attribute;
import org.jdom2.Element;

/**
 * Base class for the generation of a JDOM (XML) representation for entities.
 *
 * <p>Record and RecordType entities use this class only. Properties, Parents, Files and other
 * entities have specialized sub classes.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class EntityToElementStrategy implements ToElementStrategy {

  protected final String tagName;

  public EntityToElementStrategy(final String tagName) {
    this.tagName = tagName;
  }

  /**
   * Set the data type of this entity as a JDOM {@link Attribute} of the given element.
   *
   * <p>If the data type has a name, the name is used, otherwise the id is used.
   *
   * @param entity
   * @param element
   */
  public void setDatatype(EntityInterface entity, Element element) {
    if (entity.getDatatype().getName() != null) {
      element.setAttribute("datatype", entity.getDatatype().getName());
    } else {
      element.setAttribute("datatype", entity.getDatatype().getId().toString());
    }
  }

  /**
   * Set all properties of the entity that are considered to be part of the sparse entity, e.g.
   * name, description, etc, as {@link Attribute} of the given element.
   *
   * <p>The setFieldStrategy decides which attributes are set if present and which are omitted in
   * any case.
   *
   * @param element
   * @param entity
   * @param setFieldStrategy
   */
  public void sparseEntityToElement(
      final Element element,
      final EntityInterface entity,
      final SetFieldStrategy setFieldStrategy) {

    if (entity.getEntityACL() != null) {
      element.addContent(entity.getEntityACL().getPermissionsFor(SecurityUtils.getSubject()));
    }
    if (setFieldStrategy.isToBeSet("id") && entity.hasId()) {
      element.setAttribute("id", Integer.toString(entity.getId()));
    }
    if (entity.hasVersion()) {
      Element v = new VersionXMLSerializer().toElement(entity.getVersion());
      element.addContent(v);
    }
    if (setFieldStrategy.isToBeSet("cuid") && entity.hasCuid()) {
      element.setAttribute("cuid", entity.getCuid());
    }
    if (setFieldStrategy.isToBeSet("name") && entity.hasName()) {
      element.setAttribute("name", entity.getName());
    }
    if (setFieldStrategy.isToBeSet("description") && entity.hasDescription()) {
      element.setAttribute("description", entity.getDescription());
    }
    if (setFieldStrategy.isToBeSet("datatype") && entity.hasDatatype()) {
      setDatatype(entity, element);
    }
    if (setFieldStrategy.isToBeSet("message") && entity.hasMessages()) {
      for (final ToElementable m : entity.getMessages()) {
        m.addToElement(element);
      }
    }
    if (setFieldStrategy.isToBeSet("query") && entity.getQueryTemplateDefinition() != null) {
      final Element q = new Element("Query");
      q.setText(entity.getQueryTemplateDefinition());
      element.addContent(q);
    }
  }

  /**
   * Set the value of the entity.
   *
   * <p>The setFieldStrategy decides if the value is to be set at all.
   *
   * <p>If the value is a reference, the setFieldStrategy decides whether the referenced entity is
   * added as a deep Element tree (as a whole, so to speak) or just the ID of the referenced entity.
   *
   * @param entity
   * @param element
   * @param setFieldStrategy
   */
  public void setValue(EntityInterface entity, Element element, SetFieldStrategy setFieldStrategy) {
    if (entity.hasValue()) {
      try {
        entity.parseValue();
      } catch (final Message | NullPointerException e) {
        // Ignore. Parsing the value failed. But that does not concern us here, because this is the
        // case when a write transaction failed. The error for that has already been handled by the
        // CheckValueParsable job.
      }

      if (entity.isReference() && setFieldStrategy.isToBeSet("_referenced")) {
        // Append the complete entity. This needs to be done when we are
        // processing SELECT Queries.
        EntityInterface ref = ((ReferenceValue) entity.getValue()).getEntity();
        if (ref != null) {
          if (entity.hasDatatype()) {
            setDatatype(entity, element);
          }
          ref.addToElement(element, setFieldStrategy);
          // the referenced entity has been appended. Return here to suppress
          // adding the reference id as well.
          return;
        }
      } else if (entity.isReferenceList() && setFieldStrategy.isToBeSet("_referenced")) {
        // Append the all referenced entities. This needs to be done when we are
        // processing SELECT Queries.
        boolean skipValue = false;
        for (IndexedSingleValue sv : ((CollectionValue) entity.getValue())) {
          EntityInterface ref = ((ReferenceValue) sv.getWrapped()).getEntity();
          if (ref != null) {
            if (entity.hasDatatype()) {
              setDatatype(entity, element);
            }
            Element valueElem = new Element("Value");
            ref.addToElement(valueElem, setFieldStrategy);
            element.addContent(valueElem);
            skipValue = true;
          }
        }
        if (skipValue)
          // the referenced entity has been appended. Return here to suppress
          // adding the reference id as well.
          return;
      }

      if (setFieldStrategy.isToBeSet("value")) {
        if (entity.hasDatatype()) {
          setDatatype(entity, element);
        }
        entity.getValue().addToElement(element);
      }
    }
  }

  @Override
  public Element toElement(final EntityInterface entity, final SetFieldStrategy setFieldStrategy) {
    final Element element = new Element(tagName);

    // always have the values at the beginning of the children
    setValue(entity, element, setFieldStrategy);

    sparseEntityToElement(element, entity, setFieldStrategy);

    if (entity.hasStatementStatus() && setFieldStrategy.isToBeSet("importance")) {
      element.setAttribute("importance", entity.getStatementStatus().toString());
    }
    if (entity.hasParents() && setFieldStrategy.isToBeSet("parent")) {
      entity.getParents().addToElement(element);
    }
    if (entity.hasProperties()) {
      entity.getProperties().addToElement(element, setFieldStrategy);
    }
    if (entity.hasTransactionLogMessages() && setFieldStrategy.isToBeSet("history")) {
      for (final TransactionLogMessage t : entity.getTransactionLogMessages()) {
        t.xmlAppendTo(element);
      }
    }
    return element;
  }

  @Override
  public Element addToElement(
      final EntityInterface entity, final Element parent, final SetFieldStrategy setFieldStrategy) {
    if (entity.getEntityStatus() != EntityStatus.IGNORE) {
      parent.addContent(toElement(entity, setFieldStrategy));
    }
    return parent;
  }
}
