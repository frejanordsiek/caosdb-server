/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import org.apache.shiro.SecurityUtils;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.CredentialsValidator;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.database.backend.transaction.DeletePassword;
import org.caosdb.server.database.backend.transaction.DeleteUser;
import org.caosdb.server.database.backend.transaction.RetrievePasswordValidator;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

public class DeleteUserTransaction extends AccessControlTransaction {

  private final String user;
  private final String realm;

  public DeleteUserTransaction(final String user) {
    this.realm = UserSources.getInternalRealm().getName();
    this.user = user;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject()
        .checkPermission(ACMPermissions.PERMISSION_DELETE_USER(this.realm, this.user));

    final CredentialsValidator<String> validator =
        execute(new RetrievePasswordValidator(this.user), getAccess()).getValidator();

    if (validator == null) {
      throw ServerMessages.ACCOUNT_DOES_NOT_EXIST;
    }

    execute(new DeletePassword(this.user), getAccess());
    execute(new DeleteUser(this.realm, this.user), getAccess());
  }

  public Element getUserElement() {
    final Element ret = new Element("User");
    ret.setAttribute("realm", this.realm);
    ret.setAttribute("name", this.user);
    ret.addContent(new Message(MessageType.Info, 0, "This user has been deleted.").toElement());
    return ret;
  }
}
