/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import static java.sql.Types.BIGINT;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.InsertEntityPropertiesImpl;
import org.caosdb.server.database.exceptions.IntegrityException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.FlatProperty;
import org.caosdb.server.datatype.AbstractDatatype.Table;

public class MySQLInsertEntityProperties extends MySQLTransaction
    implements InsertEntityPropertiesImpl {

  public static final String STMT_INSERT_ENTITY_PROPERTY =
      "call insertEntityProperty(?,?,?,?,?,?,?,?,?,?,?,?)";

  public MySQLInsertEntityProperties(final Access access) {
    super(access);
  }

  @Override
  public void execute(
      final Integer domain,
      final Integer entity,
      final FlatProperty fp,
      final Table table,
      final Long unit_sig)
      throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_INSERT_ENTITY_PROPERTY);

      stmt.setInt(1, domain);
      stmt.setInt(2, entity);
      stmt.setInt(3, fp.id);
      stmt.setString(4, table.toString());

      stmt.setString(5, fp.value);
      if (unit_sig != null) {
        stmt.setLong(6, unit_sig);
      } else {
        stmt.setNull(6, BIGINT);
      }
      stmt.setString(7, fp.status);
      stmt.setString(8, fp.name);
      stmt.setString(9, fp.desc);
      stmt.setString(10, fp.type);
      stmt.setString(11, fp.collection);
      stmt.setInt(12, fp.idx);
      stmt.execute();
    } catch (final SQLIntegrityConstraintViolationException exc) {
      throw new IntegrityException(exc);
    } catch (final SQLException exc) {
      throw new TransactionException(exc);
    } catch (final ConnectionException exc) {
      throw new TransactionException(exc);
    }
  }
}
