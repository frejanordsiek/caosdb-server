/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.transaction.RetrieveFullEntityTransaction;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.entity.xml.SetFieldStrategy;
import org.caosdb.server.entity.xml.ToElementStrategy;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.jobs.ScheduledJob;
import org.caosdb.server.jobs.core.JobFailureSeverity;
import org.caosdb.server.jobs.core.RemoveDuplicates;
import org.caosdb.server.jobs.core.ResolveNames;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

public class Retrieve extends Transaction<RetrieveContainer> {

  public Retrieve(final RetrieveContainer container) {
    super(container);
  }

  @Override
  protected void init() throws Exception {
    // acquire weak access
    setAccess(getAccessManager().acquireReadAccess(this));

    // resolve names
    {
      final ResolveNames r = new ResolveNames();
      r.init(JobFailureSeverity.WARN, null, this);
      ScheduledJob scheduledJob = getSchedule().add(r);
      getSchedule().runJob(scheduledJob);
    }

    {
      final RemoveDuplicates job = new RemoveDuplicates();
      job.init(JobFailureSeverity.ERROR, null, this);
      ScheduledJob scheduledJob = getSchedule().add(job);
      getSchedule().runJob(scheduledJob);
    }

    // make schedule for all parsed entities
    makeSchedule();
  }

  @Override
  protected void preCheck() throws InterruptedException {}

  @Override
  protected void postCheck() {}

  @Override
  protected void preTransaction() {}

  @Override
  protected void postTransaction() {
    // generate Error for missing RETRIEVE:ENTITY Permission.
    for (final EntityInterface e : getContainer()) {
      if (e.getEntityACL() != null) {
        try {
          e.checkPermission(EntityPermission.RETRIEVE_ENTITY);
        } catch (final AuthorizationException exc) {
          e.setToElementStragegy(
              new ToElementStrategy() {

                @Override
                public Element toElement(
                    final EntityInterface entity, final SetFieldStrategy setFieldStrategy) {
                  Element ret;
                  if (entity.hasRole()) {
                    ret = new Element(entity.getRole().toString());
                  } else {
                    ret = new Element("Entity");
                  }
                  ret.setAttribute("id", entity.getId().toString());
                  for (final ToElementable m : entity.getMessages()) {
                    m.addToElement(ret);
                  }
                  return ret;
                }

                @Override
                public Element addToElement(
                    final EntityInterface entity,
                    final Element parent,
                    final SetFieldStrategy setFieldStrategy) {
                  parent.addContent(toElement(entity, setFieldStrategy));
                  return parent;
                }
              });
          e.setEntityStatus(EntityStatus.UNQUALIFIED);
          e.addError(ServerMessages.AUTHORIZATION_ERROR);
          e.addInfo(exc.getMessage());
        }
      }
    }
    // generate Error for non-existent entities.
    for (final EntityInterface wrappedEntity : getContainer()) {
      if (wrappedEntity.getEntityStatus() == EntityStatus.NONEXISTENT) {
        wrappedEntity.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
      }
    }
  }

  @Override
  protected void cleanUp() {
    // release weak access
    getAccess().release();
  }

  @Override
  protected void transaction() throws Exception {
    // retrieve entities from mysql database
    retrieveFullEntities(getContainer(), getAccess());
  }

  private void retrieveFullEntities(final RetrieveContainer container, final Access access)
      throws Exception {
    execute(new RetrieveFullEntityTransaction(container), access);
  }

  @Override
  public boolean logHistory() {
    return false;
  }
}
