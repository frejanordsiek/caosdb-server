package org.caosdb.server.jobs;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.UpdateEntity;
import org.jdom2.Element;
import org.junit.BeforeClass;
import org.junit.Test;

public class JobConfigTest {

  @BeforeClass
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testGetTransactionType() {
    final JobConfig jobConfig = JobConfig.getInstance();
    assertEquals("Retrieve", jobConfig.getTransactionType(new RetrieveEntity("test")));
    assertEquals("Insert", jobConfig.getTransactionType(new InsertEntity("test", Role.Record)));
    assertEquals("Delete", jobConfig.getTransactionType(new DeleteEntity(1234)));
    assertEquals("Update", jobConfig.getTransactionType(new UpdateEntity(new Element("Record"))));
  }
}
