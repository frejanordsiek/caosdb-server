/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.datetime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class LeapSecondDateTimeStringStrategy implements DateTimeStringStrategy {

  private final int leapSeconds;
  private final Calendar calendar;

  public LeapSecondDateTimeStringStrategy(final GregorianCalendar gc, final int leapSeconds) {
    this.leapSeconds = leapSeconds;
    this.calendar = gc;
  }

  @Override
  public String toDateTimeString(final TimeZone tz) {
    final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:'<XXX><NS>'Z");
    fmt.setCalendar(this.calendar);
    fmt.setTimeZone(tz);
    final String dateFormatted = fmt.format(this.calendar.getTime());
    final String seconds = Integer.toString(this.calendar.get(Calendar.SECOND) + this.leapSeconds);
    return dateFormatted.replaceFirst("<XXX>", (seconds.length() == 1 ? "0" + seconds : seconds));
  }

  @Override
  public String toDotNotation(final TimeZone tz) {
    this.calendar.setTimeZone(tz);
    final String date = getDateDotNotation();
    final String time = getTimeDotNotation();
    return date + "." + time + "." + "<NS>";
  }

  private String getTimeDotNotation() {
    final int hour = this.calendar.get(Calendar.HOUR_OF_DAY);
    final int minute = this.calendar.get(Calendar.MINUTE);
    final int second = this.calendar.get(Calendar.SECOND);

    final Integer time =
        ((hour + 1) * 10000) + ((minute + 1) * 100) + (second + this.leapSeconds + 1);
    return time.toString();
  }

  private String getDateDotNotation() {
    final int year = this.calendar.get(Calendar.YEAR);
    final int month = this.calendar.get(Calendar.MONTH);
    final int dom = this.calendar.get(Calendar.DAY_OF_MONTH);

    final Integer date = (year * 10000) + ((month + 1) * 100) + dom;
    return date.toString();
  }
}
