/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.HashSet;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.CacheableBackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrievePermissionRulesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.permissions.PermissionRule;

public class RetrievePermissionRules
    extends CacheableBackendTransaction<String, HashSet<PermissionRule>> {

  private static final ICacheAccess<String, HashSet<PermissionRule>> cache =
      Cache.getCache("BACKEND_PermissionRules");
  private HashSet<PermissionRule> rules;
  private final String role;

  public RetrievePermissionRules(final String role) {
    super(cache);
    this.role = role;
  }

  public static void removeCached(final String role) {
    cache.remove(role);
  }

  @Override
  public HashSet<PermissionRule> executeNoCache() throws TransactionException {
    final RetrievePermissionRulesImpl t = getImplementation(RetrievePermissionRulesImpl.class);
    return t.retrievePermissionRule(this.role);
  }

  @Override
  protected void process(final HashSet<PermissionRule> t) throws TransactionException {
    this.rules = t;
  }

  @Override
  protected String getKey() {
    return this.role;
  }

  public HashSet<PermissionRule> getRules() {
    return this.rules;
  }
}
