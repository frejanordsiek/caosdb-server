package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.SetFileChecksumImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLSetFileChecksum extends MySQLTransaction implements SetFileChecksumImpl {

  public MySQLSetFileChecksum(Access access) {
    super(access);
  }

  public static final String STMT_SET_CHECKSUM =
      "UPDATE files SET hash = unhex(?) WHERE file_id = ?";

  @Override
  public void execute(Integer id, String checksum) {
    try {
      PreparedStatement stmt = prepareStatement(STMT_SET_CHECKSUM);
      stmt.setInt(2, id);
      stmt.setString(1, checksum);
      stmt.execute();
    } catch (SQLException | ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
