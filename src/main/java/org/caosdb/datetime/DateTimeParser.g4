/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
parser grammar DateTimeParser;



options { tokenVocab = DateTimeLexer; }

datetime [DateTimeFactoryInterface dtf]
:
	(
        dot_notation[$dtf]
        | utcSec_notation[$dtf]
        | NOW {$dtf.setSystemDate(new java.util.Date());}
		| date[$dtf] (T | SPACE) time[$dtf] 
		| date[$dtf] time_zone[$dtf]?
		| time[$dtf]
	)
	EOF
;

utcSec_notation [DateTimeFactoryInterface dtf]
:
    signed_int {$dtf.setUTCSeconds($signed_int.text);}
    UTC
    (
        unsigned_int {$dtf.setNanoseconds($unsigned_int.text);}
    )?
;

dot_notation [DateTimeFactoryInterface dtf]
:
    (
        NULL |signed_int {$dtf.setDate($signed_int.text);}
    )    
    DOT 
    (
        NULL | unsigned_int {$dtf.setTime($unsigned_int.text);}
    )
    DOT
    ( 
        NULL | unsigned_int {$dtf.setTimeNS($unsigned_int.text);}
    )
;

unsigned_int
:
    NUM+
;

signed_int
:
    (HYPHEN)?? NUM+
;

date [DateTimeFactoryInterface dtf]
:
	year[$dtf]
	(
		HYPHEN
		month[$dtf]
		(
			HYPHEN 
			day[$dtf]
		)?
	)?
;

time [DateTimeFactoryInterface dtf]
:
	hour[$dtf]
	(
		COLON
		minute[$dtf]
		(
			COLON
			second[$dtf]
		)?
	)?
	time_zone[$dtf]?
;

year [DateTimeFactoryInterface dtf]
:
	(HYPHEN)?? NUM?? NUM?? NUM?? NUM {$dtf.setYear(Integer.parseInt($text));}
;

month [DateTimeFactoryInterface dtf]
:
	NUM NUM {$dtf.setMonth(Integer.parseInt($text));}
;

day [DateTimeFactoryInterface dtf]
:
	NUM NUM {$dtf.setDom(Integer.parseInt($text));}
;

hour [DateTimeFactoryInterface dtf]
:
	NUM NUM {$dtf.setHour(Integer.parseInt($text));}
;

minute [DateTimeFactoryInterface dtf]
:
	NUM NUM {$dtf.setMinute(Integer.parseInt($text));}
;

second [DateTimeFactoryInterface dtf]
:
	full_second[$dtf]
	(
		DOT
		fractional_seconds[$dtf]
	)?
;

full_second [DateTimeFactoryInterface dtf]
:
	NUM NUM {$dtf.setSecond(Integer.parseInt($text));}
;

fractional_seconds [DateTimeFactoryInterface dtf]
:
	NUM+ {$dtf.setNanoSecondFromFracionalString($text);}
;

time_zone [DateTimeFactoryInterface dtf]
:
	time_zone_offset[$dtf]
	| (T | SLASH |ABC | UTC)+ {$dtf.setTimeZone($text);}
;

/**
 * ISO 8601 designator for time zone offset.
 */
time_zone_offset [DateTimeFactoryInterface dtf] locals [int sign, int h, int m]
@init {
	$m = 0;
}
:
	(
		HYPHEN {$sign = UTCTimeZoneShift.NEGATIVE;}
		| PLUS {$sign = UTCTimeZoneShift.POSITIVE;}
	) 
	hours = time_zone_offset_part 
			{$h = Integer.parseInt($hours.text);} 
	(
		COLON? 
		mins = time_zone_offset_part 
				{$m = Integer.parseInt($mins.text);}
	) 
	{$dtf.setTimeZoneOffset($sign, $h, $m);}
;

time_zone_offset_part
:
	NUM NUM
;
