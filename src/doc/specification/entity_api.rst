The Entity API
==============

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   Overview of the CaosDB Entities' structure<Entity_Overview>

.. note::

   Until the corresponding `bug of openapi-codegen <https://github.com/OpenAPITools/openapi-generator/issues/9923>`_
   is resolved, the documentation of the OpenAPI specification of the
   entity API is best rendered by gitlab.

The details of the specification, including interactive examples, can
be found `here
<https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/tree/dev/src/doc/development/api/xml/caosdb_openapi.yaml>`_.
Note that some commands may require authentication, which can be done with
an authentication cookie as described in the :doc:`api
documentation<Authentication>`. Specialities of the :doc:`scripting
api<Server-side-scripting>` and the :doc:`file api<Fileserver>` are
explained in the corresponding sections of the specifications.
