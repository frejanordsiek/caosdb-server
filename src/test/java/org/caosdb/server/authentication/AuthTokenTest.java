/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.authentication;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.input.CharSequenceInputStream;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.Config;
import org.caosdb.server.accessControl.OneTimeAuthenticationToken;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.SelfValidatingAuthenticationToken;
import org.caosdb.server.accessControl.SessionToken;
import org.caosdb.server.accessControl.SessionTokenRealm;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrievePasswordValidatorImpl;
import org.caosdb.server.database.backend.interfaces.RetrievePermissionRulesImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveUserImpl;
import org.caosdb.server.resource.TestScriptingResource.RetrievePasswordValidator;
import org.caosdb.server.resource.TestScriptingResource.RetrievePermissionRules;
import org.caosdb.server.resource.TestScriptingResource.RetrieveRole;
import org.caosdb.server.resource.TestScriptingResource.RetrieveUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AuthTokenTest {

  @BeforeClass
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @BeforeClass
  public static void setupShiro() throws IOException {
    BackendTransaction.setImpl(RetrieveRoleImpl.class, RetrieveRole.class);
    BackendTransaction.setImpl(RetrievePermissionRulesImpl.class, RetrievePermissionRules.class);
    BackendTransaction.setImpl(RetrieveUserImpl.class, RetrieveUser.class);
    BackendTransaction.setImpl(
        RetrievePasswordValidatorImpl.class, RetrievePasswordValidator.class);

    CaosDBServer.initServerProperties();
    CaosDBServer.initShiro();
  }

  @Before
  public void reset() {
    OneTimeAuthenticationToken.resetConfig();
  }

  @Test
  public void testSessionToken() throws InterruptedException {
    // Token 1 - wrong checksum, not expired
    final SessionToken t1 =
        new SessionToken(
            new Principal("somerealm", "someuser1"),
            System.currentTimeMillis(),
            60000,
            "345sdf56sdf",
            "wrong checksum",
            null,
            null);
    Assert.assertFalse(t1.isExpired());
    Assert.assertFalse(t1.isHashValid());
    Assert.assertFalse(t1.isValid());

    // Token 3 - correct checksum, not expired
    final SessionToken t3 =
        new SessionToken(new Principal("somerealm", "someuser2"), 60000, null, null);
    Assert.assertFalse(t3.isExpired());
    Assert.assertTrue(t3.isHashValid());
    Assert.assertTrue(t3.isValid());

    // Token 5 - correct checksum, soon to be expired
    final SessionToken t5 =
        new SessionToken(new Principal("somerealm", "someuser3"), 2000, null, null);
    Assert.assertFalse(t5.isExpired());
    Assert.assertTrue(t5.isHashValid());
    Assert.assertTrue(t5.isValid());
    // wait until expired
    Thread.sleep(2001);
    Assert.assertTrue(t5.isExpired());
    Assert.assertTrue(t5.isHashValid());
    Assert.assertFalse(t5.isValid());

    // Token 6 - correct checksum, immediately expired
    final SessionToken t6 =
        new SessionToken(new Principal("somerealm", "someuser3"), 0, null, null);
    Assert.assertTrue(t6.isExpired());
    Assert.assertTrue(t6.isHashValid());
    Assert.assertFalse(t6.isValid());

    // All tokens can be successfully parsed back.
    final SelfValidatingAuthenticationToken t1p = SessionToken.parse(t1.toString());
    final SelfValidatingAuthenticationToken t3p = SessionToken.parse(t3.toString());
    final SelfValidatingAuthenticationToken t5p = SessionToken.parse(t5.toString());
    final SelfValidatingAuthenticationToken t6p = SessionToken.parse(t6.toString());
    Assert.assertEquals(t1.toString(), t1p.toString());
    Assert.assertEquals(t3.toString(), t3p.toString());
    Assert.assertEquals(t5.toString(), t5p.toString());
    Assert.assertEquals(t6.toString(), t6p.toString());

    // ... and parsed tokens have the correct hash validation
    Assert.assertFalse(t1p.isHashValid());
    Assert.assertTrue(t3p.isHashValid());
    Assert.assertTrue(t5p.isHashValid());
    Assert.assertTrue(t6p.isHashValid());

    Assert.assertFalse(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3))
            .isExpired());
    Assert.assertTrue(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3))
            .isHashValid());
    Assert.assertTrue(
        AuthenticationUtils.parseSessionTokenCookie(
                AuthenticationUtils.createSessionTokenCookie(t3))
            .isValid());

    // TODO parse invalid tokens
  }

  @Test
  public void testOneTimeTokenSerialization() {
    final OneTimeAuthenticationToken t1 =
        new OneTimeAuthenticationToken(
            new Principal("somerealm", "someuser"),
            60000,
            new String[] {"permissions"},
            new String[] {"roles"},
            1L,
            3000L);
    Assert.assertEquals(1L, t1.getMaxReplays());
    Assert.assertFalse(t1.isExpired());
    Assert.assertTrue(t1.isHashValid());
    Assert.assertTrue(t1.isValid());

    String serialized = t1.toString();
    OneTimeAuthenticationToken parsed =
        (OneTimeAuthenticationToken) OneTimeAuthenticationToken.parse(serialized);

    Assert.assertEquals(t1, parsed);
    Assert.assertEquals(serialized, parsed.toString());

    Assert.assertEquals(1L, parsed.getMaxReplays());
    Assert.assertFalse(parsed.isExpired());
    Assert.assertTrue(parsed.isHashValid());
    Assert.assertTrue(parsed.isValid());
  }

  @Test(expected = AuthenticationException.class)
  public void testOneTimeTokenConsume() {
    final OneTimeAuthenticationToken t1 =
        new OneTimeAuthenticationToken(
            new Principal("somerealm", "someuser"),
            60000,
            new String[] {"permissions"},
            new String[] {"roles"},
            3L,
            3000L);
    Assert.assertFalse(t1.isExpired());
    Assert.assertTrue(t1.isHashValid());
    Assert.assertTrue(t1.isValid());
    try {
      t1.consume();
      t1.consume();
      t1.consume();
    } catch (AuthenticationException e) {
      Assert.fail(e.getMessage());
    }

    // throws
    t1.consume();
    Assert.fail("4th time consume() should throw");
  }

  @Test(expected = AuthenticationException.class)
  public void testOneTimeTokenConsumeByParsing() {
    final OneTimeAuthenticationToken t1 =
        new OneTimeAuthenticationToken(
            new Principal("somerealm", "someuser"),
            60000,
            new String[] {"permissions"},
            new String[] {"roles"},
            3L,
            3000L);
    Assert.assertFalse(t1.isExpired());
    Assert.assertTrue(t1.isHashValid());
    Assert.assertTrue(t1.isValid());

    String serialized = t1.toString();
    try {
      SelfValidatingAuthenticationToken parsed1 = OneTimeAuthenticationToken.parse(serialized);
      Assert.assertTrue(parsed1.isValid());
      SelfValidatingAuthenticationToken parsed2 = OneTimeAuthenticationToken.parse(serialized);
      Assert.assertTrue(parsed2.isValid());
      SelfValidatingAuthenticationToken parsed3 = OneTimeAuthenticationToken.parse(serialized);
      Assert.assertTrue(parsed3.isValid());
    } catch (AuthenticationException e) {
      Assert.fail(e.getMessage());
    }

    // throws
    OneTimeAuthenticationToken.parse(serialized);
    Assert.fail("4th parsing should throw");
  }

  @Test
  public void testOneTimeTokenConfigEmpty() throws Exception {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("[]");

    List<Config> configs =
        OneTimeAuthenticationToken.loadConfig(new CharSequenceInputStream(testYaml, "utf-8"));

    Assert.assertTrue("empty config", configs.isEmpty());
  }

  @Test
  public void testOneTimeTokenConfigDefaults() throws Exception {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("roles: []\n");

    List<Config> configs =
        OneTimeAuthenticationToken.loadConfig(new CharSequenceInputStream(testYaml, "utf-8"));

    Assert.assertEquals(1, configs.size());
    Assert.assertTrue("parsing to Config object", configs.get(0) instanceof Config);

    Config config = configs.get(0);

    Assert.assertEquals(
        Integer.parseInt(
            CaosDBServer.getServerProperty(ServerProperties.KEY_ONE_TIME_TOKEN_EXPIRES_MS)),
        config.getExpiresAfter());
    Assert.assertEquals(1, config.getMaxReplays());
    Assert.assertNull("no purpose", config.getPurpose());

    Assert.assertArrayEquals("no permissions", new String[] {}, config.getPermissions());
    Assert.assertArrayEquals("no roles", new String[] {}, config.getRoles());
    Assert.assertNull("no output", config.getOutput());
  }

  @Test
  public void testOneTimeTokenConfigBasic() throws Exception {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("purpose: test purpose 1\n");
    testYaml.append("roles: [ role1, \"role2\"]\n");
    testYaml.append("expiresAfterSeconds: 10\n");
    testYaml.append("maxReplays: 3\n");
    testYaml.append("permissions:\n");
    testYaml.append("  - permission1\n");
    testYaml.append("  - 'permission2'\n");
    testYaml.append("  - \"permission3\"\n");
    testYaml.append("  - \"permission with white space\"\n");

    OneTimeAuthenticationToken.initConfig(new CharSequenceInputStream(testYaml, "utf-8"));
    Map<String, Config> map = OneTimeAuthenticationToken.getPurposeMap();
    Assert.assertNotNull("test purpose there", map.get("test purpose 1"));
    Assert.assertTrue("parsing to Config object", map.get("test purpose 1") instanceof Config);
    Config config = map.get("test purpose 1");
    Assert.assertEquals(10000, config.getExpiresAfter());
    Assert.assertEquals(3, config.getMaxReplays());

    Assert.assertArrayEquals(
        "permissions parsed",
        new String[] {"permission1", "permission2", "permission3", "permission with white space"},
        config.getPermissions());
    Assert.assertArrayEquals("roles parsed", new String[] {"role1", "role2"}, config.getRoles());
  }

  @Test
  public void testOneTimeTokenConfigNoRoles() throws Exception {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("purpose: no roles test\n");
    testYaml.append("permissions:\n");
    testYaml.append("  - permission1\n");
    testYaml.append("  - 'permission2'\n");
    testYaml.append("  - \"permission3\"\n");
    testYaml.append("  - \"permission with white space\"\n");

    OneTimeAuthenticationToken.initConfig(new CharSequenceInputStream(testYaml, "utf-8"));
    Map<String, Config> map = OneTimeAuthenticationToken.getPurposeMap();
    Config config = map.get("no roles test");

    Assert.assertArrayEquals("empty roles array parsed", new String[] {}, config.getRoles());
  }

  @Test
  public void testOneTimeTokenConfigNoPurpose() throws Exception {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("permissions:\n");
    testYaml.append("  - permission1\n");
    testYaml.append("  - 'permission2'\n");
    testYaml.append("  - \"permission3\"\n");
    testYaml.append("  - \"permission with white space\"\n");

    OneTimeAuthenticationToken.initConfig(new CharSequenceInputStream(testYaml, "utf-8"));
    Map<String, Config> map = OneTimeAuthenticationToken.getPurposeMap();
    Assert.assertEquals(map.size(), 0);
  }

  @Test
  public void testOneTimeTokenConfigMulti() throws Exception {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("- purpose: purpose 1\n");
    testYaml.append("- purpose: purpose 2\n");
    testYaml.append("- purpose: purpose 3\n");
    testYaml.append("- purpose: purpose 4\n");

    OneTimeAuthenticationToken.initConfig(new CharSequenceInputStream(testYaml, "utf-8"));
    Map<String, Config> map = OneTimeAuthenticationToken.getPurposeMap();
    Assert.assertEquals("four items", 4, map.size());
    Assert.assertTrue(map.get("purpose 2") instanceof Config);
  }

  @Test
  public void testOneTimeTokenConfigOutputFile() throws Exception {
    File tempFile = File.createTempFile("authtoken", "json");
    tempFile.deleteOnExit();

    StringBuilder testYaml = new StringBuilder();
    testYaml.append("- output:\n");
    testYaml.append("    file: " + tempFile.getAbsolutePath() + "\n");
    testYaml.append("  permissions: [ permission1 ]\n");

    // write the token
    OneTimeAuthenticationToken.initConfig(new CharSequenceInputStream(testYaml, "utf-8"));
    Assert.assertTrue(tempFile.exists());
    try (BufferedReader reader = new BufferedReader(new FileReader(tempFile))) {
      OneTimeAuthenticationToken token =
          (OneTimeAuthenticationToken) SelfValidatingAuthenticationToken.parse(reader.readLine());
      assertEquals("Token has anonymous username", "anonymous", token.getPrincipal().getUsername());
      assertEquals(
          "Token has anonymous realm",
          OneTimeAuthenticationToken.REALM_NAME,
          token.getPrincipal().getRealm());
      assertArrayEquals(
          "Permissions array has been written and read",
          new String[] {"permission1"},
          token.getPermissions().toArray());
    }
  }

  @Test
  public void testOneTimeTokenForAnonymous() throws Exception {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("purpose: for anonymous\n");
    testYaml.append("roles: [ role1 ]\n");
    testYaml.append("permissions:\n");
    testYaml.append("  - permission1\n");

    OneTimeAuthenticationToken.initConfig(new CharSequenceInputStream(testYaml, "utf-8"));

    Subject anonymous = SecurityUtils.getSubject();
    anonymous.login(AnonymousAuthenticationToken.getInstance());

    OneTimeAuthenticationToken token =
        OneTimeAuthenticationToken.generateForPurpose("for anonymous", anonymous);
    assertEquals("anonymous", token.getPrincipal().getUsername());
  }

  @Test
  public void testSessionTokenRealm() {
    Config config = new Config();
    OneTimeAuthenticationToken token = OneTimeAuthenticationToken.generate(config);

    String serialized = token.toString();
    SelfValidatingAuthenticationToken parsed = SelfValidatingAuthenticationToken.parse(serialized);

    SessionTokenRealm sessionTokenRealm = new SessionTokenRealm();
    Assert.assertTrue(sessionTokenRealm.supports(token));
    Assert.assertTrue(sessionTokenRealm.supports(parsed));

    Assert.assertNotNull(sessionTokenRealm.getAuthenticationInfo(token));
    Assert.assertNotNull(sessionTokenRealm.getAuthenticationInfo(parsed));

    Subject anonymous = SecurityUtils.getSubject();
    anonymous.login(token);
  }

  @Test
  public void testIntInConfigYaml() throws IOException {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("- expiresAfterSeconds: 1000\n");
    testYaml.append("  replayTimeout: 1000\n");

    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    ObjectReader reader = mapper.readerFor(Config.class);
    Config config =
        (Config) reader.readValues(new CharSequenceInputStream(testYaml, "utf-8")).next();

    assertEquals(1000000, config.getExpiresAfter());
    assertEquals(1000, config.getReplayTimeout());
  }

  @Test
  public void testLongInConfigYaml() throws IOException {
    StringBuilder testYaml = new StringBuilder();
    testYaml.append("- expiresAfter: 9223372036854775000\n");
    testYaml.append("  replayTimeoutSeconds: 922337203685477\n");

    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    ObjectReader reader = mapper.readerFor(Config.class);
    Config config =
        (Config) reader.readValues(new CharSequenceInputStream(testYaml, "utf-8")).next();

    assertEquals(9223372036854775000L, config.getExpiresAfter());
    assertEquals(922337203685477000L, config.getReplayTimeout());
  }
}
