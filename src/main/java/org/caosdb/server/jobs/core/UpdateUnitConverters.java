/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.database.backend.transaction.InsertLinCon;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.unit.Converter;
import org.caosdb.unit.LinearConverter;
import org.caosdb.unit.Unit;

@JobAnnotation(stage = TransactionStage.PRE_TRANSACTION)
public class UpdateUnitConverters extends EntityJob {

  @Override
  protected void run() {
    final Unit unit = getEntity().getUnit();
    final Converter converter = unit.getConverter();
    LinearConverter linearConverter = null;
    if (converter instanceof LinearConverter) {
      linearConverter = (LinearConverter) converter;
    } else {
      throw new UnsupportedOperationException(
          "Unit conversion function is not linear. Non-linear conversion functions are not supported yet. You ");
    }
    final double a = linearConverter.get_offset_a();
    final long b_dividend = linearConverter.get_dividend();
    final long b_divisor = linearConverter.get_divisor();
    final double c = linearConverter.get_offset_c();
    final long from = unit.getSignature();
    final Unit parent = unit.normalize();
    final long to = parent.getSignature();

    insertNewLinearConversion(from, to, a, b_dividend, b_divisor, c);
  }

  private void insertNewLinearConversion(
      final long signature_from,
      final long signature_to,
      final double a,
      final long b_dividend,
      final long b_divisor,
      final double c) {
    if (signature_from != signature_to) {
      final InsertLinCon t =
          new InsertLinCon(signature_from, signature_to, a, b_dividend, b_divisor, c);
      execute(t);
    }
  }
}
