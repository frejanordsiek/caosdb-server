package org.caosdb.server.jobs.core;

import org.caosdb.server.database.backend.transaction.GetAllNames;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;

/**
 * A jobs which retrieves all known names for which the requesting user has the RETRIEVE_ENTITY
 * permission.
 *
 * <p>The entites' status are set to VALID because the entities parents and properties do not have
 * to be retrieved.
 */
@JobAnnotation(flag = "names", stage = TransactionStage.INIT)
public class RetriveAllNames extends FlagJob {

  @Override
  protected void job(String value) {

    GetAllNames t = execute(new GetAllNames());
    for (EntityInterface e : t.getEntities()) {
      if (e.hasPermission(EntityPermission.RETRIEVE_ENTITY)) {
        this.getContainer().add(e);
        e.setEntityStatus(EntityStatus.VALID);
      }
    }
  }
}
