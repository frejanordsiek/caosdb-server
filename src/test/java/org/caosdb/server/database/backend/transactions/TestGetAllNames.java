package org.caosdb.server.database.backend.transactions;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetAllNamesImpl;
import org.caosdb.server.database.backend.transaction.GetAllNames;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.entity.EntityInterface;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestGetAllNames {

  public static class GetAllNamesBackend implements GetAllNamesImpl {

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public List<SparseEntity> execute() {
      ArrayList<SparseEntity> ret = new ArrayList<>();

      for (int i = 0; i < 10; i++) {

        SparseEntity e = new SparseEntity();
        e.role = "RecordType";
        e.name = "Test" + Integer.toString(i);
        e.acl = "";
        ret.add(e);
      }
      return ret;
    }

    public GetAllNamesBackend(Access access) {}
  }

  @BeforeClass
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
    BackendTransaction.init();
    BackendTransaction.setImpl(GetAllNamesImpl.class, GetAllNamesBackend.class);
  }

  @Test
  public void test() {
    GetAllNames getAllNames = new GetAllNames();
    getAllNames.executeTransaction();
    List<EntityInterface> entities = getAllNames.getEntities();
    assertEquals(entities.size(), 10);
    EntityInterface first = entities.get(0);
    assertEquals(first.getName(), "Test0");
  }
}
