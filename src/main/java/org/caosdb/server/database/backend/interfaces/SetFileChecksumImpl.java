package org.caosdb.server.database.backend.interfaces;

public interface SetFileChecksumImpl extends BackendTransactionImpl {

  void execute(Integer id, String checksum);
}
