package org.caosdb.server.caching;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.io.IOException;
import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.accessControl.Pam;
import org.caosdb.server.database.backend.transaction.RetrieveProperties;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCaching {

  @BeforeClass
  public static void init() throws IOException {
    CaosDBServer.initServerProperties();
    JCSCacheHelper.init();
  }

  @Test
  public void testCacheConfig() {
    CacheAccess<String, String> retrieve_properties_cache =
        JCS.getInstance(RetrieveProperties.CACHE_REGION);
    assertEquals(1003, retrieve_properties_cache.getCacheAttributes().getMaxObjects());

    CacheAccess<String, String> retrieve_entities_cache = JCS.getInstance("BACKEND_SparseEntities");
    assertEquals(1002, retrieve_entities_cache.getCacheAttributes().getMaxObjects());

    CacheAccess<String, String> pam_groups_cache = JCS.getInstance(Pam.CACHE_REGION_GROUPS);
    assertEquals(1000, pam_groups_cache.getCacheAttributes().getMaxObjects());
    assertEquals(false, pam_groups_cache.getCacheAttributes().isUseMemoryShrinker());
    assertEquals(false, pam_groups_cache.getDefaultElementAttributes().getIsEternal());
    assertEquals(61, pam_groups_cache.getDefaultElementAttributes().getIdleTime());
    assertEquals(601, pam_groups_cache.getDefaultElementAttributes().getMaxLife());
  }

  @Test
  public void testCacheElements() throws IOException {
    final CacheAccess<Object, Object> cache = JCS.getInstance("default");

    final String key = "KEY";
    final String value = "VALUE";

    cache.put(key, value);

    assertEquals(value, cache.get(key));
    assertSame(value, cache.get(key));
  }
}
