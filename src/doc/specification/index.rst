Specification
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   AbstractProperty
   Fileserver
   Record
   Authentication
   Datatype
   Paging
   RecordType
   Server side scripting <Server-side-scripting>
   Specification of the Message API <Specification-of-the-Message-API>
   Specification of the Entity API <entity_api>
